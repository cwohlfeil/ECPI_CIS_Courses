Debug 03-01
-------------
// This pseudocode segment is intended to determine whether students have
// passed or failed a course based on the average score of two
// tests. Student needs 60 average or better to pass.
start
   Declarations
      num firstTest
      num secondTest
      num average
      num PASSING = 60
   output "Enter first score or -1 to quit "
   input firstTest
   while firstTest >= 0
      output "Enter second score"
      input secondTest
      average = (firstTest + secondTest) / 2
      ouput "Average is ", average
      if average >= PASSING then
         output "Pass"
      else
         output "Fail"
      endif
      output "Enter first score or -1 to quit "
      input firstTest
   endwhile
stop


Debug 03-02
-------------
// This pseudocode segment is intended to display
// employee net pay values. All employees have a standard
// $45 deduction from their checks.
// If an employee does not earn enough to cover the deduction,
// an error message is displayed.
start
   Declarations
      string name
      num hours
      num rate
      num DEDUCTION = 45
      string EOFNAME = "ZZZ"
      num gross
      num net
   output "Enter first name or ", EOFNAME, " to quit"
   input name
   while name != EOFNAME
      output "Enter hours worked for ", name
      input hours
      output "Enter hourly rate for ", name
      input rate
      gross = hours * rate
      net = gross - DEDUCTION
      if net > 0 then
         output "Net pay for ", name, " is ", net
      else
          output "Employee did not make enough to cover deductions. Net is 0"
      output "Enter next name or ", EOFNAME, " to quit"
      input name
   endwhile
   output "End of job"
stop


Debug 03-03
-------------
// This pseudocode segment is intended to display
// employee net pay values. All employees have a standard
// $45 deduction from their checks.
// If an employee does not earn enough to cover the deduction,
// an error message is displayed.
// This example is modularized.
start
   Declarations
      string name
      string EOFNAME = "ZZZZ"
   housekeeping()
   while name != EOFNAME
      mainLoop()
   endwhile
   finish()
stop

housekeeping()
   output "Enter first name or ", EOFNAME, " to quit "
return

mainLoop()
   Declarations
      num hours
      num rate
      num DEDUCTION = 45
      num gross
      num net
   output "Enter hours worked for ", name
   input hours
   output "Enter hourly rate for ", name
   input rate
   gross = hours * rate
   net = gross - DEDUCTION
   if net > 0 then
      output "Net pay for ", name, " is ", net
   else
      output "Employee did not make enough to cover deductions. Net is 0"
   endif
   output "Enter next name or ", EOFNAME, " to quit"
   input name
return

finish()
   output "End of job"
return
