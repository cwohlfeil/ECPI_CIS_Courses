Debug 06-01
------------
// A high school is holding a recycling competition
// This program allows a user to enter a student's 
// year in school (1 through 4)
// and number of cans collected
// Data is entered continuously until the user wnats to quit
// After headings, output is four lines
// one for each school year class
start
   Declarations
      num year
      num cans
      num SIZE = 4
      num QUIT = 9
      num collected[SIZE] = 0, 0, 0, 0
      string HEAD1 = "Can Recycling Report"
      string HEAD2 = "Year	Cans collected"
   output "Enter year of student or ", QUIT, " to quit "
   input year
   while year <> QUIT
      output "Enter number of cans collected "
      input cans
      collected[year - 1] = collected[year - 1] + cans
      output "Enter year of student or ", QUIT, " to quit "
      input year
   endwhile
   output HEAD1
   output HEAD2
   year = 1
   while year < SIZE
      output year, collected[year - 1]
      year = year + 1 
   endwhile
stop         



Debug 06-02
------------
// Program lets user input scores on four tests
// Average is computed and letter grade is determined
// Letter grades are based on 90 for an A, 80 for a B, and so on
start
   string name
   num score 
   num NUM_TESTS = 4
   num NUM_RANGES = 5
   num RANGES[NUM_RANGES] = 90, 80, 70, 60, 0
   num QUIT = "ZZZZZ"
   string GRADES[NUM_RANGES] = "A", "B", "C", "D", "F"
   num total
   num average
   num sub
   output "Enter student name or ", QUIT, " to quit "
   input name
   while name <> QUIT
      total = 0
      sub = 0
      while sub < NUM_TESTS
         output "Enter score "
         input score
         total = total + score
         sub = sub + 1
      endwhile
      average = total / NUM_TESTS
      sub = 0
      while average < RANGES[sub]
         sub = sub + 1
      endwhile
      letterGrade = RANGES[sub]
      output name, letterGrade
      output "Enter student name or ", QUIT, " to quit "
      input name
   endwhile
stop



Debug 06-03
------------
// This program counts how many sales are made in each of five categories of products
start
   Declarations
      num category
      num SIZE = 5
      num QUIT = 9
      num sales[SIZE] = 0, 0, 0, 0, 0
      string HEAD1 = "Sales"
      string HEAD2 = "Category   Number of Sales"
   output "Enter category ", QUIT, " to quit "
   input category
   while category <> QUIT
      if category >= 1 AND category <= SIZE then
         sales[category - 1] = sales[category - 1] + 1
      else
         output "Invalid category"
      endif
      output "Enter category ", QUIT, " to quit "
      input category
   endwhile
   output HEAD1
   output HEAD2
   category = 0
   while category < SIZE
      output category + 1, sales[category]
      category = category + 1
   endwhile
stop         
