// This pseudocode segment is intended to describe
// computing the price of an item on sale for 20% off
start
   input origPrice
   set discount = origPrice * 0.20
   set finalPrice = origPrice - discount
   output final
stop
