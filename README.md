# ECPI_CIS_Courses

The code I wrote for the CIS courses I have taken at ECPI university.

If you find any personal info in this repository, contact me so I can purge it from the git history.

**DO NOT USE THIS FOR PLAGIARISM**

This is only meant to help other people learn from what I've done. It's important you do your own assignments and ensure you understand the material. Not to mention, I don't always ace assignments. I frequently do them my own way, misread or ignore instructions, and get docked points. Not to mention your coursework may be different than mine since I've had a mix of instructors and have moved between online, hybrid, and night classes.

If you are a current ECPI student, here's some general lessons I can give you:

1) Always follow instructions exactly as they are given.
2) You may know how to write better or more clever code than the instructor, but don't. Do it separately.
3) Comment every line of code that performs an action. Seriously.
4) Always use the version of software and tools specified in the instructions. 
5) Always make sure your code compiles/runs.
6) Always make sure your code meets all requirements.
7) If you are unsure of anything, ask the instructor for clarification.
8) Don't just copy and paste code, understand what is happening.

These are all mistakes I have made, and doing things your own way 

If you need help with something and want to try and figure it out on your own before asking for help (you should!), try these sites. 

* [Microsoft Docs (C#, Access, SQL Server)](https://docs.microsoft.com/en-us/)
* [Java Docs](https://docs.oracle.com/javase/10/)
* [Learn Cpp (C/C++)](http://www.learncpp.com/)