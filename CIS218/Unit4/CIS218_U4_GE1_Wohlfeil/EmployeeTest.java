/*
 * Name: Cameron Wohlfeil
 * Date: 02/19/2019
 * Description: Employee Pay Calculator
 *
 * 1) Create an Array of 10 Employee Objects
 * 2) Ask the user in a loop, which type of employee they are creating (Hourly, Salary, Commission, or PieceWorker).
 * Depending on the type of employee, ask the user for additional information as necessary
 * (first name, last name, ssn, etc). Then Instantiate your class and store in your array.
 * 3) Ensure that the loop can handle any exceptions thrown and will allow the user to enter up to 10 different types of employees.
 * 4) Once the user finishes entering data, loop through your array and print out the toString method of everything stored in the array.
 * 5) Also, print out the total earnings for all the employees entered and the number of each type of employee entered.
 */

import java.util.Scanner;

public class EmployeeTest {
    public static void main(String[] args) {
        Employee[] employees = new Employee[10];
        int[] employeeTypeCounter = new int[4];
        int employeeCounter = 0, employeeType = 0;
        double payRate, payModifier, totalEarnings = 0;
        String firstName, lastName, socialSecurityNumber;
        Scanner input = new Scanner(System.in);

        System.out.println("Employee Pay Calculator");

        while (employeeType != -1) {
            System.out.print("Enter 1 for hourly, 2 for salary, 3 for commission, 4 for piece worker, -1 to quit: ");
            employeeType = input.nextInt();
            input.nextLine();  // Consume newline left-over

            // If employee type is between 1 and 4, process it, otherwise pass to next iteration
            if (employeeType >= 1 && employeeType <= 4) {
                // Get general employee info
                System.out.print("Enter employee first name: ");
                firstName = input.nextLine();

                System.out.print("Enter employee last name: ");
                lastName = input.nextLine();

                System.out.print("Enter employee social security number: ");
                socialSecurityNumber = input.nextLine();

                // Get class specific employee info
                try {
                    switch (employeeType) {
                        case 1: // hourly
                            System.out.print("Enter wages: ");
                            payRate = input.nextDouble();
                            System.out.print("Enter hours: ");
                            payModifier = input.nextDouble();
                            employees[employeeCounter] = new HourlyEmployee(firstName, lastName, socialSecurityNumber,
                                    payRate, payModifier);
                            employeeTypeCounter[0]++;
                            break;
                        case 2: // salary
                            System.out.print("Enter weekly salary: ");
                            payRate = input.nextDouble();
                            employees[employeeCounter] = new SalariedEmployee(firstName, lastName, socialSecurityNumber,
                                    payRate);
                            employeeTypeCounter[1]++;
                            break;
                        case 3: // commission
                            System.out.print("Enter gross sales: ");
                            payModifier = input.nextDouble();
                            System.out.print("Enter commission rate: ");
                            payRate = input.nextDouble();
                            employees[employeeCounter] = new CommissionEmployee(firstName, lastName, socialSecurityNumber,
                                    payModifier, payRate);
                            employeeTypeCounter[2]++;
                            break;
                        case 4: // pieces worker
                            System.out.print("Enter wages per piece: ");
                            payRate = input.nextDouble();
                            System.out.print("Enter pieces produced: ");
                            payModifier = input.nextDouble();
                            employees[employeeCounter] = new PieceWorker(firstName, lastName, socialSecurityNumber,
                                    payRate, payModifier);
                            employeeTypeCounter[3]++;
                            break;
                    } // end switch
                    employeeCounter++;
                } catch (IllegalArgumentException e) {
                    System.out.println(e.getMessage());
                } // end try/catch
            } // end if
        } // end while

        // Loop through array and print out employee, add earning to total
        for (Employee employee : employees) {
            if (employee != null) {
                System.out.printf("%n%s", employee.toString());
                totalEarnings += employee.earnings();
            }
        }

        // Print out total earnings and count of account types
        System.out.printf("%nTotal hourly employees: %d", employeeTypeCounter[0]);
        System.out.printf("%nTotal salary employees: %d", employeeTypeCounter[1]);
        System.out.printf("%nTotal commission employees: %d", employeeTypeCounter[2]);
        System.out.printf("%nTotal piece workers: %d", employeeTypeCounter[3]);
        System.out.printf("%nTotal earnings: $%,.2f", totalEarnings);

        input.close();
    } // end main
} // end class
