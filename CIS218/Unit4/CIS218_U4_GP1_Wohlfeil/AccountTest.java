/*
 * Name: Cameron Wohlfeil
 */

import java.util.Scanner;

public class AccountTest {
    public static void main(String[] args) {
        Account accounts[] = new Account[10];
        int accountCounter = 0;
        int accountType = 0;
        double fee, interestRate, balance;
        Scanner input = new Scanner(System.in);

        while (accountType != -1) {
            System.out.println("Enter 1 for checking, 2 for savings, 3 for general account, -1 to quit");
            accountType = input.nextInt();
            System.out.println("Enter balance ");
            balance = input.nextDouble();
            try {
                switch (accountType) {
                    case 1: // checking
                        System.out.println("Enter fee ");
                        fee = input.nextDouble();
                        accounts[accountCounter] = new CheckingAccount(balance, fee);
                        break;
                    case 2: // savings
                        System.out.println("Enter interest rate ");
                        interestRate = input.nextDouble();
                        accounts[accountCounter] = new SavingsAccount(balance, interestRate);
                        break;
                    case 3: // account
                        accounts[accountCounter] = new Account(balance);
                        break;
                }
                accountCounter++;
            } catch (IllegalArgumentException e) {
                System.out.println(e.getMessage());
            }
        }

        for (Account account : accounts) {
            if (account != null) {
                if (account instanceof SavingsAccount) {
                    // downcast
                    SavingsAccount savingsAct = (SavingsAccount) account;
                    double yearlyInterest = savingsAct.getInterestRate();

                    savingsAct.setBalance(savingsAct.getBalance() + yearlyInterest);
                }
                account.toString();
            }
        }
        input.close();
    }
}
