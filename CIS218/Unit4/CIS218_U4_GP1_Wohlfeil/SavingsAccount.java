/*
 * Name: Cameron Wohlfeil
 */

public class SavingsAccount extends Account {
    private double interestRate;

    public SavingsAccount(double balance, double interestRate) {
        super(balance); // call parent constructor
        setInterestRate(interestRate);
    }

    public void setInterestRate(double interestRate) {
        if (interestRate < 0.0) {
            throw new IllegalArgumentException("Interest rate must be > 0.");
        }
        this.interestRate = interestRate;
    }

    public double getInterestRate() {
        return interestRate;
    }

    public double calculateInterest() {
        return getBalance() * interestRate;
    }

    @Override
    public String toString() {
        return String.format("%s%s:$%,.2f%n", super.toString(), "Interest Rate", interestRate);
    }
}
