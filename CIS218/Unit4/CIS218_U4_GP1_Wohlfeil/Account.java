/*
 * Name: Cameron Wohlfeil
 */

public class Account {
    private double balance;

    public Account (double balance) {
        setBalance(balance);
    }

    public void setBalance(double balance) {
        if (balance < 0.0) {
            throw new IllegalArgumentException("Balance must be greater than 0.");
        }
        this.balance = balance;
    }

    public boolean credit (double amount) {
        boolean isValid = true;
        if (amount > 0) {
            balance += amount;
        } else {
            System.out.println("Amount must be > 0.");
            isValid = false;
        }
        return isValid;
    }

    public boolean debit (double amount) {
        boolean success = true;
        if (amount > balance) {
            System.out.println("You don't have that much to debit.");
            success = false;
        } else if (amount < 0) {
            System.out.println("Amount must be > 0.");
            success = false;
        } else {
            balance -= amount;
        }
        return success;
    }

    public double getBalance() {
        return balance;
    }

    @Override
    public String toString() {
        return  String.format("%s: $%,.2f", balance);
    }
}
