/*
 * Name: Cameron Wohlfeil
 */

public class CheckingAccount extends Account {
    private double fee;

    public CheckingAccount(double balance, double fee) {
        super(balance); // call parent constructor
        setFee(fee);
    }

    public void setFee(double fee) {
        if (fee < 0.0) {
            throw new IllegalArgumentException("Fee must be > 0.");
        }
        this.fee = fee;
    }

    public double getFee() {
        return fee;
    }

    @Override
    public boolean credit(double amount) {
        boolean success;
        success = super.credit(amount);
        if (success) {
            super.debit(fee);
        }
        return success;
    }

    @Override
    public boolean debit(double amount) {
        boolean success;
        if (getBalance() < (amount + fee)) {
            System.out.println("You don't have enough to withdraw.");
            success = false;
        } else {
            success = super.debit(amount + fee);
        }

        return success;
    }

    @Override
    public String toString() {
        return String.format("%s%s:$%,.2f%n", super.toString(), "Fee", fee);
    }
}
