/*
 * Name: Cameron Wohlfeil
 * Date: 02/20/2019
 */

public class Triangle extends TwoDimensionalShape {
    private double base; 
    private double height; 
    
    public Triangle(double base, double height) {
        setBase(base);
        setHeight(height);
    } 
    
    public void setBase(double base) {
        if (base < 0) {
            throw new IllegalArgumentException("Base must be > 0");
        }
        this.base = base;
    } 
    
    public double getBase() { return base; } 
    
    public void setHeight(double height) {
        if (height < 0) {
            throw new IllegalArgumentException("Height must be > 0");
        }
        this.height = height;
    } 
    
    public double getHeight() { return height; }
    
    @Override
    public double getArea() { return getBase() * getHeight() / 2; } 
    
    @Override
    public String toString() {
        return String.format("%s %s%n%s: %.2f%n%s: %.2f", super.toString(), "Triangle",
                "Base", getBase(), "Height", getHeight());
    } 
} 
