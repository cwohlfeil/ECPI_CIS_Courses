/*
 * Name: Cameron Wohlfeil
 * Date: 02/20/2019
 * Description: Shape inheritance
 *
 * 1) Implement the Shape hierarchy shown in Fig. 9.3.
 * 2) Each TwoDimensionalShape should contain method getArea to calculate the area of the two-dimensional shape.
 * 3) Each ThreeDimensionalShape should have methods getArea and getVolume to calculate the surface area and volume, respectively, of the three-dimensional shape.
 * 4) Create a program that uses an array of Shape references to objects of each concrete class in the hierarchy.
 * 5) The program should print a text description of the object to which each array element refers.
 * 6) Also, in the loop that processes all the shapes in the array, determine whether each shape is a TwoDimensionalShape or a ThreeDimensionalShape.
 * 7) If it’s a TwoDimensionalShape, display its area.
 * 8) If it’s a ThreeDimensionalShape, display its area and volume.
 */

import java.util.Scanner;

public class ShapeTest {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        // create variables
        Shape[] shapes = new Shape[20];
        int shapeCounter = 0, shapeType = 0;
        double length, width, radius;

        while (shapeType != -1 && shapeCounter < 20) {
            System.out.print("Enter 1 for square, 2 for circle, 3 for triangle, 4 for sphere, " +
                             "5 for tetrahedron, 6 for cube or  -1 to quit: ");
            shapeType = input.nextInt();

            try {
                switch (shapeType) {
                    case 1: // square
                        System.out.print("Enter a length: ");
                        length = input.nextDouble();

                        shapes[shapeCounter] = new Square(length);
                        break;
                    case 2: // circle
                        System.out.print("Enter the radius: ");
                        radius = input.nextDouble();

                        shapes[shapeCounter] = new Circle(radius);
                        break;
                    case 3: // triangle
                        System.out.print("Enter the base: ");
                        length = input.nextDouble();

                        System.out.print("Enter the height: ");
                        width = input.nextDouble();

                        shapes[shapeCounter] = new Triangle(length, width);
                        break;
                    case 4: // sphere
                        System.out.print("Enter the radius: ");
                        radius = input.nextDouble();

                        shapes[shapeCounter] = new Sphere(radius);
                        break;
                    case 5: // tetrahedron
                        System.out.print("Enter the length of a side: ");
                        length = input.nextDouble();

                        shapes[shapeCounter] = new Tetrahedron(length);
                        break;
                    case 6: // cube
                        System.out.print("Enter the length of a side: ");
                        length = input.nextDouble();

                        shapes[shapeCounter] = new Cube(length);
                        break;
                } // end switch

                shapeCounter++;
            } catch (IllegalArgumentException e) {
                System.out.println(e.getMessage());
            }
        } // end while

        // loop through array of shapes and print their area
        for (Shape currentShape : shapes) {
            if (currentShape != null) {
                System.out.println(currentShape);

                // if three-dimensional shape, print area and volume
                if (currentShape instanceof ThreeDimensionalShape) {
                    ThreeDimensionalShape threeDim = (ThreeDimensionalShape) currentShape;
                    System.out.printf("Area: %.2f %n", threeDim.getArea());
                    System.out.printf("Volume: %.2f %n%n", threeDim.getVolume());
                } else {
                    System.out.printf("Area: %.2f%n%n", currentShape.getArea());
                }
            }
        } // end for

        input.close();
    } // end main
} // end class
