/*
 * Name: Cameron Wohlfeil
 * Date: 02/20/2019
 */

public class Cube extends ThreeDimensionalShape {
    private double length; 

    public Cube(double length) {
        setLength(length);
    } 
    
    public void setLength(double length) {
        if (length < 0) {
            throw new IllegalArgumentException("Length must be > 0");
        }
        this.length = length;
    } 
    
    public double getLength() {
        return length;
    }
    
    @Override
    public double getArea() { return getLength() * getLength() * 6; } 
    
    @Override
    public double getVolume() {
        return getLength() * getLength() * getLength();
    } 
    
    @Override
    public String toString() {
        return String.format("%s: %s%n%s: %.2f",
                super.toString(), "Cube", "Length", getLength());
    } 
} 
