/*
 * Name: Cameron Wohlfeil
 * Date: 02/20/2019
 */

public abstract class ThreeDimensionalShape extends Shape {
    public abstract double getVolume(); 
    
    @Override
    public String toString() {
        return String.format("Three Dimensional Shape");
    } 
} 
