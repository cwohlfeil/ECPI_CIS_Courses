/*
 * Name: Cameron Wohlfeil
 * Date: 02/20/2019
 */

public class Sphere extends ThreeDimensionalShape {
    private double radius; 
    
    public Sphere(double radius) {
        setRadius(radius);
    } 
    
    public void setRadius(double radius) {
        if (radius < 0) {
            throw new IllegalArgumentException("Radius must be > 0");
        }
        this.radius = radius;
    } 
    
    public double getRadius() { return radius; } 
    
    @Override
    public double getArea() {
        return getRadius() * getRadius() * 4 * Math.PI;
    } 
    
    @Override
    public double getVolume() {
        return (getRadius() * getRadius() * getRadius() * Math.PI * 4) / 3;
    } 
    
    @Override
    public String toString() {
        return String.format("%s: %s%n%s: %.2f",
                super.toString(), "Sphere", "Radius", getRadius());
    } 
} 
