/*
 * Name: Cameron Wohlfeil
 * Date: 02/20/2019
 */

public class Tetrahedron extends ThreeDimensionalShape {
    private double length;
    
    public Tetrahedron(double length) {
        setLength(length);
    } 
    
    public void setLength(double length) {
        if (length < 0) {
            throw new IllegalArgumentException("Length must be > 0");
        }
        this.length = length;
    } 
    
    public double getLength() { return length; } 
    
    @Override
    public double getArea() {
        return getLength() * getLength() * Math.sqrt(3);
    }
    
    @Override
    public double getVolume() {
        return (getLength() * getLength() * getLength()) / (6 * Math.sqrt(2));
    } 
    
    @Override
    public String toString() {
        return String.format("%s: %s%n%s: %.2f", 
                super.toString(), "Regular Tetrahedron", "Length of edge", getLength());
    } 
} 
