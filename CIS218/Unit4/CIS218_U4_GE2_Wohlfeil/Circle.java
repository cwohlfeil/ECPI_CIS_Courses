/*
 * Name: Cameron Wohlfeil
 * Date: 02/20/2019
 */

public class Circle extends TwoDimensionalShape {
    private double radius; 

    public Circle(double radius) {
        setRadius(radius);
    } 
    
    public void setRadius(double radius) {
        if (radius < 0) {
            throw new IllegalArgumentException("Radius must be < 0");
        }
        this.radius = radius;
    } 
    
    public double getRadius() { return radius; }
    
    @Override
    public double getArea() {
        return getRadius() * getRadius() * Math.PI;
    }
    
    @Override
    public String toString() {
        return String.format("%s %s%n%s: %.2f", super.toString(), "Circle",
                "Radius", getRadius());
    } 
}
