/*
 * Name: Cameron Wohlfeil
 * Date: 02/20/2019
 */

public abstract class TwoDimensionalShape extends Shape {
    @Override
    public String toString() {
        return String.format("Two dimensional shape:");
    } 
}
