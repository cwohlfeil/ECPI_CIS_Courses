/*
 * Name: Cameron Wohlfeil
 * Date: 02/20/2019
 */

public abstract class Shape {
    // Both 2D and 3D shapes will have the getArea method
    public abstract double getArea();
} 
