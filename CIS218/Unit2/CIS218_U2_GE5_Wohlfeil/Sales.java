/*
 * Name: Cameron Wohlfeil
 * Date: 2/6/2019
 * Description: Sales Calculator
 *
 * 1) Sells five products whose retail prices are as follows: Product 1, $2.98; product 2, $4.50; product 3, $9.98; product 4, $4.49 and product 5, $6.87.  
 * 2) Reads a series of pairs of numbers as follows: product number, quantity sold.
 * 3) Use a switch statement to determine the retail price for each product. 
 * 4) Use a sentinel-controlled loop to determine when the program should stop looping and display the final results.
 * 5) Ensure that invalid entries are flagged. 
 * 6) Calculate and display the total retail value of all products sold. 
 */

import java.util.Scanner; // Scanner for input
import java.text.NumberFormat; // NumberFormat for currency formatting
import java.util.Locale; // Locale for currency formatting

public class Sales {
    public static void main(String[] args) {
        // try-with-resources to automatically close scanner
        try (Scanner input = new Scanner(System.in)) {            
            // Currency formatter, US locale
            NumberFormat formatter = NumberFormat.getCurrencyInstance(Locale.US);
            
            // Variable to track state between loops
            int cardinal = -1;
            double total = 0;

            // while loop to keep program running until user exits
            while (cardinal != 0) {
                int quantity; // Input variable
                
                // Prompt and set cardinal
                System.out.print("Enter product number (1-5 or 0 to stop): "); 
                cardinal = input.nextInt();
                
                // if cardinal is not between 1-5, stop iteration of loop
                if (cardinal == 0)
                    continue;
                else if (cardinal < 1 || cardinal > 5) {
                    System.out.println("Product number must be between 1 and 5 or 0 to stop.");
                    continue; 
                }
                
                // Prompt and set quantity
                System.out.print("Enter quantity sold: "); 
                quantity = input.nextInt();

                // Switch to handle adding product to total
                switch (cardinal) {
                    case 1: 
                        total += 2.98 * quantity;
                        break;
                    case 2:
                        total += 4.5 * quantity;
                        break;
                    case 3:
                        total += 9.98 * quantity;
                        break;
                    case 4:
                        total += 4.49 * quantity;
                        break;
                    case 5:
                        total += 6.87 * quantity;
                        break;
                } // end switch
            } // end while
            
            // Output total
            System.out.printf("%nTotal retail value of all products sold is %s%n",
                              formatter.format(total));
        } // end try
    } // end main
} // end class 