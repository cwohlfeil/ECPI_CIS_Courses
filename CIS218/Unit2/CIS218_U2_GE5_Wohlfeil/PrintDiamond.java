/*
 * Name: Cameron Wohlfeil
 * Date: 2/6/2019
 * Description: Print Diamond of Asterisks
 *
 * 1) Ask the user for an odd number from 1 to 19.
 * 2) Loop to ensure that the user enter's an odd number between 1 and 19.
 * 3) Once you have an odd number, print out the diamond.
 */

import java.util.Scanner; // Scanner for input
import java.util.Collections; // Collections to help with output formatting
import java.lang.String; // String to help with output formatting

public class PrintDiamond {
    public static void main(String[] args) {
        // try-with-resources to automatically close scanner
        try (Scanner input = new Scanner(System.in)) {            
            // Declare and init diamond rows
            int rows = 0;
             
            // Prompt and set side length while rows are less than 1, more than 19, or even
            while (rows < 1 || rows > 19 || rows % 2 == 0) {
                System.out.print("Enter number of rows (odd number 1 to 19): "); 
                rows = input.nextInt();
            }
            
            // Loop variables
            int odd = 1, spaces = rows / 2; 
            
            // for loop to handle rows
            for (int i = 1; i <= rows; i++) {
                // for loop to handle spaces
                for (int k = spaces; k >= 1; k--)
                    System.out.print(" ");
                // for loop to handle asterisks
                for (int j = 1; j <= odd; j++)
                    System.out.print("*");
                
                // next row
                System.out.println();
                
                // if/else to handle columns
                if (i < (rows / 2 + 1)) {
                    odd += 2; // columns increasing until center row 
                    spaces -= 1; // spaces decreasing until center row 
                } else {
                    odd -= 2; // columns decreasing after center row 
                    spaces += 1; // spaces increasing after center row 
                }
            } // end for to handle rows
        } // end try
    } // end main
} // end class 