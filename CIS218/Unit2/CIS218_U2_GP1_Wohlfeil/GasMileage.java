/*
 * Name: Cameron Wohlfeil
 * Date: 2/1/2018
 * Description: Gas Mileage Calculator
 */

import java.util.Scanner; // import scanner for input

public class GasMileage {

    public static void main(String[] args) {
        int milesDriven, gallonsUsed, totalMilesDriven = 0, totalGallonsUsed = 0;
        double milesPerGallon;

        // try-with-resources statement to automatically close scanner after loops exists
        try (Scanner input = new Scanner(System.in)) {
            System.out.print("Enter miles driven (-1 to quit): ");
            milesDriven = input.nextInt();

            while (milesDriven < -1) {
                System.out.println("Error. Miles driven must be positive or -1 to quit.");
                System.out.print("Enter miles driven (-1 to quit): ");
                milesDriven = input.nextInt();
            } // end while

            while (milesDriven != -1) {
                System.out.print("Enter gallons of gas used: ");
                gallonsUsed = input.nextInt();

                while (gallonsUsed <= 0) {
                    System.out.println("Error. Gallons used must be positive.");
                    System.out.print("Enter gallons of gas used: ");
                    gallonsUsed = input.nextInt();
                } // end while

                milesPerGallon = (double) milesDriven / gallonsUsed;
                totalMilesDriven += milesDriven;
                totalGallonsUsed += gallonsUsed;

                System.out.printf("MPG this tankful: %.2f%n", milesPerGallon);

                System.out.print("Enter miles driven (-1 to quit): ");
                milesDriven = input.nextInt();

                while (milesDriven < -1) {
                    System.out.println("Error. Miles driven must be positive or -1 to quit.");
                    System.out.print("Enter miles driven (-1 to quit): ");
                    milesDriven = input.nextInt();
                } // end while
            } // end while

            if (totalGallonsUsed != 0)
                System.out.printf("Total MPG: %.2f%n", (double) totalMilesDriven / totalGallonsUsed);
        } // end try
    } // end main method
} // end class
