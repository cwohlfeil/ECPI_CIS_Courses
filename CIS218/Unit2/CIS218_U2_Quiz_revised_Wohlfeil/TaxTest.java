/*
 * Name: Cameron Wohlfeil
 * Date: 2/1/2018
 * Description: Tax Test Class
 */

import java.util.Scanner; // import scanner for input

public class TaxTest {

    public static void main(String[] args) {
        // try-with-resources statement to automatically close scanner after loops exists
        String name;
        double income, effectiveTaxRate, incomeTax, flatTax;

        try (Scanner input = new Scanner(System.in)) {
            // prompt and obtain user input
            System.out.print("Enter your name: ");
            name = input.nextLine();
            System.out.printf("%n%s, enter your yearly income: ", name);
            income = input.nextDouble();

            // Create tax object with given values
            Tax tax = new Tax(name, income);

            incomeTax = tax.calcIncomeTax();
            flatTax = tax.flatRateTax();

            // Output tax info
            System.out.printf("%n%s, you owe %,.2f", name, incomeTax);
            effectiveTaxRate = (incomeTax / income) * 100;
            System.out.printf("%nYou have an effective tax rate of  %,.2f%n", effectiveTaxRate);

            if (incomeTax < flatTax) {
                System.out.println("You would pay more with a flat tax.");
            } else if (incomeTax > flatTax) {
                System.out.println("You would pay less with a flat tax.");
            } else {
                System.out.println("You would pay the same with a flat tax.");
            }
        } // end try
    } // end main
} // end class
