/*
 * Name: Cameron Wohlfeil
 * Date: 2/1/2018
 * Description: Tax Class
 */

public class Tax {
    // instance variables
    private String name;
    private double income;

    // Invoice constructor that receives four parameters
    public Tax(String name, double income)
    {
        // Assign instance variables by using setters
        setName(name);
        setIncome(income);
    }

    // Getters and Setters

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public double getIncome() { return income; }

    public void setIncome(double income) {
        if (income > 0)
            this.income = income;
        else
            this.income = 0;
    }

    // returns tax total based on income
    public double calcIncomeTax() {
        // 10%        $1-$9,525
        // 12%        $9,526-$38,700
        // 22%        $38,701-$82,500
        // 24%        > $82,500

        double runningIncome = getIncome(), taxTotal = 0;

        if (runningIncome > 82501) {
            taxTotal += (runningIncome - 82500) * 0.24;
            runningIncome = 82500;
        }
        if (runningIncome >= 38701) {
            taxTotal += (runningIncome - 38700) * 0.22;
            runningIncome = 38700;
        }
        if (runningIncome >= 9526) {
            taxTotal += (runningIncome - 9525) * 0.12;
            runningIncome = 9525;
        }
        if (runningIncome >= 1) {
            taxTotal += runningIncome * 0.1;
            runningIncome = 0;
        }

        return taxTotal;
    }

    // returns flat tax total
    public double flatRateTax() { return getIncome() * 0.145; }
}
