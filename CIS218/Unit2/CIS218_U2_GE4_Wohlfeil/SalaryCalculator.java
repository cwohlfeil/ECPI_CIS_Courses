/*
 * Name: Cameron Wohlfeil
 * Date: 2/6/2019
 * Description: Employee Salary Calculator
 *
 * 1) Determines the gross pay for employees. 
 * 2) Pays straight time for the first 40 hours worked, time and a half otherwise.
 * 3) You’re given a list of the employees, their number of hours worked, and their hourly rates. 
 * 4) Your program should input this information for each employee, then determine and display the employee’s gross pay. 
 * 5) Use class Scanner to input the data.
 * 6) Print how many employees worked overtime and the total gross pay of all the employees.
 */

import java.util.Scanner; // Scanner for input
import java.text.NumberFormat; // NumberFormat for currency formatting
import java.util.Locale; // Locale for currency formatting

public class SalaryCalculator {
    private static double calculate_gross_pay(double hours, double salary) {
        // Function to handle the calculation of gross pay for an employee
        if (hours > 40) {
            double overtime_pay = salary * 1.5 * (hours - 40);
            return 40 * salary + overtime_pay;
        } else {
            return hours * salary;
        }
    }
    
    public static void main(String[] args) {
        // try-with-resources to automatically close scanner
        try (Scanner input = new Scanner(System.in)) {            
            // Currency formatter, US locale
            NumberFormat formatter = NumberFormat.getCurrencyInstance(Locale.US);
            
            // Variables to track state between loops
            int overtime = 0, employee_count;
            double total = 0;
            
            // Prompt and set number of employees
            System.out.println("Employee Salary Calculator"); 
            System.out.print("Enter number of employees: "); 
            employee_count = input.nextInt();
            
            // for loop to iterate through employees
            for (int i = 1; i <= employee_count; i++) {
                // declare loop variables
                double hours, salary, gross_pay;
                
                // Prompt and set hours
                System.out.printf("%nEnter hours works for Employee %d: ", i);
                hours = input.nextDouble();
                    
                // Prompt and set salary    
                System.out.printf("Enter hourly rate for Employee %d: ", i);
                salary = input.nextDouble();
                
                // Calculate, store, and output gross pay 
                gross_pay = calculate_gross_pay(hours, salary);
                System.out.printf("Gross Pay for Employee %d is %s.%n", i, 
                                  formatter.format(gross_pay));
                
                // Update state variables
                total += gross_pay;
                if (hours > 40)
                    overtime++;
            } // end for
            
            // Output state variables
            System.out.printf("%nNumber of Overtime Employees entered is %d", overtime);
            System.out.printf("%nTotal Gross Pay is %s%n", formatter.format(total));
        } // end try
    } // end main
} // end class 