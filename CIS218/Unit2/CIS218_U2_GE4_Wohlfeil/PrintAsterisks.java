/*
 * Name: Cameron Wohlfeil
 * Date: 2/6/2019
 * Description: Print Square of Asterisks
 *
 * 1) Prompts the user to enter the size of the side of a square.
 * 2) Displays a hollow square of that size made of asterisks.
 * 3) Should work for squares of all side lengths between 1 and 20.
 */

import java.util.Scanner; // Scanner for input
import java.util.Collections; // Collections to help with output formatting
import java.lang.String; // String to help with output formatting

public class PrintAsterisks {
    public static void main(String[] args) {
        // try-with-resources to automatically close scanner
        try (Scanner input = new Scanner(System.in)) {            
            // Declare and init square side length
            int length = 0;
            
            // Prompt and set side length
            while (length < 1 || length > 20) {
                System.out.print("Enter side length of square to print (1-20): "); 
                length = input.nextInt();
            }
            
            // for loop to iterate through employees
            for (int i = 1; i <= length; i++) {
                if (i == 1 || i == length)
                    System.out.println(String.join("", Collections.nCopies(length, "* ")));
                else 
                    System.out.println("*" + String.join("", Collections.nCopies(length-2, "  ")) + " *");
            } // end for
        } // end try
    } // end main
} // end class 