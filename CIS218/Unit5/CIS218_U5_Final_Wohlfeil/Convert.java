// Name: Cameron Wohlfeil
// Date:  02/22/2019
// Description
// Program converts Fahrenheit to Celsius and vice versa.
// Allows the user to enter multiple values to convert till
// they enter 3. 
// All the prints should be done in the main program not in 
// the modules

import java.util.Scanner;

public class Convert {
    // return Celsius equivalent of Fahrenheit temperature
    private static int celsius(int fahrenheitTemperature) {
        return (fahrenheitTemperature - 32) * 5 / 9;
    }

    // return Fahrenheit equivalent of Celsius temperature
    private static int fahrenheit(int celsiusTemperature) {
        return celsiusTemperature * 9 / 5 + 32;
    }

	// convert temperatures
	public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int choice; // the user's choice in the menu

        do {
            // print the menu
            System.out.println("1. Fahrenheit to Celsius");
            System.out.println("2. Celsius to Fahrenheit");
            System.out.println("3. Exit");
            System.out.print("Choice: ");
            choice = input.nextInt();

            if (choice != 3) {
                System.out.print("Enter temperature: ");
                int oldTemperature = input.nextInt();

                // convert the temperature appropriately
                switch (choice) {
                    case 1:
                        System.out.printf("%d Fahrenheit is %d Celsius%n%n",
                                oldTemperature, celsius(oldTemperature));
                        break;
                    case 2:
                        System.out.printf("%d Celsius is %d Fahrenheit%n%n",
                                oldTemperature, fahrenheit(oldTemperature));
                        break;
                }
            }
        } while (choice != 3);
    }
}
