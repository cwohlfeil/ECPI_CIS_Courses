/*
 * Name: Cameron Wohlfeil
 * Date: 02/22/2019
 * Description: Insured Patient child class
 *
 * Create a subclass of Patient, called InsuredPatient. InsuredPatient should create additional instance variables for
 * the insurance company name, and the coinsurance percentage (0 to 100%).  [20% coinsurance means the patient is
 * responsible for 20% of the bill].  Create get/sets for each instance variable.  Also override both the toString
 * method and the getAmountDue method of the super class.
 * 5) Create the appropriate constructor for InsuredPatient to be able instantiate the parent class, plus the two
 * additional instance variables for InsuredPatient. Constructors should call the sets.
 * 6) Set methods should validate that coinsurance is between 0 and 100 and that a valid string was passed in for
 * Insurance Company name.  They should throw an exception if not.
 * 7) Override the getAmountDue method from the base class, this should just return the amount the patient has due
 * after the coinsurance has been factored in.
 * 8) Override the toString method to show the additional two variables.
 */

public class InsuredPatient extends Patient {
    private String insuranceCompanyName;
    private int coinsurancePercentage;

    public InsuredPatient(String id, String name, int age, double amountDue,
                          String insuranceCompanyName, int coinsurancePercentage){
        super(id, name, age, amountDue);
        setInsuranceCompanyName(insuranceCompanyName);
        setCoinsurancePercentage(coinsurancePercentage);
    }

    public String getInsuranceCompanyName() {
        return insuranceCompanyName;
    }

    public void setInsuranceCompanyName(String insuranceCompanyName) {
        if (insuranceCompanyName == null)
            throw new IllegalArgumentException("Insurance company name must be set.");
        else
            this.insuranceCompanyName = insuranceCompanyName;
    }

    public int getCoinsurancePercentage() {
        return coinsurancePercentage;
    }

    public void setCoinsurancePercentage(int coinsurancePercentage) {
        if (coinsurancePercentage < 0 || coinsurancePercentage > 100)
            throw new IllegalArgumentException("Coinsurance percentage must be between 0-100.");
        else
            this.coinsurancePercentage = coinsurancePercentage;
    }

    @Override
    public double getAmountDue() {
        double amountDue = super.getAmountDue();
        return amountDue - (amountDue * (getCoinsurancePercentage() / 100.0));
    }

    @Override
    public String toString() {
        return String.format("%s%nInsurance Company Name: %s%nCoinsurance Percentage: %d%%",
                super.toString(), getInsuranceCompanyName(), getCoinsurancePercentage());
    }
}
