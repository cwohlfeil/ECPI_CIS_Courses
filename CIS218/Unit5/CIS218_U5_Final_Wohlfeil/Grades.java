/*
 * Name: Cameron Wohlfeil
 * Date: 02/22/2019
 * Description: Gradebook program
 *
 * 1) Create a program where you ask the instructor, how many students in his/her class.
 * 2) Then create two arrays of that size, one called grades (which is a double), and one called names (which is a String).
 * 3) In a loop with user input, fill your arrays.  Don't allow for grades < 0 or grades > 100.
 * 4) Once the instructor has entered all the grades and names,
 * allow the instructor to search through the arrays and print out the corresponding grade,
 * and whether they passed or not (passing is over 65).
 * 5) Also, print out information from their search
 * (how many were searched for, how many that were searched passed, and how many that were searched failed).
 */

import java.util.Scanner;

public class Grades {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int[] searchStats = new int[4];

        System.out.print("Enter class size: ");
        int classSize = input.nextInt();
        input.nextLine(); // flush newline

        String[] names = new String[classSize];
        int[] grades = new int[classSize];

        for (int counter = 0; counter < names.length; counter++) {
            int grade;

            System.out.printf("Enter name of student %d: ", counter+1);
            names[counter] = input.nextLine();

            do {
                System.out.printf("Enter grade of student %d: ", counter+1);
                grade = input.nextInt();
                input.nextLine(); // flush newline
            } while (grade < 0 || grade > 100);

            grades[counter] = grade;
        }


        String searchTermLower;
        do {
            System.out.print("Who do you wish to search for (Enter quit to end)? ");
            String searchTerm = input.nextLine();
            searchTermLower = searchTerm.toLowerCase();
            boolean successfulSearch = false;

            if (!searchTermLower.equals("quit")) {
                searchStats[0]++; // increment search counter

                for (int counter = 0; counter < names.length; counter++) {
                    if (names[counter].toLowerCase().equals(searchTermLower)) {
                        successfulSearch = true;

                        if (grades[counter] >= 65) {
                            System.out.printf("%s has a grade of %d and passed the class.%n",
                                    names[counter], grades[counter]);
                            searchStats[3]++; // increment passing student search counter
                        } else {
                            System.out.printf("%s has a grade of %d and did not pass the class.%n",
                                    names[counter], grades[counter]);
                            searchStats[2]++; // increment failing student search counter
                        }
                    }
                }

                if (!successfulSearch) {
                    System.out.printf("%s is not enrolled in your class. %n", searchTerm);
                    searchStats[1]++; // increment unsuccessful search counter
                }
            }
        } while (!searchTermLower.equals("quit"));

        System.out.printf("You searched for %d students. %d didn't exist, %d failed the class, and %d passed the class.%n",
                searchStats[0], searchStats[1], searchStats[2], searchStats[3]);
    }
}