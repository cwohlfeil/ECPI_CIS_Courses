/*
 * Name: Cameron Wohlfeil
 * Date: 02/22/2019
 * Description: Patient parent class
 *
 * Create a patient class for a hospital that has instance variables for patient id (String), name, age, and amount due
 * to the hospital.  Create get/sets for each instance variable and override the toString method.
 * 1) Create a constructor that takes in all the information, and call various sets to set the data.
 * 2) Sets for patient id and name should verify that some data was entered, and it should throw an Exception if not.
 * 3) Sets for age and amount due should ensure that amount entered was > 0, and throw an Exception if not.
 * 4) Override the toString method to return all the details (instance variables) of this patient.
 */

public class Patient {
    private String id, name;
    private int age;
    private double amountDue;

    public Patient(String id, String name, int age, double amountDue) {
        setId(id);
        setName(name);
        setAge(age);
        setAmountDue(amountDue);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        if (id == null)
            throw new IllegalArgumentException("ID must be set.");
        else
            this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name == null)
            throw new IllegalArgumentException("Name must be set.");
        else
            this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age < 0)
            throw new IllegalArgumentException("Age must be < 0.");
        else
            this.age = age;
    }

    public double getAmountDue() {
        return amountDue;
    }

    public void setAmountDue(double amountDue) {
        if (amountDue < 0.0)
            throw new IllegalArgumentException("Amount due must be < 0.0.");
        else
            this.amountDue = amountDue;
    }

    @Override
    public String toString() {
        return String.format("%nPatient ID: %s%nPatient Name: %s%nPatient Age: %d%nAmount Due: $%,.2f",
                             getId(), getName(), getAge(), getAmountDue());
    }
}
