/*
 * Name: Cameron Wohlfeil
 * Date: 02/22/2019
 * Description: Patient Test class
 *
 * 9)Create an Array of 10 Patients.  Ask the user if they have insurance or not, and then depending on the answer
 * create the appropriate class and put them in  your array.  Make sure you ask the user for all values that are needed
 * to fill your constructors.  Allow the user to stop entering patients before 10, by use of some sentinel.
 * 10) Once the user is done entering patients, loop through your array and print out the toString method for each
 * Patient.  Also, print out the total amount owed by all the patients, and the amount the insured patients saved by
 * having insurance. [All these calculations need to be done in this loop for full credit, i.e not done when reading in the data]
 */
import java.util.Scanner;

public class PatientTest {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        // State variables
        Patient[] patients = new Patient[10];
        int counter = 0;
        boolean sentinel = true;
        double totalOwed = 0.0, insuranceSavings = 0.0;

        while (counter < 10 && sentinel) {
            // Prompt for patient type or sentinel
            System.out.print("Does patient have insurance? (y, n, q to quit): ");
            String patientType = input.nextLine().toLowerCase();

            // Try to create objects, catch exceptions and output message
            try {
                if (patientType.startsWith("q")) {
                    // if q, change sentinel
                    sentinel = false;
                } else {
                    // Get basic patient info
                    System.out.print("Enter patient ID: ");
                    String id = input.nextLine();
                    System.out.print("Enter patient name: ");
                    String name = input.nextLine();
                    System.out.print("Enter patient age: ");
                    int age = input.nextInt();
                    System.out.print("Enter amount due: ");
                    double amountDue = input.nextDouble();
                    input.nextLine(); // flush newline

                    // If y, get additional insurance info and create object
                    if (patientType.startsWith("y")) {
                        System.out.print("Enter insurance company name: ");
                        String insuranceCompanyName = input.nextLine();
                        System.out.print("Enter coinsurance percentage: ");
                        int coinsurancePercentage = input.nextInt();
                        input.nextLine(); // flush newline

                        patients[counter] = new InsuredPatient(id, name, age, amountDue,
                                insuranceCompanyName, coinsurancePercentage);
                    } else if (patientType.startsWith("n")) {
                        // If n create object
                        patients[counter] = new Patient(id, name, age, amountDue);
                    }
                    // success, increment counter
                    counter++;
                }
            } catch (IllegalArgumentException e) {
                System.out.print(e.getMessage());
            }
        }

        // Loop through entire array
        for (Patient patient : patients) {
            // Ignore null objects
            if (patient != null) {
                // Call toString
                System.out.println(patient);
                // Add amount due to total
                totalOwed += patient.getAmountDue();

                // if InsuredPatient, calculate savings
                if (patient instanceof InsuredPatient) {
                    InsuredPatient insuredPatient = (InsuredPatient) patient;
                    double amountDue = insuredPatient.getAmountDue();
                    insuranceSavings += amountDue / (1.0 - (insuredPatient.getCoinsurancePercentage() / 100.0)) - amountDue;
                }
            }
        }

        // Output totals
        System.out.printf("%nTotal amount owed by all patients: $%,.2f%n", totalOwed);
        System.out.printf("Total amount saved by insured patients: $%,.2f%n", insuranceSavings);

        input.close(); // close scanner
    }
}
