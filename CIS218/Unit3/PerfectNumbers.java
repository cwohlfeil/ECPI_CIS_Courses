/*
 * Name: Cameron Wohlfeil
 * Date: 2/8/2019
 * Description: Perfect numbers
 *
 * 1) Write a method isPerfect that determines whether parameter number is a perfect number.
 * 2) Use this method in an application that displays all the perfect numbers between 1 and 1000.
 * 3) Display the factors of each perfect number to confirm that the number is indeed perfect.
 * 4) Challenge the computing power of your computer by testing numbers much larger than 1000. Display the results.
 */

public class PerfectNumbers {
    public static boolean isPerfect(int number) {
        int sum = 0;

        for(int i = 1; i < number; i++) {
            if (number % i == 0) {
                sum += i;
            }
        }

        return sum == number;
    }

    public static void main(String[] args) {
        for(int i = 2; i <= 10000; i++) {
            if (isPerfect(i)) {
                System.out.printf("%d is a perfect number because it is divisible by all factors: ", i);

                for (int j = 1; j < i; j++) {
                    if (i % j == 0) {
                        System.out.printf("%d ", j);
                    }
                }

                System.out.printf("%n");
            }
        }
    }
}
