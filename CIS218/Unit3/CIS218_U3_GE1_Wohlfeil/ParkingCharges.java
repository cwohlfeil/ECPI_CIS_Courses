/*
 * Name: Cameron Wohlfeil
 * Date: 2/10/2019
 * Description: Parking Charge Calculator
 *
 * 1) A parking garage charges a $2.00 minimum fee to park for up to three hours.
 * 2) The garage charges an additional $0.50 per hour for each hour or part thereof in excess of three hours.
 * 3) The maximum charge for any given 24-hour period is $10.00.
 * 4) Assume that no car parks for longer than 24 hours at a time.
 * 5) Write an application that calculates and displays the parking charges for each customer who parked in the garage yesterday.
 * 6) You should enter the hours parked for each customer. (hours > 0)
 * 7) The program should display the charge for the current customer
 * 8) The program should calculate and display the running total of yesterday’s receipts.
 * 9) The program should also print out the average charge per customer.
 * 10) It needs to use the method calculateCharges to determine the charge for each customer.
 */

import java.util.Scanner;

public class ParkingCharges {
    public static double calculateCharges(int hours) {
        // Initialize iteration variable with default fee
        double parkingCharges = 2.0;

        // If hours are greater than 3, add extra charge to default value
        if (hours > 3) {
            parkingCharges += 0.5 * (hours - 3);
        }
        // If parkingCharge is greater than 10, set it to 10
        if (parkingCharges > 10.0) {
            parkingCharges = 10.0;
        }

        return parkingCharges;
    }

    public static void main(String[] args) {
        double totalParkingCharges = 0.0;
        int hours = 0, customerCount = 0;

        Scanner input = new Scanner(System.in);

        // Continue loop until user enters quit value of -1
        while (hours != -1) {
            // Iteration variable
            double parkingCharges;

            // Prompt and input hours
            System.out.print("Enter number of hours [0-24] (-1 to quit): ");
            hours = input.nextInt();

            // While hours not between -1 and 24, print error and keep asking for hours
            while (hours > 24 || hours < -1) {
                System.out.println("Hours must be between 0 and 24.");
                System.out.print("Enter number of hours [0-24] (-1 to quit): ");
                hours = input.nextInt();
            }

            // If hours are greater than 0 continue, otherwise do nothing and finish iteration
            if (hours > 0) {
                parkingCharges = calculateCharges(hours);

                // Add current customer data to totals and output current data
                totalParkingCharges += parkingCharges;
                customerCount++;
                System.out.printf("Current charge: $%.2f, Total receipts: $%.2f%n", parkingCharges, totalParkingCharges);
            }
        }

        // Output totals
        System.out.printf("%nThere were %d customers entered, with an average charge of $%.2f.", customerCount,
                          totalParkingCharges / customerCount);

        input.close();
    }

}