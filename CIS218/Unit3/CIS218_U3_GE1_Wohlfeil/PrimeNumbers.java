/*
 * Name: Cameron Wohlfeil
 * Date: 2/13/2019
 * Description: Prime numbers
 *
 * 1) Write a method isPerfect that determines whether parameter number is a perfect number.
 * 2) Use this method in an application that displays all the perfect numbers between 1 and 1000.
 * 3) Display the factors of each perfect number to confirm that the number is indeed perfect.
 * 4) Challenge the computing power of your computer by testing numbers much larger than 1000. Display the results.
 */

public class PrimeNumbers {
    public static boolean isPrime(int number) {
        // Check if number is a multiple of 2, if it is then it can't be prime (except 2)
        if (number % 2 == 0) {
            return false;
        }

        // Otherwise, just compare it against odds, skip 1 and 2 since they're special primes
        for (int i = 3; i * i <= number; i += 2) {
            if (number % i == 0) {
                return false;
            }
        }

        // If it hasn't returned false, it's prime, so return true
        return true;
    }

    public static void main(String[] args) {
        int sum = 0, count = 0;

        for (int i = 1; i <= 10000; i++) {
            if (isPrime(i)) {
                System.out.printf("%d is a prime number.%n", i);
                sum += i;
                count++;
            }
        }

        System.out.printf("Count of primes less than 10,000: %d", count);
        System.out.printf("%nSum of primes less than 10,000: %d", sum);
    }
}
