/*
 * Name: Cameron Wohlfeil
 * Date: 2/10/2019
 * Description: Duplicate finder
 *
 * 1) Use a one-dimensional array to solve the following problem.
 * 2) Write an application that inputs five numbers, each between 10 and 100, inclusive.
 * 3) As each number is read, display it only if it’s not a duplicate of a number already read.
 * 4) Display the complete set of unique values input after the user enters each new value.
 * 5) Print out how many duplicate numbers they entered, as well as how many numbers outside the range they entered.
 */

import java.util.Scanner;

public class Duplicates {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in); // Scanner for input

        // State variables
        int[] numbers = new int[5];
        int inputNumber, outsideRangeCount = 0, duplicateCount = 0, counter = 0;


        // While the last entry in the array is zero, ie default value
        while (numbers[4] == 0) {
            // variable for tracking if value is a duplicate
            boolean dupe = false;

            System.out.print("Enter number from 10-100: ");
            inputNumber = input.nextInt();

            // while input is not within proper range, keep asking for it and update the error tracker
            while (inputNumber < 10 || inputNumber > 100) {
                System.out.print("Error, number must be between 10-100.");
                outsideRangeCount++;
                System.out.printf("%nEnter number from 10-100: ");
                inputNumber = input.nextInt();
            } // end inner while

            // Loop through array and check for duplicates, if found set dupe to true
            for (int number : numbers) {
                if (number == inputNumber) {
                    dupe = true;
                }
            }// end for

            // Check if number was a dupe, if so print out warning and add to duplicate count
            // Otherwise add to array and increment counter
            if (dupe) {
                System.out.printf("Error, %d has already been entered.%n", inputNumber);
                duplicateCount++;
            } else {
                numbers[counter] = inputNumber;
                counter++;
            }

            // Print out all numbers, ignoring zeros
            for (int number : numbers) {
                if (number != 0) {
                    System.out.printf("%d ", number);
                }
            } // end for

            System.out.println(); // newline for proper formatting
        } // end outer while

        input.close(); // Close scanner

        // Output statistics
        System.out.printf("%nYou entered %d duplicate number(s) and%n", duplicateCount);
        System.out.printf("%d number(s) outside the range of 10-100%n", outsideRangeCount);
    } // end main
} // end class