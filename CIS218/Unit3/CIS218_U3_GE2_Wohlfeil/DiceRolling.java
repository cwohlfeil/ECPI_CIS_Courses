/*
 * Name: Cameron Wohlfeil
 * Date: 2/10/2019
 * Description: Dice Roll Tally
 *
 * 1) Write an application to simulate the rolling of two dice.
 * 2) The application should use an object of class SecureRandom once to roll the first die and again to roll the second die.
 * 3) The sum of the two values should then be calculated.
 * 4) Your application should roll the dice 36,000,000 times.
 * 5) Use a one-dimensional array to tally the number of times each possible side appears.
 * 6) Display the results in tabular format.
 * 7) Show the side(s) that were rolled the least.
 * 8) Show the side(s) that were rolled the most.
 * 9) Show the amount of times they were rolled.
 */

import java.security.SecureRandom;

public class DiceRolling {
    public static void main(String[] args) {
        SecureRandom randomNumbers = new SecureRandom();
        int[] frequency = new int[13];
        int max = 0, min = 12;

        // roll 2 dice 36,000,000 times; use die value as frequency index
        for (int roll = 1; roll <= 36000000; roll++) {
            // Generate 2 random die rolls (bound of 6, 0 inclusive)
            int die1 = 1 + randomNumbers.nextInt(6);
            int die2 = 1 + randomNumbers.nextInt(6);

            // Add roll to frequency counter
            ++frequency[die1 + die2];
        }

        // Loop through all elements of frequency array, ignoring 0 and 1
        for (int face = 2; face < frequency.length; face++) {
            // If current element is larger than max, set max to the value
            if (frequency[face] > frequency[max]) {
                max = face;
            }
            // If current element is less than min, set min to the value
            if (frequency[face] < frequency[min]) {
                min = face;
            }
        }

        // Output
        System.out.printf("%s%10s%n", "Face", "Frequency");

        // output each array element's value, ignoring 0 and 1
        for (int face = 2; face < frequency.length; face++)
            System.out.printf("%4d%10d%n", face, frequency[face]);

        System.out.printf("%nMost frequent: %d, rolled %d times.", max, frequency[max]);
        System.out.printf("%nLeast frequent: %d, rolled %d times.", min, frequency[min]);
    }
}

