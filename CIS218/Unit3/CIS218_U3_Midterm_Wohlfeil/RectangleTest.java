/*
 * Name: Cameron Wohlfeil
 * Date: 2/8/2019
 * Description: Rectangle Test
 *
 * 1) Create a class Rectangle with instance variables length and width which are doubles.
 * 2) Create two constructors, one allows you to pass in a length and width, and the other just a length.
 * 3) All the constructors should call the appropriate set methods.
 * 4) Both the length and width attributes should have get/set methods.
 * 5) The set methods should verify that the numbers are larger than 0.0 and less than 20.0.
 * 6) Less than zero, make it 0. Greater than 20.0, make it 20.
 * 7) Create two methods double getPerimeter() and double getArea() that calculate and return to the calling program
 *    the perimeter and area of the rectangle.
 * 8) Create a void printRectangle() method that prints out the length, width, perimeter and area of the Rectangle.
 * 9) In your main method, show the Rectangle class being used.
 * 10) Ask the user for information, and create two Rectangles that use the different constructors.
 * 11) Call the printRectangle() method for each Rectangle.
 */

import java.util.Scanner;

public class RectangleTest {
    public static void main(String[] args) {
        // try-with-resources to automatically close scanner
        try (Scanner input = new Scanner(System.in)) {
            double length, width; // Input variable

            // Prompt and set just length, then create object with it
            System.out.print("Enter length of rectangle (double):");
            length = input.nextDouble();

            Rectangle rect1 = new Rectangle(length);

            // Prompt and set the width, then create object with both
            System.out.print("Enter width of rectangle (double):");
            width = input.nextDouble();

            Rectangle rect2 = new Rectangle(length, width);

            System.out.print("Rectangle 1");
            rect1.printRectangle();
            System.out.print("Rectangle 2");
            rect2.printRectangle();
        } // end try
    } // end main
} // end class
