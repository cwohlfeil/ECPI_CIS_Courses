/*
 * Name: Cameron Wohlfeil
 * Date: 2/8/2019
 * Description: Rectangle Class
 */

public class Rectangle {
    // instance variables
    private double length, width;

    public Rectangle (double length) {
        // Constructor when only length is given, assumed to be a square
        setLength(length);
        setWidth(length);
    }

    public Rectangle (double length, double width) {
        // Constructor when both values are given
        setLength(length);
        setWidth(width);
    }

    // Setters and getters
    public void setLength(double length) {
        if (length < 0.0) {
            this.length = 0.0;
        } else if (length > 20.0) {
            this.length = 20.0;
        } else {
            this.length = length;
        }
    }

    public double getLength() {
        return length;
    }

    public void setWidth(double width) {
        if (width < 0.0) {
            this.width = 0.0;
        } else if (width > 20.0) {
            this.width = 20.0;
        } else {
            this.width = width;
        }
    }

    public double getWidth() {
        return width;
    }

    // Methods for rectangle information
    public double getPerimeter() {
        return 2 * (getLength() + getWidth());
    }

    public double getArea() {
        return getLength() * getWidth();
    }

    public void printRectangle() {
        System.out.printf("%nWidth: %.2f", getWidth());
        System.out.printf("%nLength: %.2f", getLength());
        System.out.printf("%nPerimeter: %.2f", getPerimeter());
        System.out.printf("%nArea: %.2f%n", getArea());
    }
}
