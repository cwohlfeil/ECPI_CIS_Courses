/*
 * Name: Cameron Wohlfeil
 * Date: 1/29/2018
 * Description: BMI Calculator
 */

import java.util.Scanner; // import scanner for input

public class CIS218_U1_GE2_Part2_Wohlfeil {
    public static void main(String[] args) { // begin main
        System.out.printf("%nBMI Calculator (CTRL+D to exit)%n");
        
        // Output BMI chart
        System.out.printf("%nBMI VALUES");
        System.out.printf("%nUnderweight: less than %.1f", 18.5); 
        System.out.printf("%nNormal: between %.2f and %.1f", 18.5, 24.9); 
        System.out.printf("%nOverweight: between %d and %.1f", 25, 29.9); 
        System.out.printf("%nObese: %d or greater%n", 30);

        // try-with-resources statement to automatically close scanner after loops exists
        try(Scanner input = new Scanner(System.in)) {
            // while loop to keep program running until user exits
            while (true) {
                // declare input and output variables
                char system;
                double height, weight, bmi = 0;

                System.out.printf("%nMetric or Imperial (m or i): "); // prompt
                system = Character.toLowerCase(input.next(".").charAt(0)); // read the first char, convert to lowercase

                switch (system) {
                    case 'm':
                        System.out.print("Enter height (meters): "); // prompt
                        height = input.nextDouble(); // read height from user

                        System.out.print("Enter weight (kilograms): "); // prompt
                        weight = input.nextDouble(); // read weight from user

                        bmi = weight / (height * height);
                        break;
                    case 'i':
                        System.out.print("Enter height (inches): "); // prompt
                        height = input.nextDouble(); // read height from user

                        System.out.print("Enter weight (pounds): "); // prompt
                        weight = input.nextDouble(); // read weight from user

                        bmi = (weight * 703) / (height * height);
                        break;
                }

                System.out.printf("%nYour BMI is: %.2f", bmi);

                // If statements to output correct BMI category
                if (bmi < 18.5) {
                    System.out.printf("%nBased on your BMI, you are underweight.%n");
                } else if (bmi >= 18.5 && bmi < 25) {
                    System.out.printf("%nBased on your BMI, you are normal weight.%n");
                } else if (bmi >= 25 && bmi < 30) {
                    System.out.printf("%nBased on your BMI, you are overweight.%n");
                } else if (bmi >= 30) {
                    System.out.printf("%nBased on your BMI, you are obese.%n");
                }
            } // end while
        } // end try
    } // end main method
} // end class
