/*
 * Name: Cameron Wohlfeil
 * Date: 1/29/2018
 * Description: Arithmetic; sum, average, product, largest, and smallest.
 */

import java.util.Scanner; // import scanner for input

public class CIS218_U1_GE2_Part1_Wohlfeil {
    public static void main(String[] args) { // begin main
        Scanner input = new Scanner(System.in); // Scanner to get input from command line

        System.out.printf("%nArithmetic With Three Integers (CTRL+D to exit)%n");
        
        // while loop to keep program running until user exits
        while (true) { 
            int[] numbers = new int[3]; // array for user input numbers to compare
            // declare and initialize output variables
            int sum = 0, smallest = Integer.MAX_VALUE, largest = Integer.MIN_VALUE, product = 1;  

            System.out.print("Enter first integer: "); // prompt 
            numbers[0] = input.nextInt(); // read first number from user 

            System.out.print("Enter second integer: "); // prompt 
            numbers[1] = input.nextInt(); // read second number from user 

            System.out.print("Enter third integer: "); // prompt 
            numbers[2] = input.nextInt(); // read third number from user 

            // loop through all numbers, perform calculations and comparisons
            for (int num : numbers) { 
                sum += num;
                product *= num;
                if (num < smallest)
                    smallest = num;
                else if (num > largest)
                    largest = num;
            }

            // Outputs
            System.out.printf("%nSum: %d", sum); // output sum
            System.out.printf("%nAverage: %d", (int)Math.round(sum / numbers.length)); // calculate, round, and output average 
            System.out.printf("%nProduct: %d", product); // output product
            System.out.printf("%nLargest: %d", largest); // output largest
            System.out.printf("%nSmallest: %d%n", smallest); // output smallest
        } // end while
    } // end main method
} // end class Comparison
