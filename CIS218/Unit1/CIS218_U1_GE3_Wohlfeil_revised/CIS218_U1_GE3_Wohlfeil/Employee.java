/* 
 * Name: Cameron Wohlfeil
 * Date: 1/30/2019
 * Description: Employee class
 */

public class Employee {  
    // instance variables 
    private String firstName, lastName; 
    private double monthlySalary;

    // Employee constructor that receives three parameters and calls the set methods 
    public Employee(String firstName, String lastName, double monthlySalary) {
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setMonthlySalary(monthlySalary);
    }

    // Setters and getters
    public void setFirstName(String firstName) { this.firstName = firstName; }

    public String getFirstName() { return firstName; }
    
    public void setLastName(String lastName) { this.lastName = lastName; }

    public String getLastName() { return lastName; }
    
    public void setMonthlySalary(double monthlySalary) {
        if (monthlySalary > 0.0)
            this.monthlySalary = monthlySalary; 
    } 

    public double getMonthlySalary() { return monthlySalary; }
    
    // method that prints employee info
    public void printEmployee() {
        System.out.printf("%n%nEMPLOYEE%n");
        System.out.printf("%nFirst Name: %s", this.getFirstName());
        System.out.printf("%nLast Name: %s", this.getLastName());
        System.out.printf("%nYearly Salary: %.2f%n", this.getMonthlySalary() * 12);
    }
} // end class