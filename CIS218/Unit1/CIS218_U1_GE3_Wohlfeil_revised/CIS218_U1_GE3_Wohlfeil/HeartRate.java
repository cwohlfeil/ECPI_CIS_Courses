/* 
 * Name: Cameron Wohlfeil
 * Date: 1/30/2019
 * Description: HeartRate class
 */
import java.time.LocalDate;
import java.time.Period;

public class HeartRate {  
    // instance variables 
    private String firstName, lastName; 
    private int birthDay, birthMonth, birthYear;

    // HeartRate constructor that receives five parameters and calls the set methods 
    public HeartRate(String firstName, String lastName, int birthDay, int birthMonth, int birthYear) {
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setBirthDay(birthDay);
        this.setBirthMonth(birthMonth);
        this.setBirthYear(birthYear);
    }

    // Setters and getters
    public void setFirstName(String firstName) { this.firstName = firstName; }

    public String getFirstName() { return firstName; }

    public void setLastName(String lastName) { this.lastName = lastName; }

    public String getLastName() { return lastName; }
    
    public void setBirthDay(int birthDay) { this.birthDay = birthDay; }

    public int getBirthDay() { return birthDay; }
    
    public void setBirthMonth(int birthMonth) { this.birthMonth = birthMonth; }

    public int getBirthMonth() { return birthMonth; }
    
    public void setBirthYear(int birthYear) { this.birthYear = birthYear; }

    public int getBirthYear() { return birthYear; }
    
    // method that calculates and prints age
    public int getAge() {
        LocalDate birthDate = LocalDate.of(this.getBirthYear(), this.getBirthMonth(), this.getBirthDay());
        LocalDate currentDate = LocalDate.now();
        return Period.between(birthDate, currentDate).getYears();
    }
    
    // 220 - your age (in years)
    public double maxHeartRate() { return 220 - this.getAge(); }
    
    // 50% of maxHeartRate
    public double minTargetHeartRate() { return this.maxHeartRate() * 0.5; }

    // 85% of maxHeartRate
    public double maxTargetHeartRate() { return this.maxHeartRate() * 0.85; }

    // Prints the users first name, last name, DOB, age, maxHeartRate, and targetHeartRates
    public void printInformation() {
        System.out.printf("%n%nHEART RATE INFORMATION%n");
        System.out.printf("%nFirst Name: %s", this.getFirstName());
        System.out.printf("%nLast Name: %s", this.getLastName());
        System.out.printf("%nDate of Birth: %d/%d/%d", this.getBirthMonth(), this.getBirthDay(), this.getBirthYear());
        System.out.printf("%nAge: %d", this.getAge()); 
        System.out.printf("%nMaximum heart rate: %d", (int)this.maxHeartRate());
        System.out.printf("%nMaximum target heart rate: %d", (int)this.maxTargetHeartRate());
        System.out.printf("%nMinimum target heart rate: %d%n", (int)this.minTargetHeartRate());
    }
} // end class