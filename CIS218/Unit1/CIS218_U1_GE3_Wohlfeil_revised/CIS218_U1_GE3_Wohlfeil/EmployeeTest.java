/* 
 * Name: Cameron Wohlfeil
 * Date: 1/30/2019
 * Description: EmployeeTest class
 */
import java.util.Scanner;

public class EmployeeTest {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in); // Scanner to get input
        // Input variables
        String firstName, lastName;
        double monthlySalary, raise;
        
        // Create employee with user input
        System.out.println("Add a new employee");
        System.out.print("Enter first name: "); 
        firstName = input.nextLine(); 
        System.out.print("Enter last name: "); 
        lastName = input.nextLine();
        System.out.print("Enter monthly salary: "); 
        monthlySalary = input.nextDouble(); 
        
        Employee emp = new Employee(firstName, lastName, monthlySalary); // Create Employee object
        emp.printEmployee(); // Display employee info
        
        // Update salary with raise
        System.out.printf("%nGive employee a raise (in percent): ");
        raise = input.nextDouble();
        monthlySalary += monthlySalary / raise;
        emp.setMonthlySalary(monthlySalary);
            
        emp.printEmployee(); // Display updated employee info
        input.close(); // close scanner
    } // end main
} // end class