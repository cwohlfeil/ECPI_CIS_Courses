/* 
 * Name: Cameron Wohlfeil
 * Date: 1/30/2019
 * Description: HeartRateTest class
 */
import java.util.Scanner;

public class HeartRateTest {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in); // Scanner to get input
        // Input variables
        String firstName, lastName;
        int birthDay, birthMonth, birthYear;
        
        // Create HeartRate object with user input
        System.out.println("Heart Rate Calculator");
        System.out.println("WARNING: Always consult a physician or " + 
                           "qualified health-care professional before " +
                           "beginning or modifying an exercise program.");
        System.out.print("Enter first name: "); 
        firstName = input.nextLine(); 
        System.out.print("Enter last name: "); 
        lastName = input.nextLine();
        System.out.print("Enter birth day (day of the month, i.e. 15): "); 
        birthDay = input.nextInt();
        System.out.print("Enter birth month (i.e. 12): "); 
        birthMonth = input.nextInt(); 
        System.out.print("Enter birth year (i.e. 1990): "); 
        birthYear = input.nextInt(); 
        
        HeartRate hr = new HeartRate(firstName, lastName, birthDay, 
                                     birthMonth, birthYear); 
        hr.printInformation(); // Display info
    } // end main
} // end class