/*
   Name: Cameron Wohlfeil
   Date: 01/25/2019
   Description: Invoice class
*/

public class Invoice
{
   // instance variables
   private String partNumber;
   private String partDescription;
   private int quantity;
   private double pricePerItem;

   // Invoice constructor that receives four parameters
   public Invoice(String partNumber, String partDescription, int quantity, double pricePerItem)
   {
      // Assign instance variables by using setters
      setPartNumber(partNumber);
      setPartDescription(partDescription);
      setQuantity(quantity);
      setPricePerItem(pricePerItem);
   }

   // Setters and getters
   public void setPartNumber(String partNumber) {
      if (partNumber != "") // if the partNumber is valid
         this.partNumber = partNumber; // set it
      else // otherwise set it to ""
         this.partNumber = ""; // set it
   }

   public String getPartNumber() {
      return this.partNumber;
   }

   public void setPartDescription(String partDescription) {
      if (partDescription != "") // if the partNumber is valid
         this.partDescription = partDescription; // set it
      else // otherwise set it to ""
         this.partDescription = ""; // set it
   }

   public String getPartDescription() {
      return this.partDescription;
   }

   public void setQuantity(int quantity) {
      if (quantity > 0) // if the quantity is valid
         this.quantity = quantity; // set it
      else // otherwise set it to 0
         this.quantity = 0;
   }

   public int getQuantity() {
      return this.quantity;
   }

   public void setPricePerItem(double pricePerItem) {
      if (pricePerItem > 0.0) // if the pricePerItem is valid
         this.pricePerItem = pricePerItem; // set it
      else // otherwise set it to 0.0
         this.pricePerItem = 0.0;
   }

   public double getPricePerItem() {
      return this.pricePerItem;
   }

   // Multiply quantity by pricePerItem and return the value
   public double getInvoiceAmount() {
      return this.quantity * this.pricePerItem;
   }
} // end class Invoice

