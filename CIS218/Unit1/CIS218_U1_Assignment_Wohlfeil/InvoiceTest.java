/*
   Name: Cameron Wohlfeil
   Date: 01/25/2019
   Description: Invoice test
*/

import java.util.Scanner;

public class InvoiceTest
{
   public static void main(String[] args) 
   {
      Invoice invoice1 = new Invoice("001", "Hammer", 1, 10.00);
      Invoice invoice2 = new Invoice("002", "Nails", 100, 0.05);

      // create a Scanner to obtain input from the command window
      Scanner input = new Scanner(System.in);

      // Display initial values of each object, testing constructor and getters
      System.out.printf("Invoice 1%n" +
                      "Part number: %s%n" +
                      "Part Description %s%n" +
                      "Quantity: %s%n" +
                      "Price Per Item: %s%n" +
                      "Invoice Total: %s%n%n",
                      invoice1.getPartNumber(), invoice1.getPartDescription(), invoice1.getQuantity(),
                      invoice1.getPricePerItem(), invoice1.getInvoiceAmount());
      System.out.printf("Invoice 2%n" +
                      "Part number: %s%n" +
                      "Part Description %s%n" +
                      "Quantity: %s%n" +
                      "Price Per Item: %s%n" +
                      "Invoice Total: %s%n%n",
                      invoice2.getPartNumber(), invoice2.getPartDescription(), invoice2.getQuantity(),
                      invoice2.getPricePerItem(), invoice2.getInvoiceAmount());

      // Use scanner to test setters
      System.out.printf("%nEnter part number for invoice1: "); // prompt
      invoice1.setPartNumber(input.nextLine()); // obtain user input
      System.out.printf("Enter part description for invoice1: "); // prompt
      invoice1.setPartDescription(input.nextLine()); // obtain user input
      System.out.printf("Enter part quantity for invoice1: "); // prompt
      invoice1.setQuantity(input.nextInt()); // obtain user input
      System.out.printf("Enter part price per item for invoice1: "); // prompt
      invoice1.setPricePerItem(input.nextDouble()); // obtain user input

      System.out.printf("%nEnter part number for invoice2: "); // prompt
      invoice2.setPartNumber(input.nextLine()); // obtain user input
      System.out.printf("Enter part description for invoice2: "); // prompt
      invoice2.setPartDescription(input.nextLine()); // obtain user input
      System.out.printf("Enter part quantity for invoice2: "); // prompt
      invoice2.setQuantity(input.nextInt()); // obtain user input
      System.out.printf("Enter part price per item for invoice2: "); // prompt
      invoice2.setPricePerItem(input.nextDouble()); // obtain user input


      // Display new values of each object
      System.out.printf("Invoice 1%n" +
                      "Part number: %s%n" +
                      "Part Description %s%n" +
                      "Quantity: %s%n" +
                      "Price Per Item: %s%n" +
                      "Invoice Total: %s%n%n",
                      invoice1.getPartNumber(), invoice1.getPartDescription(), invoice1.getQuantity(),
                      invoice1.getPricePerItem(), invoice1.getInvoiceAmount());
      System.out.printf("Invoice 2%n" +
                      "Part number: %s%n" +
                      "Part Description %s%n" +
                      "Quantity: %s%n" +
                      "Price Per Item: %s%n" +
                      "Invoice Total: %s%n%n",
                      invoice2.getPartNumber(), invoice2.getPartDescription(), invoice2.getQuantity(),
                      invoice2.getPricePerItem(), invoice2.getInvoiceAmount());
   } // end main
} // end class InvoiceTest