/*
   Name: Cameron Wohlfeil
   Date: 01/25/2019
   Description: EvenOdd
*/

import java.util.Scanner;

public class EvenOdd {
    public static void main(String[] args) {
        int evenOddInput;
        Scanner input = new Scanner(System.in);

        System.out.printf("Determine if an integer is even or odd. (Press CTRL+D to exit)%n");

        // While loop to run until user stops program
        while (true) {
            // Tell user what to do and receive input
            System.out.printf("%nEnter a non-zero integer: ");
            evenOddInput = input.nextInt();

            if (evenOddInput == 0) // If zero, give error
                System.out.printf("Integer must be non-zero!%n");
            else if (evenOddInput % 2 == 0) // If divisible by two without remainder, it's even
                System.out.printf("Integer is even.");
            else // Otherwise it's odd
                System.out.printf("Integer is odd.");
        }
    }
}
