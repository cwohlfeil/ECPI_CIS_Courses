/*
    Name: Cameron Wohlfeil
    Date: 01/25/2019
    Description: Circle calculator
*/

import java.util.Scanner;
import java.lang.Math;

public class Circle {
    public static void main(String[] args) {
        double radius, area, circumference, diameter;
        Scanner input = new Scanner(System.in);

        System.out.println("Enter radius: ");
        radius = input.nextDouble();

        if (radius <= 0)
            System.out.println("Radius must be greater than 0.");
        else {
            diameter = 2 * radius;
            circumference = 2 * radius * Math.PI;
            area = Math.PI * Math.pow(radius, 2);

            System.out.printf("Radius: %.2f%n", radius);
            System.out.printf("Diameter: %.2f%n", diameter);
            System.out.printf("Circumference: %.2f%n", circumference);
            System.out.printf("Area: %.2f%n", area);
        }
    }
}
