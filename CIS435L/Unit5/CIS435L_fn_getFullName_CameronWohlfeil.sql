/* Cameron Wohlfeil */

CREATE FUNCTION fnGetFullName 
	(@FName varchar(60), @LName varchar(60))
RETURNS varchar(120) AS
BEGIN
	RETURN CONCAT(@FName, ' ', @LName)
END;