/* Cameron Wohlfeil */

CREATE VIEW EmployeeSameState AS
SELECT t1.*
FROM Employees t1 
INNER JOIN Employees t2 ON t2.State = t1.State 
WHERE t2.EmployeeID != t1.EmployeeID;