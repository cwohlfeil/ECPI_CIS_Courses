/* Cameron Wohlfeil */

CREATE PROCEDURE employeePay 
	@EmployeeID int,
	@HourlySalaryIncrease money
AS 
BEGIN
	DECLARE SpCursor CURSOR
	FOR
	SELECT EmployeeID 
	FROM Employees
	OPEN SpCursor
		FETCH NEXT FROM SpCursor
		INTO @EmployeeID
	WHILE @@FETCH_STATUS = 0
		BEGIN
		  UPDATE Employees
		  SET HourlySalary = HourlySalary + @HourlySalaryIncrease 
		  WHERE EmployeeID = @EmployeeID
		  FETCH NEXT FROM SpCursor INTO @EmployeeID
		END
CLOSE SpCursor
DEALLOCATE SpCursor
END