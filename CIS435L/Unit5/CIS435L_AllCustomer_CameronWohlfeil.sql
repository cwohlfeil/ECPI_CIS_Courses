/* Cameron Wohlfeil */

CREATE VIEW AllCustomer AS
SELECT FirstName + ' ' + LastName AS CustomerName, 
	Address, PhoneNumber
FROM Customers JOIN Deposits 
	ON Customers.CustomerID = Deposits.CustomerID
WHERE DepositAmount > 1000;