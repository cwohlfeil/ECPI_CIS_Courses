/* Cameron Wohlfeil */

SELECT AddressLine1, AddressLine2, City, PostalCode
FROM Person.Address
WHERE AddressLine2 IS NOT NULL;