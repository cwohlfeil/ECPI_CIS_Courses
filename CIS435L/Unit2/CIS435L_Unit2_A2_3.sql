/* Cameron Wohlfeil */

SELECT Name
FROM Production.Product
WHERE ProductSubcategoryID IN
	(SELECT ProductSubcategoryID 
	FROM Production.ProductSubcategory
	WHERE Name LIKE 'Bo%'); 
