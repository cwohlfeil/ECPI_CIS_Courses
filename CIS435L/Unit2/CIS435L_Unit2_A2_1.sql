/* Cameron Wohlfeil */

CREATE DATABASE Test;
GO

USE Test;

CREATE TABLE TestTable (TableID int);

INSERT INTO TestTable (TableID) VALUES
(1);

UPDATE TestTable
SET TableID = 2
WHERE TableID = 1;
