/* Cameron Wohlfeil */

SELECT Name
FROM Production.Product
WHERE ProductSubCategoryID = 
	(SELECT ProductSubcategoryID 
	FROM Production.ProductSubcategory 
	WHERE Name= 'Saddles'); 
