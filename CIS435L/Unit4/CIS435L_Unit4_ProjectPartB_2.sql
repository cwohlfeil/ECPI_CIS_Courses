/* Cameron Wohlfeil */

--Create tables

USE WBank;

CREATE TABLE Locations (
  LocationID        INT				PRIMARY KEY		IDENTITY,
  LocationCode      varchar(4)		NOT NULL		UNIQUE,
  Address           TEXT			NOT NULL,
  City				VARCHAR(50)		NOT NULL,
  State				CHAR(2)			NOT NULL
);

CREATE TABLE AccountType (
  AccountTypeID		INT            PRIMARY KEY  IDENTITY,
  AccountType       VARCHAR(60)    NOT NULL		UNIQUE
);

CREATE TABLE Employees (
  EmployeeID			INT				PRIMARY KEY		IDENTITY,
  EmployeeNumber		INT				NOT NULL		UNIQUE, 
  FirstName				VARCHAR(60)		NOT NULL,
  LastName				VARCHAR(60)		NOT NULL,
  Title					VARCHAR(60)		NOT NULL,
  CanCreateNewAccount	BIT				NOT NULL		DEFAULT 0,
  HourlySalary			MONEY			NOT NULL,
  Address				TEXT            NOT NULL,
  City					VARCHAR(60)		NOT NULL,
  State					CHAR(2)			NOT NULL,
  ZipCode				VARCHAR(10)		NOT NULL,
  EmailAddress			VARCHAR(100)	NOT NULL		UNIQUE
);

CREATE TABLE Customers (
  CustomerID		INT				PRIMARY KEY		IDENTITY,
  DateCreated		DATETIME		NOT NULL		DEFAULT GETDATE(),
  AccountNumber		INT				NOT NULL		UNIQUE,
  AccountTypeID		INT				REFERENCES AccountType (AccountTypeID),
  FirstName			VARCHAR(60)		NOT NULL,
  LastName			VARCHAR(60)		NOT NULL,
  Gender			CHAR(1)			NULL,
  Address			TEXT			NOT NULL,
  City				VARCHAR(60)		NOT NULL,
  State				CHAR(2)			NOT NULL,
  ZipCode			VARCHAR(10)		NOT NULL,
  PhoneNumber		VARCHAR(15)		NULL,
  EmailAddress		VARCHAR(100)	NULL			UNIQUE
);

CREATE TABLE Deposits (
  DepositID			INT				PRIMARY KEY		IDENTITY,
  LocationID        INT				REFERENCES Locations (LocationID),
  EmployeeID        INT				REFERENCES Employees (EmployeeID),
  CustomerID        INT				REFERENCES Customers (CustomerID),
  DepositDate       DATETIME		NOT NULL		DEFAULT GETDATE(),
  DepositAmount     MONEY			NOT NULL
);

CREATE TABLE Withdrawals (
  WithdrawalID			INT				PRIMARY KEY		IDENTITY,
  LocationID			INT				REFERENCES Locations (LocationID),
  EmployeeID			INT				REFERENCES Employees (EmployeeID),
  CustomerID			INT				REFERENCES Customers (CustomerID),
  WithdrawalDate		DATETIME		NOT NULL		DEFAULT GETDATE(),
  WithdrawalAmount		MONEY			NOT NULL,
  WithdrawalSuccesful	BIT				NOT NULL    
);

CREATE TABLE CheckCashing (
  CheckCashingID		INT				PRIMARY KEY		IDENTITY,
  LocationID			INT				REFERENCES Locations (LocationID),
  EmployeeID			INT				REFERENCES Employees (EmployeeID),
  CustomerID			INT				REFERENCES Customers (CustomerID),
  CheckCashingDate      DATETIME		NOT NULL		DEFAULT GETDATE(),
  CheckCashingAmount    MONEY			NOT NULL
);