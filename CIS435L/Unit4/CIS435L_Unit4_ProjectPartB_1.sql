/* Cameron Wohlfeil */

--Create database

IF  DB_ID('WBank') IS NOT NULL
DROP DATABASE WBank;
GO

CREATE DATABASE WBank;
GO
