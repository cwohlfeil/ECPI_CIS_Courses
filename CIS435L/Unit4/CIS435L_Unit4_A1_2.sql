USE AdventureWorks2012;
GO
SELECT ProductID, ListPrice, dbo.ufnGetProductDealerPrice(ProductID, StartDate) AS DealerPrice,
   StartDate, EndDate
FROM Production.ProductListPriceHistory
WHERE ListPrice > 3000.00
ORDER BY ProductID, StartDate; 