/* Cameron Wohlfeil */

USE AdventureWorks2012;
GO
SELECT CustomerID,('AW' + dbo.ufnLeadingZeros(CustomerID))
    AS GenerateAccountNumber
FROM Sales.Customer
ORDER BY CustomerID;
GO