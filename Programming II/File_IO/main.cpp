#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>

using namespace std;

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main() 
{
	char filename[256];
	string student;
	string line;
	char trash[2];
	float grade = 0;
	float total = 0;
	float avg = 0;
	
	cout << "Enter a file name: ";
	cin.getline(filename, 256);
	
	ofstream fileWrite(filename);
	
	if(fileWrite.is_open()) 
	{
		cout << "Enter student grade (-1 to end): ";
		cin >> grade;
		cin.getline(trash,2);
		
		while(grade >= 0)
		{
			cout << "Enter student name: ";
			getline(cin, student);
			
			fileWrite << grade << endl;
			fileWrite << student << endl;
			
			cout << "Enter student grade (-1 to end): ";
			cin >> grade;
			cin.getline(trash,2);
		}
		fileWrite.close();
	}
	else
		cout << "Error: Cannot open file." << endl;
	
	
	ifstream fileRead(filename);
	
	if(fileRead.is_open())
	{
		float currentGrade = 0;
		int count = 1;
		
		while(getline(fileRead,line))
		{
			if(!(count % 2 == 0))
			{
				line = strtof(line, 3);
				total += line;
				currentGrade = line;
			}
			else
				cout << line << ": " << currentGrade << endl;	
		
			count++;
		}
		cout << "Class Average: " << (total / (count / 2)) << endl;	
	}
	else
		cout << "Error: Cannot open file." << endl;
	
	cin.get();
	return 0;
}
