#include <iostream>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main() 
{
	int number1;
	int number2;
	int sum;
	
	std::cout << "Enter first integer: ";
	std::cin >> number1;
	
	std::cout << "Enter second integer: ";
	std::cin >> number2;
	
	sum = number1 + number2;
	
	std::cout << "Sum is " << sum <<std::endl;
}
