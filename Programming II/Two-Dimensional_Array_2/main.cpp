#include <iostream>

using namespace std;

/*******
Name: Cameron Wohlfeil
Assignment: Statistics Using an Array
Date: 02/04/2015
********/

float average(float total, int count)
{
	float avg = total / count;
	
	return avg;
}

int outputStats(int tally[], float total, int count, const char *ranges[])
{
	
	cout << "Class Statistics:" << endl;
	cout << "The class average is: " << average(total, count) << endl;
	
	for(int i = 0; i <= 9; i++)
	{
		cout << ranges[i] << ": " << tally[i] << endl;
	} 
	
}

int main() 
{
	int class[3][10] = {0};
	int test, student;
	float total = 0;
	int count = 0;
	float current = 0;
	
	cout << "Enter a grade (-1 to exit): ";
	cin >> current;
	
	while(current > -1)
	{
		if(current >= 0 && current <= 100)
			tally[(int) current / 10]++;
		
		total += current;
		count++;
		
		cout << "Enter a grade (-1 to exit): ";
		cin >> current;	   
	}
	
	outputStats(tally,total,count,ranges);	
	
	return 0;
}
