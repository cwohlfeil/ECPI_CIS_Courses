#include <iostream>

using namespace std;

/* 
Name: Cameron Wohlfeil
Exam Number: 1
Date: 2/11/2015
Program: Mileage Chart
*/

int main() 
{
	int city1;
	int city2;
	int distance[6][6] = {{0,97,90,268,262,130},{97,0,74,337,144,128},{90,74,0,354,174,201},{268,337,354,0,475,269},{262,144,174,475,0,238},{130,128,201,269,238,0}};
	const char *cities[6] = {"Daytona Beach","Gainesville","Jacksonville","Miami","Tallahassee","Tampa"};
	
	for(int i = 0; i <= 5; i++)
	{
			cout << i << ": " << cities[i] << endl;
	}
	 
	cout << "Enter the numbers of two cities to determine the mileage (-1 to quit):";
	cin >> city1 >> city2;
	
	while((city1 >= 0 && city1 <= 5) && (city2 >= 0 && city2 <= 5))
	{
		cout << "The mileage between " << cities[city1] << " and " << cities[city2] << " is: " << distance[city1][city2] << endl;
		
		cout << "Enter the numbers of two cities to determine the mileage (-1 to quit):";
		cin >> city1 >> city2;
	}
	
	cin.get();
	return 0;
}
