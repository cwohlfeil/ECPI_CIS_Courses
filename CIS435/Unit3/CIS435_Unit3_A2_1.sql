/* Cameron Wohlfeil */

SELECT VendorContactFName + ' ' + LEFT(VendorContactLName, 1) + '.' AS Contact,
	SUBSTRING(VendorPhone,7,8) AS Phone
FROM Vendors
WHERE LEFT(VendorPhone,4) = '(559'
ORDER BY Contact;