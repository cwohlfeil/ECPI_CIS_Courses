/* Cameron Wohlfeil */

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION dbo.fnUnpaidInvoiceID()
RETURNS int
WITH EXECUTE AS CALLER 
BEGIN
	RETURN 
		(SELECT InvoiceID 
		FROM Invoices
		WHERE InvoiceTotal - CreditTotal - PaymentTotal > 0 
		AND InvoiceDueDate = (SELECT MIN(InvoiceDueDate)
		FROM Invoices
		WHERE InvoiceTotal - CreditTotal - PaymentTotal > 0));
END;
