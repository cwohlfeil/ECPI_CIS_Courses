/* Cameron Wohlfeil */

DECLARE @OutstandingBalance decimal;
SET @OutstandingBalance = (SELECT SUM(InvoiceTotal-PaymentTotal) FROM Invoices);
IF @OutstandingBalance >= 10000
	SELECT VendorName, InvoiceNumber, InvoiceDueDate,  InvoiceTotal-PaymentTotal AS BalanceDue
	FROM Invoices 
	JOIN Vendors ON Invoices.VendorID = Vendors.VendorID
	WHERE InvoiceTotal-PaymentTotal > 0;
ELSE
	PRINT "Balance due is less than $10,000.00"