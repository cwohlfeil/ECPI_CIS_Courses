/* Cameron Wohlfeil */

SELECT EmailAddress, COUNT(DISTINCT ProductID) AS UniqueOrderedProducts 
FROM Customers
INNER JOIN Orders ON Orders.CustomerID = Customers.CustomerID
INNER JOIN OrderItems ON Orders.OrderID = OrderItems.OrderID
GROUP BY EmailAddress
HAVING COUNT(Orders.CustomerID) > 1;