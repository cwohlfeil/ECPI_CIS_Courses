/* Cameron Wohlfeil */
SELECT LastName, FirstName, OrderDate, ProductName, ItemPrice, DiscountAmount, Quantity
FROM Customers AS C
JOIN Orders AS O ON C.CustomerID = O.CustomerID
JOIN OrderItems AS OI ON OI.OrderID = O.OrderID 
JOIN Products AS P ON P.ProductID = OI.ProductID
ORDER BY LastName, OrderDate, ProductName;