/* Cameron Wohlfeil */

CREATE VIEW InvoiceBasic 
AS
SELECT Vendors.VendorName, InvoiceNumber, InvoiceTotal 
FROM Invoices
JOIN Vendors on Invoices.VendorID = Vendors.VendorID;
