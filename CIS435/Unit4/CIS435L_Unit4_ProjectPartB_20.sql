/* Cameron Wohlfeil */

USE MyGuitarShop
GO

DECLARE @ProductCount int;
SET @ProductCount = (SELECT COUNT(*) FROM Products);
BEGIN 
	IF @ProductCount >= 7
		PRINT 'The number of products is greater than or equal to 7.'
	ELSE
		PRINT 'The number of products is less than 7.'
END;