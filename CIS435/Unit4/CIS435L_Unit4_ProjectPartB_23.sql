/* Cameron Wohlfeil */

CREATE FUNCTION fnDiscountPrice (@ItemID int)
RETURNS money
BEGIN
    RETURN (SELECT SUM(ItemPrice-DiscountAmount) AS DiscountPrice
    FROM OrderItems
    WHERE ItemID = @ItemID);
END;
