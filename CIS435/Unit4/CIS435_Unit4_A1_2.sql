/* Cameron Wohlfeil */
SELECT [VendorName], [InvoiceNumber], [InvoiceTotal]
FROM [AP].[dbo].[InvoiceBasic]
WHERE [VendorName] LIKE '[n-p]%'
ORDER BY [VendorName];