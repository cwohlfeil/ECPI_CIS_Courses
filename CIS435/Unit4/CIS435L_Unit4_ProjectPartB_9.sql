/* Cameron Wohlfeil */

SELECT CategoryName
FROM Categories
WHERE NOT EXISTS (SELECT CategoryID FROM Products);