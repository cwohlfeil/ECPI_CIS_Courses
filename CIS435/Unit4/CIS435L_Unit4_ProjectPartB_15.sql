/* Cameron Wohlfeil */

SELECT OrderID, OrderDate, DATEADD(DAY, 2, OrderDate) AS ApproxShipDate,
ShipDate, DATEDIFF(DAY, OrderDate, ShipDate) AS DaysToShip
FROM Orders
WHERE MONTH(OrderDate) = 3 AND YEAR(OrderDate) = '2012';