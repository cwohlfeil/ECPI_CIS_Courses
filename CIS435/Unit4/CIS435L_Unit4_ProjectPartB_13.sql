/* Cameron Wohlfeil */

SELECT ListPrice, CAST(ListPrice AS decimal(18, 1)) AS ListDecimal, 
CONVERT(int, ListPrice) AS ListPriceIntConvert, CAST(ListPrice AS int) AS ListPriceIntCast
FROM Products;
