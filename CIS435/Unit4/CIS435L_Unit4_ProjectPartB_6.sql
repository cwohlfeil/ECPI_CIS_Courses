/* Cameron Wohlfeil */

SELECT OrderID, OrderDate, ShipStatus FROM
(SELECT OrderID, OrderDate, 'SHIPPED' as ShipStatus
FROM Orders 
WHERE ShipDate IS NOT NULL
UNION
SELECT OrderID, OrderDate, 'NOT SHIPPED'
FROM Orders
WHERE ShipDate IS NULL) 
AS query ORDER BY OrderDate;