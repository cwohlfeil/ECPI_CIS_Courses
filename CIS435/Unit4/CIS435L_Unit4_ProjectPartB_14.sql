/* Cameron Wohlfeil */

SELECT OrderDate, YEAR (OrderDate) AS OrderYear, DAY(OrderDate) AS OrderDay, 
DATEADD(DAY, 30, OrderDate) AS '30Day' 
FROM Orders;