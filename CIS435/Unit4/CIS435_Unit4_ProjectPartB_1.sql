/* Cameron Wohlfeil */
SELECT ItemID, ItemPrice, DiscountAmount, Quantity, ItemPrice*Quantity AS PriceTotal, 
(ItemPrice-DiscountAmount)*Quantity AS DiscountTotal
FROM OrderItems
WHERE ItemPrice*Quantity > 500
ORDER BY PriceTotal DESC;
