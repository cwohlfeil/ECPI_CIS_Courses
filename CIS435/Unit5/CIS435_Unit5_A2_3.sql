/* Cameron Wohlfeil */

CREATE TABLE NewLogins (LoginName varchar(128));

INSERT NewLogins VALUES ('BBrown'), ('CChaplin'), ('DDyer'), ('EEbbers');

DECLARE @Dynamic varchar(256), @LoginName varchar(128) ,@TempPass char(8);

DECLARE LoginCursor CURSOR DYNAMIC FOR (SELECT DISTINCT * FROM NewLogins);

OPEN LoginCursor;

FETCH NEXT FROM LoginCursor INTO @LoginName;

WHILE @@FETCH_STATUS = 0
BEGIN
	SET @TempPass = LEFT(@LoginName, 4) + '9999';
	SET @Dynamic = 'CREATE LOGIN ' + @LoginName + ' ' + 
		'WITH PASSWORD = ''' + @TempPass + ''', ' +
		'DEFAULT_DATABASE = AP';
	EXEC (@Dynamic);
	SET @Dynamic = 'CREATE USER ' + @LoginName + ' ' + 'FOR LOGIN ' + @LoginName;
	EXEC (@Dynamic);
	SET @Dynamic = 'ALTER ROLE PaymentEntry ADD MEMBER ' + @LoginName;
	EXEC (@Dynamic);
	FETCH NEXT FROM LoginCursor INTO @LoginName;
END;

CLOSE LoginCursor;

DEALLOCATE LoginCursor;