/* Cameron Wohlfeil */ 

DECLARE @VendorName varchar(50), @InvoiceAvg money;
DECLARE InvoiceAverageCursor CURSOR STATIC 
FOR
SELECT VendorName, AVG(InvoiceTotal) AS InvoiceAvg
FROM Vendors 
JOIN Invoices ON Vendors.VendorID = Invoices.VendorID
GROUP BY VendorName;

OPEN InvoiceAverageCursor;

FETCH NEXT FROM InvoiceAverageCursor INTO @VendorName, @InvoiceAvg;

WHILE @@FETCH_STATUS = 0
BEGIN
	PRINT @VendorName + ', $' + CONVERT(varchar, @InvoiceAvg, 1);
	FETCH NEXT FROM InvoiceAverageCursor;
END;

CLOSE InvoiceAverageCursor;

DEALLOCATE InvoiceAverageCursor;

