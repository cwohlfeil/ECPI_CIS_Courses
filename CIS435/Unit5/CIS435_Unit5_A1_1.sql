/* Cameron Wohlfeil */ 

DECLARE InvoiceAverageCursor CURSOR STATIC 
FOR
SELECT VendorName, AVG(InvoiceTotal) AS InvoiceAvg
FROM Vendors 
JOIN Invoices ON Vendors.VendorID = Invoices.VendorID
GROUP BY VendorName;

OPEN InvoiceAverageCursor;

FETCH NEXT FROM InvoiceAverageCursor;

WHILE @@FETCH_STATUS = 0
FETCH NEXT FROM InvoiceAverageCursor;

CLOSE InvoiceAverageCursor;

DEALLOCATE InvoiceAverageCursor;