<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>E-Mail Validator</title>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
</head>
<body>
<?php
$EmailAddresses = array(
     "john.smith@php.test",
     "mary.smith.mail.php.example",
     "john.jones@php.invalid",
     "alan.smithee@test",
     "jsmith456@example.com",
     "jsmith456@test",
     "mjones@example",
     "mjones@example.net",
     "jane.a.doe@example.org");
function validateAddress($Address) {
   if (preg_match("/^[\w-]+(\.[\w-]+)*@" .
               "[\w-]+(\.[\w-]+)*(\.[[A-Za-z]{2,})$/i",
               $Address)==1)
          return true;
     else
          return false;
}
function sortAddresses($Addresses) {
     $SortedAddresses = array();
     $iLimit = count($Addresses)-1; // Set the upper limit for the outer loop
     $jLimit = count($Addresses); // Set the upper limit for the inner loop
     for ($i = 0; $i<$iLimit; ++$i) {
          $CurrentAddress = $Addresses[$i];
          for ($j = $i+1; $j<$jLimit; ++$j) {
               if (strcasecmp($CurrentAddress,$Addresses[$j]) > 0) {
                    $TempVal = $Addresses[$j];
                    $Addresses[$j] = $CurrentAddress;
                    $CurrentAddress = $TempVal;
               }
          }
          $SortedAddresses[] = $CurrentAddress;
     }
     return($SortedAddresses);
}
$SortedAddresses = sortAddresses($EmailAddresses);
$SortedAddressList = implode(", ", $SortedAddresses);
echo "<p>Sorted Addresses: $SortedAddressList</p>\n";
foreach ($SortedAddresses as $Address) {
     if (validateAddress($Address) == false)
          echo "<p>The e-mail address <em>$Address</em> does not appear to be 
          valid.</p>\n";
}

?>
</body>
</html>

