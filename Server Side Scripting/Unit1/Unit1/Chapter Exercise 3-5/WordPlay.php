<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Word Play</title>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
</head>
<body>
<?php
$StartingText = "mAdAm, i'M aDaM.";
$UppercaseText = strtoupper($StartingText);
$LowercaseText = strtolower($StartingText);
echo "<p>$UppercaseText</p>\n";
echo "<p>$LowercaseText</p>\n";
echo "<p>" . ucfirst($LowercaseText) . "</p>\n";
echo "<p>" . lcfirst($UppercaseText) . "</p>\n";
$WorkingText = ucwords($LowercaseText);
echo "<p>$WorkingText</p>\n";
?>
</body>
</html>

