<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Upload an Image</title>
<link rel="stylesheet" type="text/css" href="ChineseZodiac.css" /> 
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
</head>
<body>
<div class="header">
<h1>Upload an Image</h1>
</div>
<div class="midblock">
<?php
function displayForm() {
?>
<form action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" method="POST" enctype="multipart/form-data">
<input type="hidden" name="MAX_FILE_SIZE" value="5000000" />
<p>Picture to Upload:
<input type="file" name="picture_file" /> (Maximum file size 5MB)
</p>
<p><input type="reset" value="Clear Form" />&nbsp; &nbsp;<input type="submit" name="submit" value="Upload Image" /></p>
</form>
<?php
}

function saveImage($Image) {
     $ImageFolderName = "./Images/";
     $retval = TRUE;
     if (is_array($Image)) {
            if ((!(empty($Image['tmp_name']) || empty($Image['name']))) &&
                      ((!isset($Image['error']) || ($Image['error']==UPLOAD_ERR_OK)))) {
                 $destName =  $ImageFolderName . $Image['name'];
                 if (move_uploaded_file($Image['tmp_name'], $destName)) { 
                      chmod($destName,0644);
                      echo "<p>&ldquo;$destName&rdquo; is a valid file, and was successfully uploaded.<br />\n";
                      $retval = TRUE;
                 }
                 else {
                      echo "<p>There was an error in the uploaded file for image &ldquo;$key&rdquo;.</p>\n";
                      $retval = FALSE;
                 }
            }
            else {
                 echo "<p>There was an error in the file upload process for image &ldquo;$key&rdquo;.</p>\n";
                 $retval = FALSE;
            }
     }
     
     return($retval);
}

function displayImage($ImageName) {
     echo "<img src='Images/" . $ImageName . 
          "' alt='" . $ImageName . 
          "' title='" . $ImageName . 
          "' /></p>\n";
}

$ShowForm = TRUE;
$errorCount = 0;
$Image = "";
if (isset($_FILES['picture_file'])) {
     if (saveImage($_FILES['picture_file'])===FALSE) {
          ++$errorCount;
          $ShowForm = TRUE;
     }
     else
          $ShowForm = FALSE;
}
else
     $ShowForm = TRUE;
          
if ($ShowForm == TRUE) {
     if ($errorCount>0) // if there were errors
          echo "<p>Please re-enter the form information below.</p>\n";
     displayForm();
} 
else {
     displayImage($_FILES['picture_file']['name']);
}

if (($ShowForm === FALSE) || ($errorCount>0)) {
   echo "<p><a href='" . $_SERVER['SCRIPT_NAME'] . "'>Upload another file</a></p>\n";
}

?>
</div>
<div class="footer"><?php include("Includes/inc_footer.php"); ?></div>
</body>
</html>

