<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Show Source Code</title>
<link rel="stylesheet" type="text/css" href="ChineseZodiac.css" /> 
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
</head>
<body>
<div class="header">
<h2>Show Source Code</h2>
</div>
<div class="midblock">
<?php

function file_get_contents($fname) {
     $retval="";
     $fp = fopen($fname,"rb");
     if ($fp===FALSE)
        $retval=false;
     else {
          while (!feof($fp)) {
               $retval .= fread($fp, 8192);
          }
          fclose($fp);
     }
     return($retval);
}

if (isset($_GET['source_file'])) {
     echo "<p style = 'text-align:center'>" . $_GET['source_file'] .
               "</p>\n";
     echo "</div>\n";
     echo "<div class='midblock'>\n";
   $SourceFile = file_get_contents(stripslashes($_GET['source_file']));
   highlight_string($SourceFile);
}
else {
     echo "</div>\n";
     echo "<div class='midblock'>\n";
     echo "<p>No source file specified. Nothing to display!</p>\n";
}

?>
</div>
<div class="footer"><?php include("Includes/inc_footer.php"); ?></div>
</body>
</html>

