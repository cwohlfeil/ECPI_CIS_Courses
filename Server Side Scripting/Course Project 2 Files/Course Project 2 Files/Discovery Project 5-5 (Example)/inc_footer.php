<p style="font-style:italic"><?php
$ProverbFileName = "proverbs.txt";
     
$ProverbArray = file($ProverbFileName, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
if ($ProverbArray===FALSE)
   echo "There are no proverbs available.\n";
else if (count($ProverbArray)==0)
   echo "There are no proverbs available.\n";
else {
   $i = rand(0,count($ProverbArray)-1);
   echo "&ldquo;" . htmlentities(trim($ProverbArray[$i])) . "&rdquo;\n";
} 
?></p>
<p>&copy; 2009</p>
