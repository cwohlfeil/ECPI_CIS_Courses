<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Upload a Proverb</title>
<link rel="stylesheet" type="text/css" href="ChineseZodiac.css" /> 
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
</head>
<body>
<div class="header">
<h1>Upload a Proverb</h1>
</div>
<div class="midblock">
<?php
$ProverbFileName = "proverbs.txt";

function validateInput($data, $fieldName) {
     global $errorCount;
     if (empty($data)) {
          echo "\"$fieldName\" is a required field.<br />\n";
          ++$errorCount;
          $retval = "";
     } else { // Only clean up the input if it isn't empty
          $retval = trim($data);
          $retval = stripslashes($retval);
     }
     return($retval);
}

function displayForm($Proverb) {
?>
<form action = "<?php echo $_SERVER['SCRIPT_NAME']; ?>" method = "post">
<p>Chinese Proverb:<br />
<textarea rows="6" cols="100" name="Proverb"><?php echo $Proverb; ?></textarea>
</p>
<p><input type="reset" value="Clear Form" />&nbsp; &nbsp;<input type="submit" name="Submit" value="Add Chinese Proverb" /></p>
</form>
<?php
}

function saveProverb($Proverb) {
     global $ProverbFileName;
     if (!empty($Proverb)) {
          if ((is_file($ProverbFileName)) && (is_writeable($ProverbFileName))) {
               $fp = fopen($ProverbFileName,"ab");
               if ($fp===FALSE) 
                    echo "<p>Cannot save the proverb, cannot open the file.</p>\n";
               else {
                    fwrite($fp,$Proverb . "\n");
                    fclose($fp);
               }
          }
          else {
               echo "<p>Cannot save the proverb, no writeable file exists.</p>\n";
          }
     }
}


function displayProverbs() {
     global $ProverbFileName;
     
     $ProverbArray = file($ProverbFileName, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
     if ($ProverbArray===FALSE)
        echo "<p>There are no proverbs available.</p>\n";
     else if (count($ProverbArray)==0)
        echo "<p>There are no proverbs available.</p>\n";
     else {
        $i = 0;
        echo "<p><dd>\n";
        foreach ($ProverbArray as $Proverb) {
           echo "<dt>Proverb " . ++$i . "</dt>\n";
           echo "<dd>" . htmlentities($Proverb) . "</dd>\n";
        }
        echo "</dd></p>\n";
        echo "<p><a href='" . $_SERVER['SCRIPT_NAME'] . 
              "'>Add another proverb</a></p>\n";
     } 
}

$ShowForm = TRUE;
$errorCount = 0;
$Proverb = "";
if (isset($_POST['Submit'])) {
     $Proverb = validateInput($_POST['Proverb'],"New Proverb");
     if ($errorCount==0) {
          saveProverb($Proverb);
          $ShowForm = FALSE;
     }
     else
          $ShowForm = TRUE;
}
if ($ShowForm == TRUE) {
     if ($errorCount>0) // if there were errors
          echo "<p>Please re-enter the form information below.</p>\n";
     displayForm($Proverb);
} 
else {
     displayProverbs();
}

?>
</div>
<div class="footer"><?php include("Includes/inc_footer.php"); ?></div>
</body>
</html>

