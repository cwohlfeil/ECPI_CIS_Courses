<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Your Chinese Zodiac Sign</title>
<link rel="stylesheet" type="text/css" href="ChineseZodiac.css" /> 
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
</head>
<body>
<div class="header">
<h1>Your Chinese Zodiac Sign</h1>
<h2>using a <span class="code">switch</span> statement</h2>
</div>
<div class="midblock">
<?php
function validateInput($data, $fieldName) {
     global $errorCount;
     if (empty($data)) {
          echo "\"$fieldName\" is a required field.<br />\n";
          ++$errorCount;
          $retval = "";
     } else { // Only clean up the input if it isn't empty
          $retval = trim($data);
          $retval = stripslashes($retval);
     }
     return($retval);
}

function displayForm($Year) {
?>
<form action = "<?php echo $_SERVER['SCRIPT_NAME']; ?>" method = "post">
<p>Your Birth Year: <input type="text" name="Year" value="<?php echo $Year; ?>" /></p>
<p><input type="reset" value="Clear Form" />&nbsp; &nbsp;<input type="submit" name="Submit" value="Show Me My Sign" /></p>
</form>
<?php
}

function file_get_contents($fname) {
     $retval="";
     $fp = fopen($fname,"rb");
     if ($fp===FALSE)
        $retval=false;
     else {
          while (!feof($fp)) {
               $retval .= fread($fp, 8192);
          }
          fclose($fp);
     }
     return($retval);
}

function file_put_contents($fname, $data) {
     $retval=true;
     $fp = fopen($fname,"wb");
     if ($fp===FALSE)
        $retval=false;
     else {
          $retval=fwrite($fp,$data);
          fclose($fp);
     }
     return($retval);
}

function StatisticsForYear($Year) {
     $retval = 0;
     $counter_file = "./statistics/BirthYear_" . $Year . "_Count.txt";
   
     if ((is_file($counter_file)) &&
               (is_readable($counter_file))) {
          $retval = file_get_contents($counter_file); 
     }
     
     ++$retval; // Add the current visitor to the total
   
   file_put_contents($counter_file, $retval);
   
   return($retval);
}

function displayResults($Year) {
     $CZIndex = ($Year+8) % 12;
     switch ($CZIndex) {
          case 0:
               echo "<p>You were born under the sign of the Rat.</p>\n";
               echo "<p><img src='Images/Rat.gif' alt='Rat' title='Rat' /></p>\n";
               break;
          case 1:
               echo "<p>You were born under the sign of the Ox.</p>\n";
               echo "<p><img src='Images/Ox.gif' alt='Ox' title='Ox' /></p>\n";
               break;
          case 2:
               echo "<p>You were born under the sign of the Tiger.</p>\n";
               echo "<p><img src='Images/Tiger.gif' alt='Tiger' title='Tiger' /></p>\n";
               break;
          case 3:
               echo "<p>You were born under the sign of the Rabbit.</p>\n";
               echo "<p><img src='Images/Rabbit.gif' alt='Rabbit' title='Rabbit' /></p>\n";
               break;
          case 4:
               echo "<p>You were born under the sign of the Dragon.</p>\n";
               echo "<p><img src='Images/Dragon.gif' alt='Dragon' title='Dragon' /></p>\n";
               break;
          case 5:
               echo "<p>You were born under the sign of the Snake.</p>\n";
               echo "<p><img src='Images/Snake.gif' alt='Snake' title='Snake' /></p>\n";
               break;
          case 6:
               echo "<p>You were born under the sign of the Horse.</p>\n";
               echo "<p><img src='Images/Horse.gif' alt='Horse' title='Horse' /></p>\n";
               break;
          case 7:
               echo "<p>You were born under the sign of the Goat.</p>\n";
               echo "<p><img src='Images/Goat.gif' alt='Goat' title='Goat' /></p>\n";
               break;
          case 8:
               echo "<p>You were born under the sign of the Monkey.</p>\n";
               echo "<p><img src='Images/Monkey.gif' alt='Monkey' title='Monkey' /></p>\n";
               break;
          case 9:
               echo "<p>You were born under the sign of the Rooster.</p>\n";
               echo "<p><img src='Images/Rooster.gif' alt='Rooster' title='Rooster' /></p>\n";
               break;
          case 10:
               echo "<p>You were born under the sign of the Dog.</p>\n";
               echo "<p><img src='Images/Dog.gif' alt='Dog' title='Dog' /></p>\n";
               break;
          case 11:
               echo "<p>You were born under the sign of the Pig.</p>\n";
               echo "<p><img src='Images/Pig.gif' alt='Pig' title='Pig' /></p>\n";
               break;
     } 
     $YearCount = StatisticsForYear($Year);
     echo "<p>You are person $YearCount to enter the year $Year.</p>\n";
     echo "<p style = 'text-align:center'><a href='index.php?page=control_structures'>Back</a></p>\n";
}

$ShowForm = TRUE;
$errorCount = 0;
$Year = date("Y");
if (isset($_POST['Submit'])) {
     $Year = validateInput($_POST['Year'],"Birth Year");
     if ($errorCount==0)
          $ShowForm = FALSE;
     else
          $ShowForm = TRUE;
}
if ($ShowForm == TRUE) {
     if ($errorCount>0) // if there were errors
          echo "<p>Please re-enter the form information below.</p>\n";
     displayForm($Year);
} 
else {
     displayResults($Year);
}

?>
</div>
<div class="footer"><?php include("Includes/inc_footer.php"); ?></div>
</body>
</html>

