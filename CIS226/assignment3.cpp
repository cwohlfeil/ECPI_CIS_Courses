#include <iostream>
#include <string>
#include <array>
#include <initializer_list>
using namespace std;

class Issue {
    public:
        // Constructors
        Issue();
        Issue(unsigned int issue_number, unsigned int ad_count, float per_ad_revenue);
        Issue(unsigned int issue_number, unsigned int ad_count);

        // Setter
        void            SetIssueNumber(unsigned int issue_number);
        void            SetAdCount(unsigned int ad_count);
        void            SetPerAdRevenue(float per_ad_revenue);

        // Getter
        unsigned int    GetIssueNumber();
        unsigned int    GetAdCount();
        float           GetPerAdRevenue();
        string          GetMotto();
        float           GetIssueRevenue();

      private:
        unsigned int    m_issue_number;
        unsigned int    m_ad_count;
        float           m_per_ad_revenue;
};

// Default Constructor
Issue::Issue() : m_issue_number{1},
                 m_ad_count{0},
                 m_per_ad_revenue{0.f}
{}

// Overloaded Constructors
Issue::Issue(unsigned int issue_number, unsigned int ad_count, float per_ad_revenue) :
	m_issue_number{ issue_number },
	m_ad_count{ ad_count },
	m_per_ad_revenue{ per_ad_revenue }
{}

Issue::Issue(unsigned int issue_number, unsigned int ad_count) : 
	m_issue_number{ issue_number },
	m_ad_count{ ad_count },
	m_per_ad_revenue{ 50 }
{}

// Setter
void Issue::SetIssueNumber(unsigned int issue_number) { m_issue_number = issue_number; }
void Issue::SetAdCount(unsigned int ad_count) { m_ad_count = ad_count; }
void Issue::SetPerAdRevenue(float per_ad_revenue) { m_per_ad_revenue = per_ad_revenue; }

// Getter
unsigned int Issue::GetIssueNumber() { return m_issue_number; }
unsigned int Issue::GetAdCount() { return m_ad_count; }
float Issue::GetPerAdRevenue() { return m_per_ad_revenue; }
string Issue::GetMotto() { return "Everything you need to know."; }
float Issue::GetIssueRevenue() { return this->m_per_ad_revenue * static_cast<float>(this->m_ad_count); }

int main() {
    array<Issue, 3> issues;

    issues[0] = ();
    issues[1] = (2, 50, 25);
    issues[2] = (3, 25);

    for (auto& issue : issues) {
        cout << "Issue Number: " << issue.GetIssueNumber() << endl;
        cout << "Ad Count: " << issue.GetAdCount() << endl;
        cout << "Per Ad Revenue: " << issue.GetPerAdRevenue() << endl;
        cout << "Total Revenue: " << issue.GetIssueRevenue() << endl;
        cout << endl;
    }

    cout << issues[0].GetMotto() << endl;

    return 0;
}