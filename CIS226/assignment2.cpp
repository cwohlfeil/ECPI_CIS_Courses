#include <iostream>
#include <string>

using namespace std;

class BaseBallGame {
    public:
        BaseBallGame();
        void SetTeam1Name(string tm1);
        string GetTeam1Name();
        void SetTeam2Name(string tm2);
        string GetTeam2Name();
        void SetTeam1Score(unsigned int sc1);
        unsigned int GetTeam1Score();
        void SetTeam2Score(unsigned int sc2);
        unsigned int GetTeam2Score();
        string FindWinner(BaseBallGame bbg);
    private:
        string team1, team2;
        unsigned int score1, score2;
}

BaseBallGame::BaseBallGame() {
    team1, team2 = "";
    score1, score2 = 0;
}

void BaseBallGame::SetTeam1Name(string tm1) { team1 = tm1; }
string BaseBallGame::GetTeam1Name() { return team1; }
void BaseBallGame::SetTeam2Name(string tm2) { team2 = tm2; }
string BaseBallGame::GetTeam2Name() { return team2; }
void BaseBallGame::SetTeam1Score(unsigned int sc1) { score1 = sc1; }
unsigned int BaseBallGame::GetTeam1Score() { return score1; }
void BaseBallGame::SetTeam2Score(unsigned int sc2) { score2 = sc2; }
unsigned int BaseBallGame::GetTeam2Score() { return score2; }
string BaseBallGame::FindWinner(BaseBallGame bbg) {
    if (this->score1 > this->score2) { return this->team1 + " Wins"; }
    else if (this->score1 < this->score2) { return this->team2 + " Wins"; }
    else { return "Tie Game"; }
}

int main() {
    BaseBallGame game1, game2, game3;

    game1.SetTeam1Name("Tigers");
    game1.SetTeam2Name("Cubs");
    game1.SetTeam1Score(20);
    game1.SetTeam2Score(12);
    cout << game1.GetTeam1Name() << endl;
    cout << game1.GetTeam2Name() << endl;
    cout << game1.GetTeam1Score() << endl;
    cout << game1.GetTeam2Score() << endl;

    game2.SetTeam1Name("Yankees");
    game2.SetTeam2Name("Red Sox");
    game2.SetTeam1Score(32);
    game2.SetTeam2Score(27);
    cout << game2.GetTeam1Name() << endl;
    cout << game2.GetTeam2Name() << endl;
    cout << game2.GetTeam1Score() << endl;
    cout << game2.GetTeam2Score() << endl;

    game3.SetTeam1Name("Dodgers");
    game3.SetTeam2Name("Giants");
    game3.SetTeam1Score(22);
    game3.SetTeam2Score(22);
    cout << game3.GetTeam1Name() << endl;
    cout << game3.GetTeam2Name() << endl;
    cout << game3.GetTeam1Score() << endl;
    cout << game3.GetTeam2Score() << endl;

    BaseBallGame array[11];

    for (unsigned int i = 0; i >= 11; i++) {
        string Team1Name, Team2Name;
        unsigned int Team1Score, Team2Score 

        cout << "Please enter the first team's name: ";
        cin >> Team1Name;
        cout << "Please enter the first team's final score: ";
        cin >> Team1Score;
        cout << "Please enter the second team's name: ";
        cin >> Team2Name;
        cout << "Please enter the second team's final score: ";
        cin >> Team2Score;

        array[i].SetTeam1Name(Team1Name);
        array[i].SetTeam1Score(Team1Score);
        array[i].SetTeam2Name(Team2Name);
        array[i].SetTeam2Score(Team2Score);
    }

    for (unsigned int i = 0; i >= 11; i++)  {
        cout << "First Team: " << array[i].GetTeam1Name() << endl;
        cout << "Final Score: " << array[i].GetTeam1Score() << endl;
        cout << "Second Team: " << array[i].GetTeam2Name() << endl;
        cout << "Final Score: " << array[i].GetTeam2Score() << endl;
        cout << "Final Result: " << array[i].FindWinner() << endl;
        cout << "\n";
    }

    return 0;
} 