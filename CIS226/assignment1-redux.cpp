#include <iostream>
#include <string>
#include <array>
#include <vector>
#include <initializer_list>
using namespace std;

enum PizzaSize : int
{
    UNSPECIFIED,
    SMALL = 12,
    MEDIUM = 14,
    LARGE = 15,
};

// Task 1
class Pizza 
{
    public:
        //ctor
        Pizza();
        
        // setter
        void            SetPrice(float price);
        void            SetTopping(const string& topping);
        void            SetToppings(initializer_list<string> toppings);
        void            SetSize(int size);
        
        // getters
        float           GetPrice();
        const string&   GetTopping();
        vector<string>& GetToppings();
        PizzaSize       GetSize(); 

        // should be static and not return *this if you dont use the return value
        static float    CalculatePrice(const Pizza& pi);

    private:
        float          m_price;
        vector<string> m_toppings;
        PizzaSize      m_size;
};

Pizza::Pizza() :
    m_price{0.f},
    m_toppings{},
    m_size{UNSPECIFIED}
{
}

// i'd move the class and funcs to a separate file

void Pizza::SetPrice(float p) 
{ 
    m_price = p;
}

float Pizza::GetPrice() 
{ 
    return m_price;
}
void Pizza::SetTopping(const string& topping)
{
    m_toppings.push_back(topping);
}
void Pizza::SetToppings(initializer_list<string> toppings)
{ 
    copy(toppings.begin(), toppings.end(), m_toppings.begin());
}
vector<string>& Pizza::GetToppings() 
{ 
    return m_toppings;
}
void Pizza::SetSize(int s) 
{
    m_size = static_cast<PizzaSize>(s); // theres a nicer way to do this, but i don't remember
}
PizzaSize Pizza::GetSize()
{ 
    return m_size;
}

const string& Pizza::GetTopping()
{
    if (m_toppings.size() != 1)
        return "There are either too little or too many toppings";
    else
        return m_toppings[0];
}

float Pizza::CalculatePrice(const Pizza& pi) 
{
    switch (pi.m_size)
    {
    case SMALL: 
        return 13.99;
    case MEDIUM: 
        return 16.99;
    case LARGE: 
        return 19.99;

    default: 
        break;
    }

    return 0.f;
}

int main() {
    // Task 2
    Pizza pizza1, pizza2;

    pizza1.SetPrice(9.99);
    pizza1.SetTopping("Pepperoni");
    pizza1.SetSize(PizzaSize::SMALL);

    pizza2.SetPrice(11.99);
    pizza2.SetTopping("Bacon");
    pizza2.SetSize(PizzaSize::MEDIUM);

    pizza1.GetPrice();
    pizza1.GetTopping();
    pizza1.GetSize();
    pizza2.GetPrice();
    pizza2.GetTopping();
    pizza2.GetSize();

    // lambdas
    auto AskOrderPrice = [ & ](float& price, string& topping, int size)
    {
        cout << "Please enter the pizza price: ";
        cin >> price;
        cout << "Please enter the pizza topping: ";
        cin >> topping;
        cout << "Please enter the pizza size: ";
        cin >> size;
    };

    auto AskOrderNoPrice = [ & ](string& topping, int size)
    {
        cout << "Please enter the pizza topping: ";
        cin >> topping;
        cout << "Please enter the pizza size: ";
        cin >> size;
    };

    // task 3
    array<Pizza, 9> pizzas1;

    for (auto& pizza : pizzas1)
    {
        float price;
        string topping;
        int size;

        AskOrderPrice(price, topping, size);

        pizza.SetPrice(price);
        pizza.SetTopping(topping);
        pizza.SetSize(size);
    }

    for (auto pizza : pizzas1)
    {
        cout << pizza.GetPrice() << endl;

        for (auto topping : pizza.GetToppings())
        {
            pizza.GetToppings().size() == 1 ? 
                cout << topping : 
                cout << topping << ", ";
        }
        cout << endl;

        cout << pizza.GetPrice() << endl;
    }


    //task 4
    array<Pizza, 9> pizzas2;

    for (auto& pizza : pizzas2)
    {
        float price;
        string topping;
        int size;

        AskOrderNoPrice(topping, size);

        price = Pizza::CalculatePrice(pizza);

        pizza.SetPrice(price);
        pizza.SetTopping(topping);
        pizza.SetSize(size);
    }
   
    for (auto pizza : pizzas2)
    {
        cout << pizza.GetPrice() << endl;

        for (auto topping : pizza.GetToppings())
        {
            pizza.GetToppings().size() == 1 ?
                cout << topping :
                cout << topping << ", ";
        }
        cout << endl;

        cout << pizza.GetPrice() << endl;
    }

    return 0;
}