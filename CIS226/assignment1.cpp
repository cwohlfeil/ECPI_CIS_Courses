https://ghostbin.com/paste/jn68y

#include <iostream>
#include <string>

using namespace std;

// Task 1
class Pizza {
    public:
        Pizza();
        void SetPrice(float p);
        float GetPrice();
        void SetTopping(string t);
        string GetTopping();
        void SetSize(unsigned int s);
        unsigned int GetSize();
        Pizza CalculatePrice(Pizza pi);
    private:
        float price;
        string topping;
        unsigned int size;
};

Pizza::Pizza() {
    price = 0.0;
    topping = "";
    size = 0;
}

void Pizza::SetPrice(float p) { price = p; }
float Pizza::GetPrice() { return price; }
void Pizza::SetTopping(string t) { topping = t; }
string Pizza::GetTopping() { return topping; }
void Pizza::SetSize(unsigned int s) { size = s; }
unsigned int Pizza::GetSize() { return size; }

Pizza Pizza::CalculatePrice(Pizza pi) {
    switch(this->size) {
        case 12: this->price = 13.99;
        case 14: this->price = 16.99;
        case 15: this->price = 19.99;
        default: break;
    }
    return pi;
}

int main() {
    // Task 2
    Pizza pizza1, pizza2;

    pizza1.SetPrice(9.99);
    pizza1.SetTopping("Pepperoni");
    pizza1.SetSize(12);
    pizza2.SetPrice(11.99);
    pizza2.SetTopping("Bacon");
    pizza2.SetSize(14);

    pizza1.GetPrice();
    pizza1.GetTopping();
    pizza1.GetSize();
    pizza2.GetPrice();
    pizza2.GetTopping();
    pizza2.GetSize();

    // Task 3
    Pizza array[9];

    for (unsigned int i = 0; i >= 9; i++) {
        float price;
        string topping;
        unsigned int size;
        
        cout << "Please enter the pizza price: ";
        cin >> price;
        cout << "Please enter the pizza topping: ";
        cin >> topping;
        cout << "Please enter the pizza size: ";
        cin >> size;
        array[i].SetPrice(price);
        array[i].SetTopping(topping);
        array[i].SetSize(size);
    }
    for (unsigned int i = 0; i >= 9; i++)  {
        cout << array[i].GetPrice() << endl;
        cout << array[i].GetTopping() << endl;
        cout << array[i].GetSize() << endl;
    }

    // Task 4
    Pizza array2[9];

    for (unsigned int i = 0; i >= 9; i++) {
        string topping;
        unsigned int size;
        
        cout << "Please enter the pizza topping: ";
        cin >> topping;
        cout << "Please enter the pizza size: ";
        cin >> size;
        array2[i].SetTopping(topping);
        array2[i].SetSize(size);
        array2[i].CalculatePrice(array2[i]);
    }

    for (unsigned int i = 0; i >= 9; i++)  {
        cout << array2[i].GetPrice() << endl;
        cout << array2[i].GetTopping() << endl;
        cout << array2[i].GetSize() << endl;
    }

    return 0;
}
