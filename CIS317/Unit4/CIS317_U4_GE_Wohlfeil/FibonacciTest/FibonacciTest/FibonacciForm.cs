﻿// CIS218 U4 Graded Exercise 
// Add validation to the Fibonacci app in Section 23.1.
// Use try/catch to make sure a number is entered in the input.
// Add a timer to check how long the asynchronous method takes.
// Add a Textbox to the form to store the number
using System;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;

namespace FibonacciTest
{
   public partial class FibonacciForm : Form
   {
      private long n1 = 0; // initialize with first Fibonacci number
      private long n2 = 1; // initialize with second Fibonacci number
      private int count = 1; // current Fibonacci number to display

      public FibonacciForm()
      {
         InitializeComponent();
      }

      // start an async Task to calculate specified Fibonacci number
      private async void calculateButton_Click(object sender, EventArgs e)
      {
         // Start the stopwatch
         var watch = System.Diagnostics.Stopwatch.StartNew();

         // retrieve user's input as an integer
         try
         {
             int number = int.Parse(inputTextBox.Text);
             asyncResultLabel.Text = "Calculating...";

             // Task to perform Fibonacci calculation in separate thread
             Task<long> fibonacciTask = Task.Run(() => Fibonacci(number));

             // wait for Task in separate thread to complete
             await fibonacciTask;

             // display result after Task in separate thread completes
             asyncResultLabel.Text = fibonacciTask.Result.ToString();
         }
         catch (ArgumentNullException)
         {
             asyncResultLabel.Text = "Enter a valid value.";
         }
         catch (ArgumentException)
         {
             asyncResultLabel.Text = "Enter a valid value.";
         }
         catch (FormatException)
         {
             asyncResultLabel.Text = "Unable to convert.";
         }   
         catch (OverflowException)
         {
             asyncResultLabel.Text = "Out of range."; 
         }
    
         // Stop the stopwatch
         watch.Stop();

         // Get the elapsed time and update the label
         TimeSpan ts = watch.Elapsed;
         label2.Text = $"Time Async: {ts.Hours:00}:{ts.Minutes:00}:{ts.Seconds:00}.{ts.Milliseconds / 10:00}";
      }

      // calculate next Fibonacci number iteratively
      private void nextNumberButton_Click(object sender, EventArgs e)
      {
         // calculate the next Fibonacci number
         long temp = n1 + n2; // calculate next Fibonacci number
         n1 = n2; // store prior Fibonacci number in n1
         n2 = temp; // store new Fibonacci
         ++count;

         // display the next Fibonacci number
         displayLabel.Text = $"Fibonacci of {count}:";
         syncResultLabel.Text = n2.ToString();
      }

      // recursive method Fibonacci; calculates nth Fibonacci number
      public long Fibonacci(long n)
      {
         if (n == 0 || n == 1)
         {
            return n;
         }
         else
         {
            return Fibonacci(n - 1) + Fibonacci(n - 2);
         }
      }
    }
}
