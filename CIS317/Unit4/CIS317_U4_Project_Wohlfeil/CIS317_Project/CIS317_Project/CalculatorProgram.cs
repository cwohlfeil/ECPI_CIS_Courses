﻿/* 
 * Name: Cameron Wohlfeil
 * Date: 01/28/2019
 * Description: CIS317 Calculator Project
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CIS317_Project
{
    static class CalculatorProgram
    {
        // TODO: Calculation code

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new CalculatorForm());
        }
    }
}
