﻿namespace CIS317_Project
{
    partial class CalculatorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.numberDisplay = new System.Windows.Forms.TextBox();
            this.logoBox = new System.Windows.Forms.PictureBox();
            this.calculatorPanel = new System.Windows.Forms.Panel();
            this.decimalButton = new System.Windows.Forms.Button();
            this.oneButton = new System.Windows.Forms.Button();
            this.zeroButton = new System.Windows.Forms.Button();
            this.negativeButton = new System.Windows.Forms.Button();
            this.equalButton = new System.Windows.Forms.Button();
            this.sixButton = new System.Windows.Forms.Button();
            this.twoButton = new System.Windows.Forms.Button();
            this.threeButton = new System.Windows.Forms.Button();
            this.subtractButton = new System.Windows.Forms.Button();
            this.addButton = new System.Windows.Forms.Button();
            this.eightButton = new System.Windows.Forms.Button();
            this.fourButton = new System.Windows.Forms.Button();
            this.multiplyButton = new System.Windows.Forms.Button();
            this.nineButton = new System.Windows.Forms.Button();
            this.fiveButton = new System.Windows.Forms.Button();
            this.sevenButton = new System.Windows.Forms.Button();
            this.divideButton = new System.Windows.Forms.Button();
            this.backButton = new System.Windows.Forms.Button();
            this.clearButton = new System.Windows.Forms.Button();
            this.ceButton = new System.Windows.Forms.Button();
            this.fractionButton = new System.Windows.Forms.Button();
            this.squaredButton = new System.Windows.Forms.Button();
            this.sqrtButton = new System.Windows.Forms.Button();
            this.percentButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.logoBox)).BeginInit();
            this.calculatorPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // numberDisplay
            // 
            this.numberDisplay.Location = new System.Drawing.Point(4, 12);
            this.numberDisplay.Name = "numberDisplay";
            this.numberDisplay.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.numberDisplay.Size = new System.Drawing.Size(218, 20);
            this.numberDisplay.TabIndex = 3;
            // 
            // logoBox
            // 
            this.logoBox.Image = global::CIS317_Project.Properties.Resources.logo;
            this.logoBox.Location = new System.Drawing.Point(4, 295);
            this.logoBox.Name = "logoBox";
            this.logoBox.Size = new System.Drawing.Size(218, 162);
            this.logoBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.logoBox.TabIndex = 1;
            this.logoBox.TabStop = false;
            // 
            // calculatorPanel
            // 
            this.calculatorPanel.Controls.Add(this.decimalButton);
            this.calculatorPanel.Controls.Add(this.oneButton);
            this.calculatorPanel.Controls.Add(this.zeroButton);
            this.calculatorPanel.Controls.Add(this.negativeButton);
            this.calculatorPanel.Controls.Add(this.equalButton);
            this.calculatorPanel.Controls.Add(this.sixButton);
            this.calculatorPanel.Controls.Add(this.twoButton);
            this.calculatorPanel.Controls.Add(this.threeButton);
            this.calculatorPanel.Controls.Add(this.subtractButton);
            this.calculatorPanel.Controls.Add(this.addButton);
            this.calculatorPanel.Controls.Add(this.eightButton);
            this.calculatorPanel.Controls.Add(this.fourButton);
            this.calculatorPanel.Controls.Add(this.multiplyButton);
            this.calculatorPanel.Controls.Add(this.nineButton);
            this.calculatorPanel.Controls.Add(this.fiveButton);
            this.calculatorPanel.Controls.Add(this.sevenButton);
            this.calculatorPanel.Controls.Add(this.divideButton);
            this.calculatorPanel.Controls.Add(this.backButton);
            this.calculatorPanel.Controls.Add(this.clearButton);
            this.calculatorPanel.Controls.Add(this.ceButton);
            this.calculatorPanel.Controls.Add(this.fractionButton);
            this.calculatorPanel.Controls.Add(this.squaredButton);
            this.calculatorPanel.Controls.Add(this.sqrtButton);
            this.calculatorPanel.Controls.Add(this.percentButton);
            this.calculatorPanel.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calculatorPanel.Location = new System.Drawing.Point(4, 47);
            this.calculatorPanel.Name = "calculatorPanel";
            this.calculatorPanel.Size = new System.Drawing.Size(218, 242);
            this.calculatorPanel.TabIndex = 2;
            // 
            // decimalButton
            // 
            this.decimalButton.BackColor = System.Drawing.SystemColors.ControlDark;
            this.decimalButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.decimalButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.decimalButton.Location = new System.Drawing.Point(112, 205);
            this.decimalButton.Name = "decimalButton";
            this.decimalButton.Size = new System.Drawing.Size(50, 35);
            this.decimalButton.TabIndex = 23;
            this.decimalButton.Text = ".";
            this.decimalButton.UseVisualStyleBackColor = false;
            this.decimalButton.Click += new System.EventHandler(this.number_Click);
            // 
            // oneButton
            // 
            this.oneButton.BackColor = System.Drawing.SystemColors.ControlDark;
            this.oneButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.oneButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.oneButton.Location = new System.Drawing.Point(112, 164);
            this.oneButton.Name = "oneButton";
            this.oneButton.Size = new System.Drawing.Size(50, 35);
            this.oneButton.TabIndex = 22;
            this.oneButton.Text = "1";
            this.oneButton.UseVisualStyleBackColor = false;
            this.oneButton.Click += new System.EventHandler(this.number_Click);
            // 
            // zeroButton
            // 
            this.zeroButton.BackColor = System.Drawing.SystemColors.ControlDark;
            this.zeroButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.zeroButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.zeroButton.Location = new System.Drawing.Point(56, 205);
            this.zeroButton.Name = "zeroButton";
            this.zeroButton.Size = new System.Drawing.Size(50, 35);
            this.zeroButton.TabIndex = 21;
            this.zeroButton.Text = "0";
            this.zeroButton.UseVisualStyleBackColor = false;
            this.zeroButton.Click += new System.EventHandler(this.number_Click);
            // 
            // negativeButton
            // 
            this.negativeButton.BackColor = System.Drawing.SystemColors.ControlDark;
            this.negativeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.negativeButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.negativeButton.Location = new System.Drawing.Point(0, 205);
            this.negativeButton.Name = "negativeButton";
            this.negativeButton.Size = new System.Drawing.Size(50, 35);
            this.negativeButton.TabIndex = 20;
            this.negativeButton.Text = "±";
            this.negativeButton.UseVisualStyleBackColor = false;
            this.negativeButton.Click += new System.EventHandler(this.negativeButton_Click);
            // 
            // equalButton
            // 
            this.equalButton.BackColor = System.Drawing.SystemColors.ControlDark;
            this.equalButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.equalButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.equalButton.Location = new System.Drawing.Point(168, 205);
            this.equalButton.Name = "equalButton";
            this.equalButton.Size = new System.Drawing.Size(50, 35);
            this.equalButton.TabIndex = 19;
            this.equalButton.Text = "=";
            this.equalButton.UseVisualStyleBackColor = false;
            this.equalButton.Click += new System.EventHandler(this.equalButton_Click);
            // 
            // sixButton
            // 
            this.sixButton.BackColor = System.Drawing.SystemColors.ControlDark;
            this.sixButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sixButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.sixButton.Location = new System.Drawing.Point(112, 123);
            this.sixButton.Name = "sixButton";
            this.sixButton.Size = new System.Drawing.Size(50, 35);
            this.sixButton.TabIndex = 18;
            this.sixButton.Text = "6";
            this.sixButton.UseVisualStyleBackColor = false;
            this.sixButton.Click += new System.EventHandler(this.number_Click);
            // 
            // twoButton
            // 
            this.twoButton.BackColor = System.Drawing.SystemColors.ControlDark;
            this.twoButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.twoButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.twoButton.Location = new System.Drawing.Point(56, 164);
            this.twoButton.Name = "twoButton";
            this.twoButton.Size = new System.Drawing.Size(50, 35);
            this.twoButton.TabIndex = 17;
            this.twoButton.Text = "2";
            this.twoButton.UseVisualStyleBackColor = false;
            this.twoButton.Click += new System.EventHandler(this.number_Click);
            // 
            // threeButton
            // 
            this.threeButton.BackColor = System.Drawing.SystemColors.ControlDark;
            this.threeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.threeButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.threeButton.Location = new System.Drawing.Point(0, 164);
            this.threeButton.Name = "threeButton";
            this.threeButton.Size = new System.Drawing.Size(50, 35);
            this.threeButton.TabIndex = 16;
            this.threeButton.Text = "3";
            this.threeButton.UseVisualStyleBackColor = false;
            this.threeButton.Click += new System.EventHandler(this.number_Click);
            // 
            // subtractButton
            // 
            this.subtractButton.BackColor = System.Drawing.SystemColors.ControlDark;
            this.subtractButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.subtractButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.subtractButton.Location = new System.Drawing.Point(168, 123);
            this.subtractButton.Name = "subtractButton";
            this.subtractButton.Size = new System.Drawing.Size(50, 35);
            this.subtractButton.TabIndex = 15;
            this.subtractButton.Text = "−";
            this.subtractButton.UseVisualStyleBackColor = false;
            this.subtractButton.Click += new System.EventHandler(this.operator_Click);
            // 
            // addButton
            // 
            this.addButton.BackColor = System.Drawing.SystemColors.ControlDark;
            this.addButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.addButton.Location = new System.Drawing.Point(168, 164);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(50, 35);
            this.addButton.TabIndex = 14;
            this.addButton.Text = "+";
            this.addButton.UseVisualStyleBackColor = false;
            this.addButton.Click += new System.EventHandler(this.operator_Click);
            // 
            // eightButton
            // 
            this.eightButton.BackColor = System.Drawing.SystemColors.ControlDark;
            this.eightButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.eightButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.eightButton.Location = new System.Drawing.Point(56, 82);
            this.eightButton.Name = "eightButton";
            this.eightButton.Size = new System.Drawing.Size(50, 35);
            this.eightButton.TabIndex = 13;
            this.eightButton.Text = "8";
            this.eightButton.UseVisualStyleBackColor = false;
            this.eightButton.Click += new System.EventHandler(this.number_Click);
            // 
            // fourButton
            // 
            this.fourButton.BackColor = System.Drawing.SystemColors.ControlDark;
            this.fourButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.fourButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.fourButton.Location = new System.Drawing.Point(0, 123);
            this.fourButton.Name = "fourButton";
            this.fourButton.Size = new System.Drawing.Size(50, 35);
            this.fourButton.TabIndex = 12;
            this.fourButton.Text = "4";
            this.fourButton.UseVisualStyleBackColor = false;
            this.fourButton.Click += new System.EventHandler(this.number_Click);
            // 
            // multiplyButton
            // 
            this.multiplyButton.BackColor = System.Drawing.SystemColors.ControlDark;
            this.multiplyButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.multiplyButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.multiplyButton.Location = new System.Drawing.Point(168, 82);
            this.multiplyButton.Name = "multiplyButton";
            this.multiplyButton.Size = new System.Drawing.Size(50, 35);
            this.multiplyButton.TabIndex = 11;
            this.multiplyButton.Text = "×";
            this.multiplyButton.UseVisualStyleBackColor = false;
            this.multiplyButton.Click += new System.EventHandler(this.operator_Click);
            // 
            // nineButton
            // 
            this.nineButton.BackColor = System.Drawing.SystemColors.ControlDark;
            this.nineButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.nineButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.nineButton.Location = new System.Drawing.Point(112, 82);
            this.nineButton.Name = "nineButton";
            this.nineButton.Size = new System.Drawing.Size(50, 35);
            this.nineButton.TabIndex = 10;
            this.nineButton.Text = "9";
            this.nineButton.UseVisualStyleBackColor = false;
            this.nineButton.Click += new System.EventHandler(this.number_Click);
            // 
            // fiveButton
            // 
            this.fiveButton.BackColor = System.Drawing.SystemColors.ControlDark;
            this.fiveButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.fiveButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.fiveButton.Location = new System.Drawing.Point(56, 123);
            this.fiveButton.Name = "fiveButton";
            this.fiveButton.Size = new System.Drawing.Size(50, 35);
            this.fiveButton.TabIndex = 9;
            this.fiveButton.Text = "5";
            this.fiveButton.UseVisualStyleBackColor = false;
            this.fiveButton.Click += new System.EventHandler(this.number_Click);
            // 
            // sevenButton
            // 
            this.sevenButton.BackColor = System.Drawing.SystemColors.ControlDark;
            this.sevenButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sevenButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.sevenButton.Location = new System.Drawing.Point(0, 82);
            this.sevenButton.Name = "sevenButton";
            this.sevenButton.Size = new System.Drawing.Size(50, 35);
            this.sevenButton.TabIndex = 8;
            this.sevenButton.Text = "7";
            this.sevenButton.UseVisualStyleBackColor = false;
            this.sevenButton.Click += new System.EventHandler(this.number_Click);
            // 
            // divideButton
            // 
            this.divideButton.BackColor = System.Drawing.SystemColors.ControlDark;
            this.divideButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.divideButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.divideButton.Location = new System.Drawing.Point(168, 41);
            this.divideButton.Name = "divideButton";
            this.divideButton.Size = new System.Drawing.Size(50, 35);
            this.divideButton.TabIndex = 7;
            this.divideButton.Text = "÷";
            this.divideButton.UseVisualStyleBackColor = false;
            this.divideButton.Click += new System.EventHandler(this.operator_Click);
            // 
            // backButton
            // 
            this.backButton.BackColor = System.Drawing.SystemColors.ControlDark;
            this.backButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.backButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.backButton.Location = new System.Drawing.Point(112, 41);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(50, 35);
            this.backButton.TabIndex = 6;
            this.backButton.Text = "⌫";
            this.backButton.UseVisualStyleBackColor = false;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // clearButton
            // 
            this.clearButton.BackColor = System.Drawing.SystemColors.ControlDark;
            this.clearButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.clearButton.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clearButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.clearButton.Location = new System.Drawing.Point(56, 41);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(50, 35);
            this.clearButton.TabIndex = 5;
            this.clearButton.Text = "C";
            this.clearButton.UseVisualStyleBackColor = false;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // ceButton
            // 
            this.ceButton.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ceButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ceButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.ceButton.Location = new System.Drawing.Point(0, 41);
            this.ceButton.Name = "ceButton";
            this.ceButton.Size = new System.Drawing.Size(50, 35);
            this.ceButton.TabIndex = 4;
            this.ceButton.Text = "CE";
            this.ceButton.UseVisualStyleBackColor = false;
            this.ceButton.Click += new System.EventHandler(this.ceButton_Click);
            // 
            // fractionButton
            // 
            this.fractionButton.BackColor = System.Drawing.SystemColors.ControlDark;
            this.fractionButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.fractionButton.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fractionButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.fractionButton.Location = new System.Drawing.Point(168, 0);
            this.fractionButton.Name = "fractionButton";
            this.fractionButton.Size = new System.Drawing.Size(50, 35);
            this.fractionButton.TabIndex = 3;
            this.fractionButton.Text = "¹/₍ₓ₎";
            this.fractionButton.UseVisualStyleBackColor = false;
            this.fractionButton.Click += new System.EventHandler(this.fractionButton_Click);
            // 
            // squaredButton
            // 
            this.squaredButton.BackColor = System.Drawing.SystemColors.ControlDark;
            this.squaredButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.squaredButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.squaredButton.Location = new System.Drawing.Point(112, 0);
            this.squaredButton.Name = "squaredButton";
            this.squaredButton.Size = new System.Drawing.Size(50, 35);
            this.squaredButton.TabIndex = 2;
            this.squaredButton.Text = "x²";
            this.squaredButton.UseVisualStyleBackColor = false;
            this.squaredButton.Click += new System.EventHandler(this.squaredButton_Click);
            // 
            // sqrtButton
            // 
            this.sqrtButton.BackColor = System.Drawing.SystemColors.ControlDark;
            this.sqrtButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sqrtButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.sqrtButton.Location = new System.Drawing.Point(56, 0);
            this.sqrtButton.Name = "sqrtButton";
            this.sqrtButton.Size = new System.Drawing.Size(50, 35);
            this.sqrtButton.TabIndex = 1;
            this.sqrtButton.Text = "√";
            this.sqrtButton.UseVisualStyleBackColor = false;
            this.sqrtButton.Click += new System.EventHandler(this.sqrtButton_Click);
            // 
            // percentButton
            // 
            this.percentButton.BackColor = System.Drawing.SystemColors.ControlDark;
            this.percentButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.percentButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.percentButton.Location = new System.Drawing.Point(0, 0);
            this.percentButton.Name = "percentButton";
            this.percentButton.Size = new System.Drawing.Size(50, 35);
            this.percentButton.TabIndex = 0;
            this.percentButton.Text = "%";
            this.percentButton.UseVisualStyleBackColor = false;
            this.percentButton.Click += new System.EventHandler(this.percentButton_Click);
            // 
            // CalculatorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(226, 461);
            this.Controls.Add(this.numberDisplay);
            this.Controls.Add(this.logoBox);
            this.Controls.Add(this.calculatorPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "CalculatorForm";
            this.Text = "CalculatorForm";
            ((System.ComponentModel.ISupportInitialize)(this.logoBox)).EndInit();
            this.calculatorPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button percentButton;
        private System.Windows.Forms.PictureBox logoBox;
        private System.Windows.Forms.Panel calculatorPanel;
        private System.Windows.Forms.TextBox numberDisplay;
        private System.Windows.Forms.Button sqrtButton;
        private System.Windows.Forms.Button fractionButton;
        private System.Windows.Forms.Button squaredButton;
        private System.Windows.Forms.Button decimalButton;
        private System.Windows.Forms.Button oneButton;
        private System.Windows.Forms.Button zeroButton;
        private System.Windows.Forms.Button negativeButton;
        private System.Windows.Forms.Button equalButton;
        private System.Windows.Forms.Button sixButton;
        private System.Windows.Forms.Button twoButton;
        private System.Windows.Forms.Button threeButton;
        private System.Windows.Forms.Button subtractButton;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Button eightButton;
        private System.Windows.Forms.Button fourButton;
        private System.Windows.Forms.Button multiplyButton;
        private System.Windows.Forms.Button nineButton;
        private System.Windows.Forms.Button fiveButton;
        private System.Windows.Forms.Button sevenButton;
        private System.Windows.Forms.Button divideButton;
        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.Button ceButton;
    }
}

