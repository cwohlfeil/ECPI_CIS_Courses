﻿/* 
 * Name: Cameron Wohlfeil
 * Date: 02/17/2019
 * Description: CIS317 Calculator Project
 */

using System;
using System.Windows.Forms;

namespace CIS317_Project
{
    public partial class CalculatorForm : Form
    {
        // Initialize form and components
        public CalculatorForm()
        {
            InitializeComponent();
        }

        private const string V = "0";
        private const string V1 = ".";
        private const string V2 = "Error, unable to parse number.";

        // Instance variables for calculations
        private double val = 0; // Double class to use methods
        private string operation = "";
        private bool operator_pressed = false;
        private int dotCount = 0;


        // private double parseTextBox (string text)
        // {
            // TODO: Replace all parsing with this function to improve code reuse
        // }

        private void operator_Click(object sender, EventArgs e)
        {
            // Generic function to handle click on all operator buttons
            var b = (Button)sender;
            operation = b.Text;
            try
            {
                val = double.Parse(numberDisplay.Text);
                operator_pressed = true;
            }
            catch
            {
                numberDisplay.Text = V2;
            }
        }

        private void number_Click(object sender, EventArgs e)
        {
            // Generic function to handle click on all number and decimal buttons
            if ((numberDisplay.Text == V) || operator_pressed)
            {
                // Handle some error cases
                numberDisplay.Clear();
                dotCount = 0;
            }

            operator_pressed = false;
            var b = (Button)sender;

            if (b.Text == V1 && dotCount < 1)
            {
                // If a decimal point is added to the text box, handle it appropriately
                dotCount++;
                numberDisplay.Text += b.Text;
            }
            if (b.Text != V1)
            {
                numberDisplay.Text += b.Text;
            }
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            // Clear display and everything
            numberDisplay.Text = V;
            dotCount = 0;
        }

        private void ceButton_Click(object sender, EventArgs e)
        {
            // Clear display and last value, but not previous value
            numberDisplay.Text = V;
            val = 0;
            dotCount = 0;
        }

        private void equalButton_Click(object sender, EventArgs e)
        {
            // Perform correct operation and output solution
            switch (operation)
            {
                // Make sure to use Unicode characters in the case!
                case "+":
                    numberDisplay.Text = (val + double.Parse(numberDisplay.Text)).ToString();
                    break;
                case "−":
                    numberDisplay.Text = (val - double.Parse(numberDisplay.Text)).ToString();
                    break;
                case "×":
                    numberDisplay.Text = (val * double.Parse(numberDisplay.Text)).ToString();
                    break;
                case "÷":
                    numberDisplay.Text = (val / double.Parse(numberDisplay.Text)).ToString();
                    break;
                default:
                    break;
            }

            try
            {
                // Attempt to parse number in text display
                var ans = double.Parse(numberDisplay.Text);
                
                // check if should be output with or without decimal
                dotCount = ans == ans ? 1 : 0;
            }
            catch
            {
                numberDisplay.Text = V2;
            }
        }

        private void negativeButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Attempt to parse number and multiply it by -1 to flip sign
                var number = double.Parse(numberDisplay.Text) * -1;
                numberDisplay.Text = number.ToString();
            }
            catch
            {
                numberDisplay.Text = V2;
            }
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            // Clear display and last value, but not previous value
            numberDisplay.Text = V;
            val = 0;
            dotCount = 0;
        }

        private void fractionButton_Click(object sender, EventArgs e)
        {

        }

        private void squaredButton_Click(object sender, EventArgs e)
        {

        }

        private void sqrtButton_Click(object sender, EventArgs e)
        {

        }

        private void percentButton_Click(object sender, EventArgs e)
        {

        }
    }
}
