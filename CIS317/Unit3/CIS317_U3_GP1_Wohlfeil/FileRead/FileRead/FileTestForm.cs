﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace FileTest
{
    // displays contents of files and directories
    public partial class FileTestForm : Form
    {
        // Default constructor
        public FileTestForm()
        {
            InitializeComponent();
        } // end constructor

        private void inputTextBox_TextChanged(object sender, EventArgs e)
        {

        }
        // Invoked when key is pressed
        private void inputTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            // Determine if use pressed Enter key
            if (e.KeyCode == Keys.Enter)
            {
                // Get user specified file or directory
                string fileName = inputTextBox.Text;

                // Determine is fileName is a file
                if (File.Exists(fileName))
                {
                    // Get file creation date, modification date, etc.
                    GetInformation(fileName);
                    StreamReader stream = null; // Declare StreamReader

                    try
                    {
                        // Obtain reader and file contents
                        using (stream = new StreamReader(fileName))
                        {
                            outputTextBox.AppendText(stream.ReadToEnd());
                        } // end using
                    } // end try
                    catch (IOException)
                    {
                        MessageBox.Show("Error reading from file", "File Error",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    } // end catch
                } // end if 
                // determine whether fileName is a directory
                else if (Directory.Exists(fileName))
                {
                    // Get directory creation date, modification date, etc.
                    GetInformation(fileName);

                    // obtain directory list
                    string[] directoryList = Directory.GetDirectories(fileName);

                    outputTextBox.AppendText("Directory contents:\n");

                    // output directoryList
                    foreach (var directory in directoryList)
                        outputTextBox.AppendText(directory + "\n");
                } // end else if
                else
                {
                    // notify user that neither exists
                    MessageBox.Show(inputTextBox.Text + " does not exist", "File Error",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                } // end else
            } // end if
        } // end method inputTextBox_KeyDown

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void GetInformation(string fileName)
        {
            outputTextBox.Clear();
            outputTextBox.AppendText(fileName + " exists\n");
        }
    }
}
