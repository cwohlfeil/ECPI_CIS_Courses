﻿using System;
using System.Windows.Forms;
using System.IO;
using BankLibrary;

namespace ReadSequentialAccessFile
{
    public partial class ReadSequentialAccessFileForm : BankUIForm
    {
        private StreamReader fileReader;

        // default constructor
        public ReadSequentialAccessFileForm()
        {
            InitializeComponent();
        }

        // when user clicks the Open button
        private void openButton_Click(object sender, EventArgs e)
        {
            // create and show dialog box enabling user to open file         
            DialogResult result; 
            string fileName;

            using (OpenFileDialog fileChooser = new OpenFileDialog())
            {
                result = fileChooser.ShowDialog();
                fileName = fileChooser.FileName;
            }

            // ensure that user clicked "OK"
            if (result == DialogResult.OK)
            {
                ClearTextBoxes();

                // show error if user specified invalid file
                if (string.IsNullOrEmpty(fileName))
                {
                    MessageBox.Show("Invalid File Name", "Error",
                       MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    try
                    {
                        // create FileStream to obtain read access to file
                        FileStream input = new FileStream(
                           fileName, FileMode.Open, FileAccess.Read);

                        // set file from where data is read
                        fileReader = new StreamReader(input);

                        openButton.Enabled = false; // disable Open File button
                        nextButton.Enabled = true; // enable Next Record button
                    }
                    catch (IOException)
                    {
                        MessageBox.Show("Error reading from file","File Error", 
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        // invoked when user clicks Next button
        private void nextButton_Click(object sender, EventArgs e)
        {
            try
            {
                // get next record available in file
                var inputRecord = fileReader.ReadLine();

                if (inputRecord != null)
                {
                    string[] inputFields = inputRecord.Split(',');

                    // copy string-array values to TextBox values
                    SetTextBoxValues(inputFields);
                }
                else
                {
                    // close StreamReader and file
                    fileReader.Close();
                    openButton.Enabled = true; // enable Open File button
                    nextButton.Enabled = false; // disable Next Record button
                    ClearTextBoxes();

                    // notify user if no records in file
                    MessageBox.Show("No more records in file", string.Empty,
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (IOException)
            {
                MessageBox.Show("Error Reading from File", "Error",
                   MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}