// Cameron Wohlfeil
// CIS317
// 01/25/2019
// Unit 1 Guided Practice 4

using System;

class StringConcatenation
{
   static void Main()
   {
      var string1 = "Happy ";
      var string2 = "Birthday";

      // Use '$' to take advantage of C#6 string interpolation 
      Console.WriteLine($"string1 = \"{string1}\"");
      Console.WriteLine($"string2 = \"{string2}\"");
      Console.WriteLine("\nResult of string.Concat(string1, string2) = " +
                        string.Concat(string1, string2));
      Console.WriteLine($"string1 after concatenation = {string1}");
    } // end main
} // end class
