// Cameron Wohlfeil
// CIS317
// 01/25/2019
// Unit 1 Graded Exercise

using System;

public class StringComparison
{
    public static void Main()
    {
        string line1, line2;
        int comparison;
        
        Console.WriteLine("\nCIS317 Unit 1 Graded Exercise: Compare Strings (press CTRL+Z to exit)\n");

        // Infinite while loop to keep running until user exits with CTRL+Z or CTRL+C
        while (true) {
            // Output instructions to console, read lines from console
            Console.WriteLine("Type the first string (press enter to continue):");
            line1 = Console.ReadLine();
            Console.WriteLine("Enter the second string (press enter to continue):");
            line2 = Console.ReadLine();   

            // Store results of string comparison
            comparison = line1.CompareTo(line2);

            // switch to determine correct output for result of comparison
            switch (comparison) 
            {
                case 1: 
                    Console.WriteLine($"The first string, \"{line1}\", is greater than the second string, \"{line2}\".\n");
                    break;
                case -1:
                    Console.WriteLine($"The first string, \"{line1}\", is less than the second string, \"{line2}\".\n");
                    break;
                case 0:
                    Console.WriteLine($"The first string, \"{line1}\", is equal to the second string, \"{line2}\".\n");
                    break;
            }
        } 
    }
}