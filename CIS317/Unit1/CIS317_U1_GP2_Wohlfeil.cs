// Cameron Wohlfeil
// CIS317
// 01/25/2019
// Unit 1 Guided Practice 2

using System;

class StringCompare
{
    static void Main(string[] args)
    {
        // String initialization
        string string1 = "hello";
        string string2 = "good bye"; 
        string string3 = "Happy Birthday";
        string string4 = "happy birthday";
        
        // Output all strings on a newline
        Console.WriteLine("string1 = " + "\"" + string1 + "\"\n" + 
                          "string2 = " + "\"" + string2 + "\"\n" +
                          "string3 = " + "\"" + string3 + "\"\n" +
                          "string4 = " + "\"" + string4 + "\"\n");
        
        // Test for equality using string.Equals method
        if (string1.Equals("hello"))
            Console.WriteLine("string1 equals \"hello\"");
        else 
            Console.WriteLine("string1 does not equal \"hello\"");
        
        // Test for equality using equality (==) operator
        if (string1 == "hello")
            Console.WriteLine("string1 equals \"hello\"");
        else 
            Console.WriteLine("string1 does not equal \"hello\"");
        
        // Test for equality comparing cases
        if (string.Equals(string3, string4)) // Static method
            Console.WriteLine("string3 equals string4");
        else 
            Console.WriteLine("string3 does not equal string4");
        
        // Output all string comparisons on a newline
        Console.WriteLine("\nstring1.CompareTo(string2) is " + 
                          + string1.CompareTo(string2) + "\n" + 
                          "string2.CompareTo(string1) is " + 
                          + string2.CompareTo(string1) + "\n" + 
                          "string1.CompareTo(string1) is " + 
                          + string1.CompareTo(string1) + "\n" + 
                          "string3.CompareTo(string4) is " + 
                          + string3.CompareTo(string4) + "\n" + 
                          "string4.CompareTo(string3) is " + 
                          + string4.CompareTo(string3) + "\n\n");
    } // end main
} // end class
