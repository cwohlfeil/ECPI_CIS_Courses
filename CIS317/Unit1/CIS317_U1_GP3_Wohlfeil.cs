// Cameron Wohlfeil
// CIS317
// 01/25/2019
// Unit 1 Guided Practice 3

using System;

class StringIndexMethods
{
   static void Main()
   {
      var letters = "abcdefghijklmabcdefghijklm";
      char[] search_letters = {'c', 'a', '$'};

      // Use IndexOf to locate a character in a string
      Console.WriteLine("First 'c' is located at index " +
                        letters.IndexOf('c'));
      Console.WriteLine("First 'a' starting at 1 is located at index " +
                        letters.IndexOf('a', 1));
      Console.WriteLine("First '$' in the 5 positions starting at 3 " +
                        "is located at index " + letters.IndexOf('$', 3, 5));

      // Use LastIndexOf to find the last occurrence of a character in a string
      Console.WriteLine("\nLast 'c' is located at index " +
                        letters.LastIndexOf('c'));
      Console.WriteLine("Last 'a' up to position 25 is located at " +
                        "index " + letters.LastIndexOf('a', 25));
      Console.WriteLine("Last '$' in the 5 positions ending at 15 " +
                        "is located at index " + letters.LastIndexOf('$', 15, 5));

      // Use IndexOf to locate a substring in a string
      Console.WriteLine("\nFirst \"def\" is located at index " +
                        letters.IndexOf("def"));
      Console.WriteLine("First \"def\" starting at 7 is located at " +
                        "index " + letters.IndexOf("def", 7));
      Console.WriteLine("First \"hello\" in the 15 positions " +
                         "starting at 5 is located at index " +
                         letters.IndexOf("hello", 5, 15));

      // Use LastIndexOf to find the last occurrence of a substring in a string
      Console.WriteLine("\nLast \"def\" is located at index " +
                        letters.LastIndexOf("def"));
      Console.WriteLine("Last \"def\" up to position 25 is located " +
                        "at index " + letters.LastIndexOf("def", 25));
      Console.WriteLine("Last \"hello\" in the 15 positions " +
                         "ending at 20 is located at index " +
                         letters.LastIndexOf("hello", 20, 15));

      // Use IndexOfAny to find first occurrence of character in array
      Console.WriteLine("\nFirst 'c', 'a' or '$' is " +
                        "located at index " + letters.IndexOfAny(search_letters));
      Console.WriteLine("First 'c', 'a' or '$' starting at 7 is " +
                        "located at index " + letters.IndexOfAny(search_letters, 7));
      Console.WriteLine("First 'c', 'a' or '$' in the 5 positions " +
                         "starting at 7 is located at index " +
                         letters.IndexOfAny(search_letters, 7, 5));

      // Use LastIndexOfAny to find last occurrence of character in array
      Console.WriteLine("\nLast 'c', 'a' or '$' is " +
                        "located at index " + letters.LastIndexOfAny(search_letters));
      Console.WriteLine("Last 'c', 'a' or '$' up to position 1 is " +
                         "located at index " +
                         letters.LastIndexOfAny(search_letters, 1));
      Console.WriteLine("Last 'c', 'a' or '$' in the 5 positions " +
                         "ending at 25 is located at index " +
                         letters.LastIndexOfAny(search_letters, 25, 5));
    } // end main
} // end class
