// Cameron Wohlfeil
// CIS317
// 01/25/2019
// Unit 1 Guided Practice 1

using System;

class StringConstructor
{
    static void Main(string[] args)
    {
        // String initialization
        char[] character_array = {'b','i','r','t','h',' ','d','a','y'};
        string original_string = "Welcome to C# Programming!";
        string string1 = original_string; // Copy original_string
        string string2 = new string(character_array); // Make a string from character_array
        string string3 = new string(character_array, 6, 3); // Make a string starting from index 6 in character_array ending 3 indexes later.
        string string4 = new string('C', 5); // Make a new string by repeating 'C' 5 times
        
        // Output all strings on a newline
        Console.WriteLine("string1 = " + "\"" + string1 + "\"\n" + 
                         "string2 = " + "\"" + string2 + "\"\n" +
                         "string3 = " + "\"" + string3 + "\"\n" +
                         "string4 = " + "\"" + string4 + "\"\n");
    } // end main
} // end class
