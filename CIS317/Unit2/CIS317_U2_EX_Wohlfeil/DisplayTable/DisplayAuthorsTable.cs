﻿// Fig. 22.22: DisplayAuthorsTable.cs
// Displaying data from a database table in a DataGridView.
using System;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Windows.Forms;

namespace DisplayTable
{
   public partial class DisplayAuthorsTable : Form
   {
      // constructor
      public DisplayAuthorsTable()
      {
         InitializeComponent();
      }

      // Entity Framework DbContext                  
      private BooksExamples.BooksEntities dbcontext =
         new BooksExamples.BooksEntities();

      private void RefreshAuthors()
      {
          // Dispose old DbContext, if any
          if (dbcontext != null)
          {
              dbcontext.Dispose();
          }

          // create new DbContext so we can reorder records based on edits
          dbcontext = new BooksExamples.BooksEntities();

          // use LINQ to order the Authors table contents 
          // by last name, then first name
          dbcontext.Authors
              .OrderBy(entry => entry.LastName)
              .ThenBy(entry => entry.FirstName)
              .Load();

            // specify DataSource for addressBindingSource
          authorBindingSource.DataSource = dbcontext.Authors.Local;
          authorBindingSource.MoveFirst(); // go to first result     
          findTextBox.Clear(); // clear the Find TextBox              
      }

        // load data from database into DataGridView
      private void DisplayAuthorsTable_Load(object sender, EventArgs e)
      {
          RefreshAuthors();
      }

      // click event handler for the Save Button in the 
      // BindingNavigator saves the changes made to the data
      private void authorBindingNavigatorSaveItem_Click(
         object sender, EventArgs e)
      {
         Validate(); // validate the input fields                       
         authorBindingSource.EndEdit(); // complete current edit, if any

         // try to save changes
         try
         {
            dbcontext.SaveChanges(); // write changes to database file
         }
         catch (DbEntityValidationException)
         {
            MessageBox.Show("FirstName and LastName must contain values",
               "Entity Validation Exception");
         }
      }

      // use LINQ to create a data source that contains only people
      // with last names that start with the specified text
      private void findButton_Click(object sender, EventArgs e)
      {
          // use LINQ to filter contacts with last names that
          // start with findTextBox contents
          var lastNameQuery = 
              from authors in dbcontext.Authors
              where authors.LastName.StartsWith(findTextBox.Text)
              orderby authors.LastName, authors.FirstName
              select authors;

            // display matching contacts
          authorBindingSource.DataSource = lastNameQuery.ToList();
          authorBindingSource.MoveFirst(); // go to first result  

          // don't allow add/delete when contacts are filtered
          bindingNavigatorAddNewItem.Enabled = false;
          bindingNavigatorDeleteItem.Enabled = false;
      }

      // reload addressBindingSource with all rows
      private void browseAllButton_Click(object sender, EventArgs e)
      {
          // allow add/delete when contacts are not filtered
          bindingNavigatorAddNewItem.Enabled = true;
          bindingNavigatorDeleteItem.Enabled = true;
          RefreshAuthors();
      }
    }
}
