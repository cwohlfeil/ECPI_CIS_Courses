﻿/* 
 * Name: Cameron Wohlfeil
 * Date: 01/28/2019
 * Description: CIS317 Calculator Project
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CIS317_Project
{
    public partial class CalculatorForm : Form
    {
        // Initialize form and components
        public CalculatorForm()
        {
            InitializeComponent();
        }

        // Instance variables for calculations
        private Double val = 0; // Double class to use methods
        private string operation = "";
        private bool oper_pressed = false;
        private int dotCount = 0;


        private void operator_Click(object sender, EventArgs e)
        {
            // Generic function to handle click on all operator buttons
            Button b = (Button)sender;
            operation = b.Text;
            val = Double.Parse(numberDisplay.Text);
            oper_pressed = true;
        }

        private void number_Click(object sender, EventArgs e)
        {
            // Generic function to handle click on all number and decimal buttons
            if ((numberDisplay.Text == "0") || (oper_pressed))
            {
                // Handle some error cases
                numberDisplay.Clear();
                dotCount = 0;
            }

            oper_pressed = false;
            Button b = (Button)sender;

            if (b.Text == "." && dotCount < 1)
            {
                // If a decimal point is added to the text box, handle it appropriately
                dotCount++;
                numberDisplay.Text = numberDisplay.Text + b.Text;
            }
            if (b.Text != ".")
            {
                numberDisplay.Text = numberDisplay.Text + b.Text;
            }
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            // Clear display and everything
            numberDisplay.Text = "0";
            dotCount = 0;
        }

        private void ceButton_Click(object sender, EventArgs e)
        {
            // Clear display and last value, but not previous value
            numberDisplay.Text = "0";
            val = 0;
            dotCount = 0;
        }

        private void equalButton_Click(object sender, EventArgs e)
        {
            // Perform correct operation and output solution
            switch (operation)
            {
                // Make sure to use Unicode characters in the case!
                case "+":
                    numberDisplay.Text = (val + Double.Parse(numberDisplay.Text)).ToString();
                    break;
                case "−":
                    numberDisplay.Text = (val - Double.Parse(numberDisplay.Text)).ToString();
                    break;
                case "×":
                    numberDisplay.Text = (val * Double.Parse(numberDisplay.Text)).ToString();
                    break;
                case "÷":
                    numberDisplay.Text = (val / Double.Parse(numberDisplay.Text)).ToString();
                    break;
                default:
                    break;
            }

            double ans = double.Parse(numberDisplay.Text);

            // check if should be output with or without decimal
            if (ans == (double)ans) 
                dotCount = 1;
            else
                dotCount = 0;
        }

        private void negativeButton_Click(object sender, EventArgs e)
        {

        }

        private void backButton_Click(object sender, EventArgs e)
        {
            // Clear display and last value, but not previous value
            numberDisplay.Text = "0";
            val = 0;
            dotCount = 0;
        }

        private void fractionButton_Click(object sender, EventArgs e)
        {

        }

        private void squaredButton_Click(object sender, EventArgs e)
        {

        }

        private void sqrtButton_Click(object sender, EventArgs e)
        {

        }

        private void percentButton_Click(object sender, EventArgs e)
        {

        }
    }
}
