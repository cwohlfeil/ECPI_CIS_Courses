﻿// Fig. 8.3: InitArray.cs
// Initializing the elements of an array with an array initializer.
using System;

class InitArray
{
   static void Main()
   {
      // initializer list specifies the value of each element 
      int[] array = { 32, 27, 64, 18, 95 };

      Console.WriteLine($"{"Index"}{"Value",8}"); // headings

      // output each array element's value 
      for (int counter = 0; counter < array.Length; ++counter)
      {
         Console.WriteLine($"{counter,5}{array[counter],8}");
      }
   }
}
