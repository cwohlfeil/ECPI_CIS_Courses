﻿/* 
 * Name: Cameron Wohlfeil
 * Date: 02/24/2019
 * Description: CIS317 Calculator Project
 */

using System;
using System.Windows.Forms;

namespace CIS317_Project
{
    internal static class CalculatorProgram
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new CalculatorForm());
        }
    }
}
