﻿/* 
 * Name: Cameron Wohlfeil
 * Date: 02/24/2019
 * Description: CIS317 Calculator Project
 */

using System;
using System.Globalization;
using System.Windows.Forms;

namespace CIS317_Project
{
    public partial class CalculatorForm : Form
    {
        // Constants for easy reuse
        private const string V = "0";
        private const string V1 = ".";
        private const string V2 = "Error, unable to parse number.";

        // Instance variables for calculations
        private double _val = 0; // Double class to use methods
        private string _operation = "";
        private bool _operatorPressed = false;
        private int _dotCount = 0;

        // Initialize form and components
        public CalculatorForm()
        {
            InitializeComponent();
        }

        private void Operator_Click(object sender, EventArgs e)
        {
            // Generic function to handle click on all operator buttons
            var b = (Button)sender;
            _operation = b.Text;
            try
            {
                _val = double.Parse(numberDisplay.Text);
                _operatorPressed = true;
            }
            catch
            {
                numberDisplay.Text = V2;
            }
        }

        private void Number_Click(object sender, EventArgs e)
        {
            // Generic function to handle click on all number and decimal buttons
            if ((numberDisplay.Text == V) || _operatorPressed)
            {
                // Handle some error cases
                numberDisplay.Clear();
                _dotCount = 0;
            }

            _operatorPressed = false;
            var b = (Button)sender;

            if (b.Text == V1 && _dotCount < 1)
            {
                // If a decimal point is added to the text box, handle it appropriately
                _dotCount++;
                numberDisplay.Text += b.Text;
            }
            if (b.Text != V1)
            {
                numberDisplay.Text += b.Text;
            }
        }

        private void ClearButton_Click(object sender, EventArgs e)
        {
            // Clear display and everything
            numberDisplay.Text = V;
            _dotCount = 0;
        }

        private void CeButton_Click(object sender, EventArgs e)
        {
            // Clear display and last value, but not previous value
            numberDisplay.Text = V;
            _val = 0;
            _dotCount = 0;
        }

        private void EqualButton_Click(object sender, EventArgs e)
        {
            // Perform correct operation and output solution
            switch (_operation)
            {
                // Make sure to use Unicode characters in the case
                case "+":
                    numberDisplay.Text = (_val + double.Parse(numberDisplay.Text)).ToString(CultureInfo.CurrentCulture);
                    break;
                case "−":
                    numberDisplay.Text = (_val - double.Parse(numberDisplay.Text)).ToString(CultureInfo.CurrentCulture);
                    break;
                case "×":
                    numberDisplay.Text = (_val * double.Parse(numberDisplay.Text)).ToString(CultureInfo.CurrentCulture);
                    break;
                case "÷":
                    numberDisplay.Text = (_val / double.Parse(numberDisplay.Text)).ToString(CultureInfo.CurrentCulture);
                    break;
                default:
                    NotImplemented();
                    break;
            }

            try
            {
                // Attempt to parse number in text display
                var ans = double.Parse(numberDisplay.Text);

                // check if should be output with or without decimal
                #pragma warning disable CS1718 // Comparison made to same variable
                _dotCount = ans == ans ? 1 : 0;
                #pragma warning restore CS1718 
            }
            catch
            {
                numberDisplay.Text = V2;
            }
        }

        private void NegativeButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Attempt to parse number and multiply it by -1 to flip sign
                var number = -1.0 * double.Parse(numberDisplay.Text);
                numberDisplay.Text = number.ToString(CultureInfo.CurrentCulture);
            }
            catch
            {
                numberDisplay.Text = V2;
            }
        }

        private void BackButton_Click(object sender, EventArgs e)
        {
            // Clear display and last value, but not previous value
            CeButton_Click(sender, e);
        }

        private void FractionButton_Click(object sender, EventArgs e)
        {
            NotImplemented();
        }

        private void SquaredButton_Click(object sender, EventArgs e)
        {
            NotImplemented();
        }

        private void SqrtButton_Click(object sender, EventArgs e)
        {
            NotImplemented();
        }

        private void PercentButton_Click(object sender, EventArgs e)
        {
            NotImplemented();
        }

        private void NotImplemented()
        {
            numberDisplay.Text = @"ERROR, clear to continue.";
        }
    }
}
