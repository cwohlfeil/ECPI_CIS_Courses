﻿/*
 * Name: Cameron Wohlfeil
 * Date: 02/24/2019
 * Description: CIS317 Final Part 2
 */
using System;
using System.Windows.Forms;
using System.IO;

namespace CIS317_FinalPart2
{
    public partial class CIS317_FinalPart2 : Form
    {
        public CIS317_FinalPart2()
        {
            InitializeComponent();

            // If results file exists, delete it first
            if (File.Exists("..\\results.txt"))
            {
                File.Delete("..\\results.txt");
            }
        }

        private void viewSurveyResultsButton_Click(object sender, EventArgs e)
        {
            doneButton_Click(sender, e);
        }

        private void doneButton_Click(object sender, EventArgs e)
        {
            // student response array (more typically, input at run time)
            var responses = new int[100];
            var frequency = new int[11]; // array of frequency counters

            try
            {
                var counter = 0;
                string line;

                // Read the file and display it line by line.  
                var file = new StreamReader("..\\results.txt");

                while ((line = file.ReadLine()) != null)
                {
                    responses[counter] = Convert.ToInt32(line);
                    counter++;
                }

                file.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error: {ex.Message}", "File Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            // for each answer, select responses element and use that value
            // as frequency index to determine element to increment        
            for (var answer = 0; answer < responses.Length; ++answer)
            {
                try
                {
                    ++frequency[responses[answer]];
                }
                catch (IndexOutOfRangeException ex)
                {
                    outputTextBox.AppendText(ex.Message);
                    outputTextBox.AppendText($"   responses[{answer}] = {responses[answer]}\n");
                    outputTextBox.AppendText(Environment.NewLine);
                }
            }

            outputTextBox.AppendText($"{"Rating",6}{"Frequency",10}");

            // output each array element's value
            for (var rating = 1; rating < frequency.Length; ++rating)
            {
                outputTextBox.AppendText(Environment.NewLine);
                outputTextBox.AppendText($"{rating,6}{frequency[rating],10}");
            }
        }

        // invoked when user presses key
        private void inputTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            // determine whether user pressed Enter key
            if (e.KeyCode == Keys.Enter)
            {
                string[] split = inputTextBox.Text.Split(new char[] { ' ', ',', '.', ':', '\t' });

                try
                {
                    using (var stream = new StreamWriter("..\\results.txt"))
                    {
                        foreach (var s in split)
                        {
                            if (s.Trim() != "")
                                stream.WriteLine(s);
                        }

                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Error: {ex.Message}", "File Error",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
