﻿namespace CIS317_FinalPart2
{
   partial class CIS317_FinalPart2
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose( bool disposing )
      {
         if ( disposing && ( components != null ) )
         {
            components.Dispose();
         }
         base.Dispose( disposing );
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
            this.inputLabel = new System.Windows.Forms.Label();
            this.outputTextBox = new System.Windows.Forms.TextBox();
            this.inputTextBox = new System.Windows.Forms.TextBox();
            this.doneButton = new System.Windows.Forms.Button();
            this.viewSurveyResultsButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // inputLabel
            // 
            this.inputLabel.AutoSize = true;
            this.inputLabel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inputLabel.Location = new System.Drawing.Point(9, 9);
            this.inputLabel.Name = "inputLabel";
            this.inputLabel.Size = new System.Drawing.Size(242, 15);
            this.inputLabel.TabIndex = 5;
            this.inputLabel.Text = "Enter an integer result (1-10) and press Enter:";
            // 
            // outputTextBox
            // 
            this.outputTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.outputTextBox.Location = new System.Drawing.Point(12, 143);
            this.outputTextBox.Multiline = true;
            this.outputTextBox.Name = "outputTextBox";
            this.outputTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.outputTextBox.Size = new System.Drawing.Size(270, 237);
            this.outputTextBox.TabIndex = 4;
            // 
            // inputTextBox
            // 
            this.inputTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.inputTextBox.Location = new System.Drawing.Point(12, 45);
            this.inputTextBox.Name = "inputTextBox";
            this.inputTextBox.Size = new System.Drawing.Size(145, 23);
            this.inputTextBox.TabIndex = 3;
            this.inputTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.inputTextBox_KeyDown);
            // 
            // doneButton
            // 
            this.doneButton.Location = new System.Drawing.Point(176, 45);
            this.doneButton.Name = "doneButton";
            this.doneButton.Size = new System.Drawing.Size(75, 23);
            this.doneButton.TabIndex = 6;
            this.doneButton.Text = "Done";
            this.doneButton.UseVisualStyleBackColor = true;
            this.doneButton.Click += new System.EventHandler(this.doneButton_Click);
            // 
            // viewSurveyResultsButton
            // 
            this.viewSurveyResultsButton.Location = new System.Drawing.Point(82, 96);
            this.viewSurveyResultsButton.Name = "viewSurveyResultsButton";
            this.viewSurveyResultsButton.Size = new System.Drawing.Size(122, 23);
            this.viewSurveyResultsButton.TabIndex = 7;
            this.viewSurveyResultsButton.Text = "View Survey Results";
            this.viewSurveyResultsButton.UseVisualStyleBackColor = true;
            this.viewSurveyResultsButton.Click += new System.EventHandler(this.viewSurveyResultsButton_Click);
            // 
            // CIS317_FinalPart2
            // 
            this.ClientSize = new System.Drawing.Size(296, 393);
            this.Controls.Add(this.viewSurveyResultsButton);
            this.Controls.Add(this.doneButton);
            this.Controls.Add(this.inputLabel);
            this.Controls.Add(this.outputTextBox);
            this.Controls.Add(this.inputTextBox);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "CIS317_FinalPart2";
            this.Text = "Student Poll";
            this.ResumeLayout(false);
            this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.Label inputLabel;
      private System.Windows.Forms.TextBox outputTextBox;
      private System.Windows.Forms.TextBox inputTextBox;
        private System.Windows.Forms.Button doneButton;
        private System.Windows.Forms.Button viewSurveyResultsButton;
    }
}