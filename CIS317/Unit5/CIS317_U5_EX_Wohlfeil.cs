/*
 * Name: Cameron Wohlfeil
 * Date: 02/21/2019
 *
 * 1) Assign five values to an integer array called bestScores, and display them in column format, showing the index and the value. 
 */

using System;

class DisplayArray
{
    static void Main()
    {
        int[] array = {80, 85, 90, 95, 60};

        Console.WriteLine("Index Value");
        
        // Loop through array
        for (int counter = 0; counter < array.Length; ++counter)
        {
            // Use string format to space output properly
            Console.WriteLine(String.Format("{0,5}{1,5}", counter, array[counter]));
        }
    }
} 