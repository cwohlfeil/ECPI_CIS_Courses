/*
 * Name: Cameron Wohlfeil
 * Date: 3/28/2019
 * Description: CIS319 U4 EX1 - Modified Person Class
 */

public class Person {
    private int addressID;
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;

    // constructor
    public Person() { }

    // constructor
    Person(int addressID, String firstName, String lastName, String email, String phoneNumber) {
        setAddressID(addressID);
        setFirstName(firstName);
        setLastName(lastName);
        setEmail(email);
        setPhoneNumber(phoneNumber);
    }

    // returns the addressID
    int getAddressID() {
        return addressID;
    }

    // sets the addressID
    private void setAddressID(int addressID) {
        this.addressID = addressID;
    }

    // returns the first name
    String getFirstName() {
        return firstName;
    }

    // sets the firstName
    private void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    // returns the last name
    String getLastName() {
        return lastName;
    }

    // sets the lastName
    private void setLastName(String lastName) {
        this.lastName = lastName;
    }

    // returns the email address
    String getEmail() {
        return email;
    }

    // sets the email address
    private void setEmail(String email) {
        this.email = email;
    }

    // returns the phone number
    String getPhoneNumber() {
        return phoneNumber;
    }

    // sets the phone number
    private void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
} // end class Person

 