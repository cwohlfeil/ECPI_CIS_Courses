/*
 * Name: Cameron Wohlfeil
 * Date: 3/28/2019
 * Description: CIS319 U4 EX2 - Display Authors
 *
 * Using the files in Graded Exercise 1 as a guide, create a set of screens that allow me to list all the authors in the database, as well as add a new author.
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class AuthorDisplay extends JFrame {
    private AuthorQueries authorQueries;
    private List<Author> results;
    private int numberOfEntries = 0, currentEntryIndex;

    private JButton nextButton, previousButton;
    private JTextField firstNameTextField, idTextField, indexTextField, lastNameTextField,
            maxTextField, queryTextField;

    // constructor
    private AuthorDisplay() {
        super("Book Authors");

        // establish database connection and set up PreparedStatements
        authorQueries = new AuthorQueries();

        // create GUI
        JPanel navigatePanel = new JPanel();
        previousButton = new JButton();
        indexTextField = new JTextField(2);
        JLabel ofLabel = new JLabel();
        maxTextField = new JTextField(2);
        nextButton = new JButton();
        JPanel displayPanel = new JPanel();
        JLabel idLabel = new JLabel();
        idTextField = new JTextField(10);
        JLabel firstNameLabel = new JLabel();
        firstNameTextField = new JTextField(10);
        JLabel lastNameLabel = new JLabel();
        lastNameTextField = new JTextField(10);
        JPanel queryPanel = new JPanel();
        JLabel queryLabel = new JLabel();
        queryTextField = new JTextField(10);
        JButton queryButton = new JButton();
        JButton browseButton = new JButton();
        JButton insertButton = new JButton();

        setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));
        setSize(400, 355);
        setResizable(false);

        navigatePanel.setLayout(new BoxLayout(navigatePanel, BoxLayout.X_AXIS));

        previousButton.setText("Previous");
        previousButton.setEnabled(false);
        previousButton.addActionListener(
                this::previousButtonActionPerformed
        ); // end call to addActionListener

        navigatePanel.add(previousButton);
        navigatePanel.add(Box.createHorizontalStrut(10));

        indexTextField.setHorizontalAlignment(JTextField.CENTER);
        indexTextField.addActionListener(
                this::indexTextFieldActionPerformed
        ); // end call to addActionListener

        navigatePanel.add(indexTextField);
        navigatePanel.add(Box.createHorizontalStrut(10));

        ofLabel.setText("of");
        navigatePanel.add(ofLabel);
        navigatePanel.add(Box.createHorizontalStrut(10));

        maxTextField.setHorizontalAlignment(JTextField.CENTER);
        maxTextField.setEditable(false);
        navigatePanel.add(maxTextField);
        navigatePanel.add(Box.createHorizontalStrut(10));

        nextButton.setText("Next");
        nextButton.setEnabled(false);
        nextButton.addActionListener(
                this::nextButtonActionPerformed
        ); // end call to addActionListener
        
        navigatePanel.add(nextButton);
        add(navigatePanel);

        displayPanel.setLayout(new GridLayout(3, 2, 4, 4));

        idLabel.setText("Address ID:");
        displayPanel.add(idLabel);

        idTextField.setEditable(false);
        displayPanel.add(idTextField);

        firstNameLabel.setText("First Name:");
        displayPanel.add(firstNameLabel);
        displayPanel.add(firstNameTextField);

        lastNameLabel.setText("Last Name:");
        displayPanel.add(lastNameLabel);
        displayPanel.add(lastNameTextField);

        add(displayPanel);

        queryPanel.setLayout(new BoxLayout(queryPanel, BoxLayout.X_AXIS));

        queryPanel.setBorder(BorderFactory.createTitledBorder("Find an entry by last name"));
        queryLabel.setText("Last Name:");
        queryPanel.add(Box.createHorizontalStrut(5));
        queryPanel.add(queryLabel);
        queryPanel.add(Box.createHorizontalStrut(10));
        queryPanel.add(queryTextField);
        queryPanel.add(Box.createHorizontalStrut(10));

        queryButton.setText("Find");
        queryButton.addActionListener(
                this::queryButtonActionPerformed
        ); // end call to addActionListener

        queryPanel.add(queryButton);
        queryPanel.add(Box.createHorizontalStrut(5));
        add(queryPanel);

        browseButton.setText("Browse All Entries");
        browseButton.addActionListener(
                this::browseButtonActionPerformed
        ); // end call to addActionListener
        add(browseButton);

        insertButton.setText("Insert New Entry");
        insertButton.addActionListener(
                this::insertButtonActionPerformed
        ); // end call to addActionListener
        add(insertButton);

        addWindowListener(
            new WindowAdapter() {
                public void windowClosing(WindowEvent evt) {
                    authorQueries.close(); // close database connection
                    System.exit(0);
                }
            }
        ); // end call to addWindowListener

        setVisible(true);
    } // end constructor

    // main method
    public static void main(String[] args) { new AuthorDisplay(); }

    // handles call when previousButton is clicked
    private void previousButtonActionPerformed(ActionEvent evt) {
        currentEntryIndex--;

        if (currentEntryIndex < 0)
            currentEntryIndex = numberOfEntries - 1;

        indexTextField.setText("" + (currentEntryIndex + 1));
        indexTextFieldActionPerformed(evt);
    }

    // handles call when nextButton is clicked
    private void nextButtonActionPerformed(ActionEvent evt) {
        currentEntryIndex++;

        if (currentEntryIndex >= numberOfEntries)
            currentEntryIndex = 0;

        indexTextField.setText("" + (currentEntryIndex + 1));
        indexTextFieldActionPerformed(evt);
    }

    private void populateTextFields(int currentEntryIndex) {
        Author currentEntry = results.get(currentEntryIndex);
        idTextField.setText("" + currentEntry.getAuthorID());
        firstNameTextField.setText(currentEntry.getFirstName());
        lastNameTextField.setText(currentEntry.getLastName());
        maxTextField.setText("" + numberOfEntries);
        indexTextField.setText("" + (currentEntryIndex + 1));
    }

    private void enableAllButtons() {
        nextButton.setEnabled(true);
        previousButton.setEnabled(true);
    }

    // handles call when queryButton is clicked
    private void queryButtonActionPerformed(ActionEvent evt) {
        results = authorQueries.getAuthorsByLastName(queryTextField.getText());
        numberOfEntries = results.size();

        if (numberOfEntries != 0) {
            populateTextFields(0);
            enableAllButtons();
        } else {
            browseButtonActionPerformed(evt);
        }
    }

    // handles call when a new value is entered in indexTextField
    private void indexTextFieldActionPerformed(ActionEvent evt) {
        currentEntryIndex = (Integer.parseInt(indexTextField.getText()) - 1);

        if (numberOfEntries != 0 && currentEntryIndex < numberOfEntries) {
            populateTextFields(currentEntryIndex);
            enableAllButtons();
        }
    }

    // handles call when browseButton is clicked
    private void browseButtonActionPerformed(ActionEvent evt) {
        try {
            results = authorQueries.getAllAuthors();
            numberOfEntries = results.size();

            if (numberOfEntries != 0) {
                populateTextFields(0);
                enableAllButtons();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // handles call when insertButton is clicked
    private void insertButtonActionPerformed(ActionEvent evt) {
        int result = authorQueries.addAuthor(firstNameTextField.getText(),
                lastNameTextField.getText());

        if (result == 1) {
            JOptionPane.showMessageDialog(this, "Author added!",
                    "Author added", JOptionPane.PLAIN_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(this, "Author not added!",
                    "Error", JOptionPane.PLAIN_MESSAGE);
        }

        browseButtonActionPerformed(evt);
    }
} // end class AuthorDisplay
