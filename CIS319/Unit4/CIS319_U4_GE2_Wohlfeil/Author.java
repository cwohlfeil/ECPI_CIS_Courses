/*
 * Name: Cameron Wohlfeil
 * Date: 3/28/2019
 * Description: CIS319 U4 EX2 - Author Class
 */

class Author {
    private int authorID;
    private String firstName;
    private String lastName;

    // constructor
    Author(int authorID, String firstName, String lastName) {
        setAuthorID(authorID);
        setFirstName(firstName);
        setLastName(lastName);
    }

    // returns the authorID
    int getAuthorID() { return authorID; }

    // sets the authorID
    private void setAuthorID(int authorID) { this.authorID = authorID; }

    // returns the first name
    String getFirstName() { return firstName; }

    // sets the firstName
    private void setFirstName(String firstName) { this.firstName = firstName; }

    // returns the last name
    String getLastName() { return lastName; }

    // sets the lastName
    private void setLastName(String lastName) { this.lastName = lastName; }
} // end class Author