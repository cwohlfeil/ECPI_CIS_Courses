/*
 * Name: Cameron Wohlfeil
 * Date: 3/28/2019
 * Description: CIS319 U4 EX2 - Author Queries
 */

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

class AuthorQueries {
    private static final String URL = "jdbc:derby://localhost:1527/books";
    private static final String USERNAME = "deitel";
    private static final String PASSWORD = "deitel";

    private Connection connection; // manages connection
    private PreparedStatement selectAllAuthors;
    private PreparedStatement selectAuthorsByLastName;
    private PreparedStatement insertNewAuthor;

    // constructor
    AuthorQueries() {
        try {
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);

            // create query that selects all entries in the AddressBook
            selectAllAuthors = connection.prepareStatement("SELECT * FROM authors");

            // create query that selects entries with a specific last name
            selectAuthorsByLastName = connection.prepareStatement("SELECT * FROM authors " + 
                    "WHERE lastName = ?");

            // create insert that adds a new entry into the database
            insertNewAuthor = connection.prepareStatement("INSERT INTO authors " +
                    "(firstName, lastName) " +
                    "VALUES (?, ?)");
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
            System.exit(1);
        }
    } // end AuthorQueries constructor

    // select all of the authors in the database
    List<Author> getAllAuthors() {
        List<Author> results = null;
        ResultSet resultSet = null;

        try {
            // executeQuery returns ResultSet containing matching entries
            resultSet = selectAllAuthors.executeQuery();
            results = new ArrayList<>();

            while (resultSet.next()) {
                results.add(new Author(
                        resultSet.getInt("authorID"),
                        resultSet.getString("firstName"),
                        resultSet.getString("lastName")));
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        } finally {
            try {
                Objects.requireNonNull(resultSet).close();
            } catch (SQLException sqlException) {
                sqlException.printStackTrace();
                close();
            }
        }

        return results;
    }

    // select person by last name
    List<Author> getAuthorsByLastName(String name) {
        List<Author> results = null;
        ResultSet resultSet = null;

        try {
            selectAuthorsByLastName.setString(1, name); // specify last name

            // executeQuery returns ResultSet containing matching entries
            resultSet = selectAuthorsByLastName.executeQuery();

            results = new ArrayList<>();

            while (resultSet.next()) {
                results.add(new Author(resultSet.getInt("authorID"),
                        resultSet.getString("firstName"),
                        resultSet.getString("lastName")));
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        } finally {
            try {
                Objects.requireNonNull(resultSet).close();
            } catch (SQLException sqlException) {
                sqlException.printStackTrace();
                close();
            }
        }

        return results;
    }

    // add an entry
    int addAuthor(String fname, String lname) {
        int result = 0;

        // set parameters, then execute insertNewAuthor
        try {
            insertNewAuthor.setString(1, fname);
            insertNewAuthor.setString(2, lname);

            // insert the new entry; returns # of rows updated
            result = insertNewAuthor.executeUpdate();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
            close();
        }

        return result;
    }

    // close the database connection
    void close() {
        try {
            connection.close();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }
} // end class AuthorQueries