/*
 * Name: Cameron Wohlfeil
 * Date: 3/29/2019
 * Description: CIS319 Final Part 3 & 4 - Wedding Queries
 */

import java.sql.*;

class WeddingQueries {
    private static final String URL = "jdbc:derby://localhost:1527/weddings";
    private static final String USERNAME = "deitel";
    private static final String PASSWORD = "deitel";

    private Connection connection; // manages connection
    private PreparedStatement insertNewWedding;

    // constructor
    WeddingQueries() {
        try {
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);

            // create insert that adds a new entry into the database
            insertNewWedding = connection.prepareStatement("INSERT INTO CATERING " +
                    "(full_name, email, wedding_date, guest_count, total_price) " +
                    "VALUES (?, ?, ?, ?, ?)");
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
            System.exit(1);
        }
    } // end WeddingQueries constructor

    // add an entry
    int addWedding(String name, String email, Date weddingDate, int guestCount, double totalPrice) {
        int result = 0;

        // set parameters, then execute insertNewWedding
        try {
            insertNewWedding.setString(1, name);
            insertNewWedding.setString(2, email);
            insertNewWedding.setDate(3, weddingDate);
            insertNewWedding.setInt(4, guestCount);
            insertNewWedding.setDouble(5, totalPrice);

            // insert the new entry; returns # of rows updated
            result = insertNewWedding.executeUpdate();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
            close();
        }

        return result;
    }

    // close the database connection
    void close() {
        try {
            connection.close();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }
} // end class WeddingQueries