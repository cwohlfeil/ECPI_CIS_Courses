/*
 * Name: Cameron Wohlfeil
 * Date: 3/29/2019
 * Description: CIS319 Final Part 3 & 4 - Wedding Class
 */

import java.time.LocalDate;

public class Wedding {
    private int guestCount;
    private String name, email;
    private LocalDate weddingDate;
    private double totalPrice;

    // constructor
    Wedding(String name, String email, LocalDate weddingDate, int guestCount, double totalPrice) {
        setName(name);
        setEmail(email);
        setWeddingDate(weddingDate);
        setGuestCount(guestCount);
        setTotalPrice(totalPrice);
    }

    // returns the name
    String getName() {
        return name;
    }

    // sets the name
    private void setName(String name) {
        if (name != null)
            this.name = name;
        else
            throw new IllegalArgumentException("Name must be set.");
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        if (email != null)
            this.email = email;
        else
            throw new IllegalArgumentException("Email must be set.");
    }

    public LocalDate getWeddingDate() {
        return weddingDate;
    }

    public void setWeddingDate(LocalDate weddingDate) {
        if (weddingDate != null)
            this.weddingDate = weddingDate;
        else
            throw new IllegalArgumentException("Wedding date must be set.");
    }

    public int getGuestCount() {
        return guestCount;
    }

    public void setGuestCount(int guestCount) {
        if (guestCount != 0)
            this.guestCount = guestCount;
        else
            throw new IllegalArgumentException("Guest count must be set.");
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        if (totalPrice > 0)
            this.totalPrice = totalPrice;
        else
            throw new IllegalArgumentException("Total price must be set.");
    }
} // end class Wedding