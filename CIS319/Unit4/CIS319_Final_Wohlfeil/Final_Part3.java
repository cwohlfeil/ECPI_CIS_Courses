/*
 * Name: Cameron Wohlfeil
 * Date: 3/29/2019
 * Description: CIS319 Final Part 3 & 4 - Wedding Form
 */

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Final_Part3 extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("Final_Part3.fxml"));
        primaryStage.setTitle("Wedding Form");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
