/*
 * Name: Cameron Wohlfeil
 * Date: 3/29/2019
 * Description: CIS319 Final Part 3 & 4 - Wedding Controller
 */

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javax.swing.*;
import java.time.LocalDate;
import java.sql.Date;


public class Final_Part3_Controller {
    @FXML
    private TextField nameTextField, emailTextField, totalTextField;

    @FXML
    private DatePicker weddingDatePicker;

    @FXML
    private Slider guestCountSlider;

    @FXML
    private ToggleGroup serviceStyleToggleGroup;

    @FXML
    private CheckBox cocktailHourCheckbox, coffeeCheckbox, champagneCheckbox, barCheckbox;

    @FXML
    private Button saveButton;

    @FXML
    private void submit(ActionEvent evt) {
        double total = 0;
        RadioButton serviceStyle = (RadioButton) serviceStyleToggleGroup.getSelectedToggle();
        String name = nameTextField.getText(), email = emailTextField.getText(), serviceStyleText = serviceStyle.getText();
        LocalDate weddingDate = weddingDatePicker.getValue();
        int guestCount = (int) guestCountSlider.getValue();

        try {
            if (name.equals("") || email.equals("") || weddingDate == null || guestCount == 0 || serviceStyleText.equals("")) {
                final JPanel panel = new JPanel();
                JOptionPane.showMessageDialog(panel,
                        "Name, email, wedding date, guess count, and service style must be set. Please try again",
                        "Error", JOptionPane.ERROR_MESSAGE);
            } else {
                // Calculate subtotal
                switch (serviceStyleText) {
                    case "Buffet - $25":
                        total = 25 * guestCount;
                        break;
                    case "Seated Plate - $40":
                        total = 40 * guestCount;
                        break;
                    case "Cocktail Only - $15":
                        total = 15 * guestCount;
                        break;
                    default:
                        final JPanel panel = new JPanel();
                        JOptionPane.showMessageDialog(panel,
                                "Service style must be set. Please try again",
                                "Error", JOptionPane.ERROR_MESSAGE);
                        break;
                }

                if (cocktailHourCheckbox.isSelected())
                    total += 2 * guestCount;

                if (coffeeCheckbox.isSelected())
                    total += 2 * guestCount;

                if (champagneCheckbox.isSelected())
                    total += guestCount;

                if (barCheckbox.isSelected())
                    total += 10 * guestCount;

                totalTextField.setText(String.format("$%,.2f", total));
                saveButton.setDisable(false);
            }
        } catch (IllegalArgumentException e) {
            final JPanel panel = new JPanel();
            JOptionPane.showMessageDialog(panel,
                    "Values not within correct parameters. Please try again",
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    @FXML
    private void cancel(ActionEvent evt) {
        nameTextField.setText("");
        emailTextField.setText("");
        weddingDatePicker.getEditor().clear();
        guestCountSlider.setValue(0);
        serviceStyleToggleGroup.getSelectedToggle().setSelected(false);
        cocktailHourCheckbox.setSelected(false);
        coffeeCheckbox.setSelected(false);
        champagneCheckbox.setSelected(false);
        barCheckbox.setSelected(false);
        totalTextField.setText("");
        saveButton.setDisable(true);
    }

    @FXML
    private void save(ActionEvent evt) {
        WeddingQueries weddingQueries = new WeddingQueries();
        String name = nameTextField.getText(), email = emailTextField.getText();
        // Use regex to drop everything besides digits and decimal point
        double total = Double.parseDouble(totalTextField.getText().replaceAll("[^\\d.]+", ""));
        Date weddingDate = Date.valueOf(weddingDatePicker.getValue());
        int guestCount = (int) guestCountSlider.getValue();
        final JPanel panel = new JPanel();

        int result = weddingQueries.addWedding(name, email, weddingDate, guestCount, total);

        if (result == 1) {
            JOptionPane.showMessageDialog(panel, "Event added to database.",
                    "Event added", JOptionPane.PLAIN_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(panel, "Event not added to database.",
                    "Error", JOptionPane.PLAIN_MESSAGE);
        }

        // Clear form
        cancel(evt);
    }
}
