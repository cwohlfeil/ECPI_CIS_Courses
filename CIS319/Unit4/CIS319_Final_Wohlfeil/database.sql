CREATE TABLE CATERING (
	full_name VARCHAR(50) NOT NULL,
	email VARCHAR(30) NOT NULL,
	wedding_date DATE NOT NULL,
	guest_count INT NOT NULL,
	total_price DECIMAL NOT NULL
);