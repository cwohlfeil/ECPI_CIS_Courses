/*
 * Name: Cameron Wohlfeil
 * Date: 3/20/2019
 * Description: Guess the number test class
 */

import javax.swing.JFrame;

public class GuessTheNumberTest {
	public static void main(String[] args) {
		GuessTheNumberFrame guessTheNumberFrame = new GuessTheNumberFrame();
		guessTheNumberFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		guessTheNumberFrame.setSize(350, 100);
		guessTheNumberFrame.setVisible(true);
	}
}
