/*
 * Name: Cameron Wohlfeil
 * Date: 3/20/2019
 * Description: Guess the number frame class
 */

import java.util.concurrent.ThreadLocalRandom;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

class GuessTheNumberFrame extends JFrame {
	// Instance variables
	private JTextField inputField;
	private JLabel outputLabel;
	private JButton playButton;
	private int randomNum = ThreadLocalRandom.current().nextInt(1, 1001);

	GuessTheNumberFrame() {
		// Constructor
		super("Guess the Number");
		setLayout(new FlowLayout());

		JLabel promptLabel = new JLabel(
				"<html><div style=\"width:%dpx;\">Guess a number between 1 and 1000: </div><html>");
		add(promptLabel);

		inputField = new JTextField(5);
		add(inputField);

		outputLabel = new JLabel("");
		add(outputLabel);

		playButton = new JButton("Play Again?");
		playButton.setVisible(false);
		add(playButton);

		// Play button handler
		ButtonHandler handler = new ButtonHandler();

		// Listeners for button and input
		inputField.addActionListener(handler);
		playButton.addActionListener(handler);
	} // end constructor

	private class ButtonHandler implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent evt) {
			if (evt.getSource() == inputField) {
				String guessString;
				int guessValue;
				guessString = inputField.getText();

				try {
					guessValue = Integer.parseInt(guessString);

					// Error check guess
					if (guessValue < 1 || guessValue > 1000) {
						throw new NumberFormatException();
					}

					// Compare guess to random number
					if (guessValue < randomNum) {
						outputLabel.setText("You're Too Low!");
						inputField.setText("");
					} else if (guessValue > randomNum) {
						outputLabel.setText("You're Too High!");
						inputField.setText("");
					} else {
						outputLabel.setText("You Got It!");
						playButton.setVisible(true);
					}
				} catch (NumberFormatException e) {
					JOptionPane.showMessageDialog(GuessTheNumberFrame.this,
							"Please enter valid integer values.", "Error", JOptionPane.ERROR_MESSAGE);
					inputField.setText("");
				}
			} else {
				randomNum = ThreadLocalRandom.current().nextInt(1, 1001);
				outputLabel.setText("");
				inputField.setText("");
				playButton.setVisible(false);
			} // end if
		} // end actionPerformed
	} // end button handler
} // end class
