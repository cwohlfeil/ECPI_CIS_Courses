package Quiz1;

/* Fig. 10.4: Employee.java
 * Employee abstract superclass.
 * 1) Constructor: Assume that both the hireDate and birthDate values will be passed in through the Employee constructor.
 * 2) Create set/get methods in Employee for both the hireDate and the birthDate.  The getHireDate and getBirthDate should return the appropriate date in mm/dd/yyyy format.
 * 3) Also, create methods getBirthMonth() and getHireMonth() which will return the appropriate value.[A number from 1-12]
 * 4) Update the toString method to include the hiredate and the birthdate.
 */

public abstract class Employee {
	private final String firstName;
	private final String lastName;
	private final String socialSecurityNumber;
	private Date birthDate;
	private Date hireDate;

	// constructor
	public Employee(String firstName, String lastName, String socialSecurityNumber, Date birthDate, Date hireDate) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.socialSecurityNumber = socialSecurityNumber;
		this.birthDate = birthDate;
		this.hireDate = hireDate;
	}

	// return first name
	public String getFirstName() {
		return firstName;
	}

	// return last name
	public String getLastName() {
		return lastName;
	}

	// return social security number
	public String getSocialSecurityNumber() {
		return socialSecurityNumber;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Date getHireDate() {
		return hireDate;
	}

	public void setHireDate(Date hireDate) {
		this.hireDate = hireDate;
	}

	public int getBirthMonth() {
		return birthDate.getMonth();
	}

	public int getHireMonth() {
		return hireDate.getMonth();
	}

	// return String representation of Employee object
	@Override
	public String toString() {
		return String.format("%s %s%nsocial security number: %s%nbirth date: %s%nhire date: %s", getFirstName(), getLastName(),
				getSocialSecurityNumber(), getBirthDate(), getHireDate());
	}

	// abstract method must be overridden by concrete subclasses
	public abstract double earnings(); // no implementation here
} // end abstract class Employee

/**************************************************************************
 * (C) Copyright 1992-2014 by Deitel & Associates, Inc. and * Pearson Education,
 * Inc. All Rights Reserved. * * DISCLAIMER: The authors and publisher of this
 * book have used their * best efforts in preparing the book. These efforts
 * include the * development, research, and testing of the theories and programs
 * * to determine their effectiveness. The authors and publisher make * no
 * warranty of any kind, expressed or implied, with regard to these * programs
 * or to the documentation contained in these books. The authors * and publisher
 * shall not be liable in any event for incidental or * consequential damages in
 * connection with, or arising out of, the * furnishing, performance, or use of
 * these programs. *
 *************************************************************************/
