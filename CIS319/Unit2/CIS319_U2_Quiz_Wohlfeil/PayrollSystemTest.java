package Quiz1;

/* Name: Cameron Wohlfeil
 * Date: 3/8/2019
 * Fig. 10.9: PayrollSystemTest.java
 * Employee hierarchy test program.
 * 6) Payroll is processed once per month. In PayrollSystemTest.java, create birthDate and hireDate objects for each of the employees (ask the user for the information).  You don't need to do validation, as the class will throw exceptions for invalid input.
 * 7) In a loop calculate and print their earnings for each type of employee based on the following:
 *    a) $100 bonus if current month is birth month
 *    b) $50 bonus if current is anniversary month.
 * 8) Let the user know when you print their information if they got a bonus or not and how much it was.
 * 9) Keep track of how much was given in bonuses and print that out after the loop ends
 */

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PayrollSystemTest {
	public static void main(String[] args) {
		// Declarations
		String firstName, lastName, socialSecurityNumber;
		Date birthDate, hireDate;
		double grossSales, commissionRate, hourlyRate, hoursWorked, weeklySalary, wagePerPiece, totalBonus = 0;
		int empType, piecesProduced, birthDay, birthMonth, birthYear, hireDay, hireMonth, hireYear;

		LocalDate currentDate = LocalDate.now();
		int currentMonth = currentDate.getMonthValue();

		List<Employee> employees = new ArrayList<>();

		// create scanner
		Scanner input = new Scanner(System.in);

		// prompt for employee type
		System.out.println(
				"Enter employee type:\n\t1 for Salary\n\t2 for Hourly\n\t3 for Piece Worker\n\t4 for Commission\n\t-1 to quit");
		empType = input.nextInt();

		while (empType != -1) {
			input.nextLine();
			System.out.print("Enter first name: ");
			firstName = input.nextLine();

			System.out.print("Enter last name: ");
			lastName = input.nextLine();

			System.out.print("Enter Social Security Number: ");
			socialSecurityNumber = input.nextLine();

			System.out.print("Enter Birth Month (mm): ");
			birthMonth = input.nextInt();

			System.out.print("Enter Birth Day (dd): ");
			birthDay = input.nextInt();

			System.out.print("Enter Birth Year (yyyy): ");
			birthYear = input.nextInt();

			System.out.print("Enter Hire Month (mm): ");
			hireMonth = input.nextInt();

			System.out.print("Enter Hire Day (dd): ");
			hireDay = input.nextInt();

			System.out.print("Enter Hire Year (yyyy): ");
			hireYear = input.nextInt();

			birthDate = new Date(birthMonth, birthDay, birthYear);
			hireDate = new Date(hireMonth, hireDay, hireYear);

			try {
				switch (empType) {
				case 1: // salaried
					System.out.print("Enter Weekly Salary: ");
					weeklySalary = input.nextDouble();
					SalariedEmployee se = new SalariedEmployee(firstName, lastName, socialSecurityNumber, birthDate,
							hireDate, weeklySalary);
					employees.add(se);
					break;
				case 2: // hourly
					System.out.print("Enter Hourly Wage: ");
					hourlyRate = input.nextDouble();
					System.out.print("Enter Hours Worked: ");
					hoursWorked = input.nextDouble();
					HourlyEmployee he = new HourlyEmployee(firstName, lastName, socialSecurityNumber, birthDate,
							hireDate, hourlyRate, hoursWorked);
					employees.add(he);
					break;
				case 3: // pieceworker
					System.out.print("Enter Wage Per Piece: ");
					wagePerPiece = input.nextDouble();
					System.out.print("Enter Pieces Produced: ");
					piecesProduced = input.nextInt();
					PieceWorker pe = new PieceWorker(firstName, lastName, socialSecurityNumber, birthDate,
							hireDate, wagePerPiece, piecesProduced);
					employees.add(pe);
					break;
				case 4: // commission
					System.out.println("Enter Gross Sales: ");
					grossSales = input.nextDouble();
					System.out.println("Enter Commission Rate: ");
					commissionRate = input.nextDouble();
					CommissionEmployee ce = new CommissionEmployee(firstName, lastName, socialSecurityNumber, birthDate,
							hireDate, grossSales, commissionRate);
					employees.add(ce);
					break;
				}
			} catch (IllegalArgumentException e) {
				System.out.println(e.getMessage());
			}
			// prompt for employee type
			System.out.println(
					"Enter employee type:\n\t1 for Salary\n\t2 for Hourly\n\t3 for Piece Worker\n\t4 for Commission\n\t-1 to quit");
			empType = input.nextInt();

		}

		for (Employee currentEmployee : employees) {
			double bonus = 0;

			if (currentEmployee.getBirthMonth() == currentMonth) {
				bonus += 100;
			}

			if (currentEmployee.getHireMonth() == currentMonth) {
				bonus += 50;
			}

			System.out.println(currentEmployee);

			if (bonus > 0) {
				totalBonus += bonus;
				System.out.println("Employee was awarded a bonus.");
				System.out.println(String.format("Employee Earnings: $%,.2f", currentEmployee.earnings() + bonus));
			} else {
				System.out.println(String.format("Employee Earnings: $%,.2f", currentEmployee.earnings()));
			}

			System.out.println();
		}

		System.out.println(String.format("Total Bonuses Awarded: $%,.2f", totalBonus));
		System.out.println();

		// close scanner
		input.close();
	} // end main
} // end class PayrollSystemTest

/**************************************************************************
 * (C) Copyright 1992-2014 by Deitel & Associates, Inc. and * Pearson Education,
 * Inc. All Rights Reserved. * * DISCLAIMER: The authors and publisher of this
 * book have used their * best efforts in preparing the book. These efforts
 * include the * development, research, and testing of the theories and programs
 * * to determine their effectiveness. The authors and publisher make * no
 * warranty of any kind, expressed or implied, with regard to these * programs
 * or to the documentation contained in these books. The authors * and publisher
 * shall not be liable in any event for incidental or * consequential damages in
 * connection with, or arising out of, the * furnishing, performance, or use of
 * these programs. *
 *************************************************************************/
