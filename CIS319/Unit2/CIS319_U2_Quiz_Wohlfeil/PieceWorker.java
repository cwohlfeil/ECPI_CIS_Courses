package Quiz1;
/*
 * Name: 
 * Date: 
 * Description: 
 */

public class PieceWorker extends Employee {
	private double wagePerPiece;
	private int piecesProduced;

	public PieceWorker(String firstName, String lastName, String socialSecurityNumber, Date birthDate,
					   Date hireDate, double wagePerPiece, int piecesProduced) {
		super(firstName, lastName, socialSecurityNumber, birthDate, hireDate);
		// TODO Auto-generated constructor stub

		setWagePerPiece(wagePerPiece);
		setPiecesProduced(piecesProduced);
	}

	public void setWagePerPiece(double wagePerPiece) {
		if (wagePerPiece <= 0) {
			throw new IllegalArgumentException("Wage must be greater than 0");
		}

		this.wagePerPiece = wagePerPiece;
	}

	public void setPiecesProduced(int piecesProduced) {
		if (piecesProduced <= 0) {
			throw new IllegalArgumentException("Pieces Produced must be greater than 0");
		}

		this.piecesProduced = piecesProduced;
	}

	public double getWagePerPiece() {
		return wagePerPiece;
	}

	public int getPiecesProduced() {
		return piecesProduced;
	}

	@Override
	public double earnings() {
		// TODO Auto-generated method stub
		return getPiecesProduced() * getWagePerPiece();
	}

	@Override
	public String toString() {
		return String.format("pieceworker employee: %s%n%s: $%,.2f; %s: %d", super.toString(), "Wage per Piece",
				getWagePerPiece(), "Pieces Produced", getPiecesProduced());
	}

}
