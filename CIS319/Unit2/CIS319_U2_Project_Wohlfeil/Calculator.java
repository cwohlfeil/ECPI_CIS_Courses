/*
 * Name: Cameron Wohlfeil
 * Date: 03/15/2019
 * Description: Unit 2 Project - Calculator Warmup
 */

import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) { 
        String inputString, strNumber = "";
        boolean decimalExists = false;
        
        // Display instructions 
        System.out.println("Creating a number from characters, one character at a time.");
        System.out.println();

        try (Scanner input = new Scanner(System.in)) {
            // try-with-resources to safely get user input
            do {
                // Do-while so it runs once
                System.out.print("Enter a digit (double digit to stop): ");
                inputString = input.nextLine();

                if (inputString.length() == 1 && !inputString.equals(".")) {
                    // if string is 1-9, add it to strNumber and return
                    // TODO: Error check, not int
                    strNumber += inputString;
                } else if (inputString.length() == 1 && !decimalExists) {
                    // if string is a decimal point and strNumber doesn't have a decimal point, add it to strNumber and return
                    strNumber += inputString;
                    decimalExists = true;
                } else {
                    // all other cases will be treated as exit conditions
                    inputString = "";
                }

                if (strNumber.length() >= 1) {
                    // ignore first iteration when inputString is empty
                    displayNumber(false, strNumber);
                }
                // stop processing input and displaying number if empty string
            } while (inputString.length() == 1);
        } // end try-with-resources

        if (strNumber.length() >= 1) {
            // ignore empty string, display final values
            displayNumber(true, strNumber);
        }
    } // end main
    
    private static void displayNumber(boolean displayFinalValues, String strNumber) {
        // Try/catch for casting failures
        // TODO: proper precision formatting
        try {
            double dblNumber = Double.parseDouble(strNumber);
            
            if (!displayFinalValues) {
                System.out.println(String.format("Number so far: %,.9f", dblNumber));
            } else {
                System.out.println(String.format("Final value as string: %s", strNumber));
                System.out.println(String.format("Final value as a double: %,.9f", dblNumber));
                System.out.println(String.format("Final value as a string with commas: %s",
                        String.format("%.9f", dblNumber)));
            }
        } catch (ClassCastException exc) {
            System.out.println(exc.getMessage());
        }     
    }
}