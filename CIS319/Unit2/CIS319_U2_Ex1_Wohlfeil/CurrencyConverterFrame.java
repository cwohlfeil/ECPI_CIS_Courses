/*
 * Name: Cameron Wohlfeil
 * Date: 03/13/2019
 * Description: Currency Converter frame
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class CurrencyConverterFrame extends JFrame {
    private JLabel dollarLabel, dracLabel, currencyLabel;
    private JTextField dollarField, dracField, convertRateField;
    private JButton convertButton;

    public CurrencyConverter() {
        super("Currency Conversion"); // Window title
        setLayout(new FlowLayout(FlowLayout.TRAILING));

        dollarLabel = new JLabel("Dollars:");
        add(dollarLabel);
        dollarField = new JTextField(5);
        add(dollarField, BorderLayout.LINE_START);

        currencyLabel = new JLabel("Conversion Rate:");
        add(currencyLabel);
        convertRateField = new JTextField(5);
        add(convertRateField, BorderLayout.LINE_START);

        dracLabel = new JLabel("Dracs:");
        add(dracLabel);
        dracField = new JTextField(5);
        dracField.setEditable(false); // readonly
        add(dracField, BorderLayout.LINE_START);

        convertButton = new JButton("Convert");
        add(convertButton);

        ButtonHandler handler = new ButtonHandler();
        convertButton.addActionListener(handler); // call class when button is pressed
        dollarField.addActionListener(handler);
        convertRateField.addActionListener(handler);
    }

    private class ButtonHandler implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent arg) {
            double dollarValue, dracValue, convertRateValue;
            String dollarString, convertRateString;

            dollarString = dollarField.getText();
            convertRateString = convertRateField.getText();

            try {
                dollarValue = Double.parseDouble(dollarString);
                convertRateValue = Double.parseDouble(convertRateString);
                dracValue = dollarValue * convertRateValue;
                String answer = String.format("%.2f%n", dracValue);
                dracField.setText(answer);
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(CurrencyConverter.this,
                        "Please enter a valid dollar amount.", "Error", JOptionPane.ERROR_MESSAGE);
                dracField.setText(""); // clear text field
            }
        }
    }
}
