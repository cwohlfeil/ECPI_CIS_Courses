/*
 * Name: Cameron Wohlfeil
 * Date: 03/13/2019
 * Description: Currency Converter test
 */

import javax.swing.*;

public class CurrencyConverterTest {
    public static void main(String[] args) {
        // obtain user input from JOptionPane input dialogs
        CurrencyConverterFrame currencyConverterFrame = new CurrencyConverterFrame();
        currencyConverterFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        currencyConverterFrame.setSize(200, 150);
        currencyConverterFrame.setVisible(true);
    }
}
