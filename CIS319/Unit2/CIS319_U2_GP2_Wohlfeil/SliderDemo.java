// Fig. 22.4: SliderDemo.java
// Testing SliderFrame.
import javax.swing.JFrame;

public class SliderDemo {
    public static void main(String[] args){ 
        SliderFrame sliderFrame = new SliderFrame(); 
        sliderFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // set the size of the JFrame so that when the circle is at maximum size there is about the same amount of space on each side of the circle
        sliderFrame.setSize(240, 285);
        sliderFrame.setVisible(true); 
    } 
} // end class 