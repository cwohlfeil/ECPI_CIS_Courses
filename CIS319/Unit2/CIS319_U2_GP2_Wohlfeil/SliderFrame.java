// Fig. 22.3: SliderFrame.java
// Using JSliders to size an oval.
import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

public class SliderFrame extends JFrame {
    private final JSlider diameterJSlider; // slider to select diameter
    private final OvalPanel myPanel; // panel to draw circle

    // no-argument constructor
    public SliderFrame() {
        super("Slider Demo");

        // default background color is: light gray, same as the JSlider
        // default foreground color is: Dark gray/black

        myPanel = new OvalPanel(); // create panel to draw circle
        myPanel.setBackground(Color.MAGENTA); // change to magenta
        myPanel.setForeground(Color.GREEN); // set color of the circle

        // Is there a difference between .red and .RED? There is no difference, 
        // they are constants and should be uppercase, lowercase is for compatibility

        // set up JSlider to control diameter value
        // adjust the JSlider so that the square is about one-half of the maximum size
        diameterJSlider = new JSlider(SwingConstants.HORIZONTAL, 0, 120, 10);
        diameterJSlider.setMajorTickSpacing(10); // create tick every 10
        diameterJSlider.setPaintTicks(true); // paint ticks on slider

        // register JSlider event listener
        diameterJSlider.addChangeListener(
            // anonymous inner class
            new ChangeListener() {  
                // handle change in slider value
                @Override
                public void stateChanged(ChangeEvent e) {
                    myPanel.setDiameter(diameterJSlider.getValue());
                } 
            } 
        ); 

        add(diameterJSlider, BorderLayout.SOUTH); 
        add(myPanel, BorderLayout.CENTER); 
    } 
} // end class