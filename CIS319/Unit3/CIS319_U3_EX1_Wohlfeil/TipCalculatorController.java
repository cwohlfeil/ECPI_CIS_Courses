/*
 * Name: Cameron Wohlfeil
 * Date: 03/22/2019
 * Description: Modified Tip Calculator Controller
 */
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;

public class TipCalculatorController {
   // formatters for currency and percentages
   private static final NumberFormat currency = NumberFormat.getCurrencyInstance();
   private static final NumberFormat percent = NumberFormat.getPercentInstance();

   private BigDecimal tipPercentage = new BigDecimal(0.15); // 15% default

   // GUI controls defined in FXML and used by the controller's code
   @FXML
   private TextField amountTextField;

   @FXML
   private Label tipPercentageLabel;

   @FXML
   private Slider tipPercentageSlider;

   @FXML
   private TextField tipTextField;

   @FXML
   private TextField totalTextField;

   @FXML
   private TextField totalPerPersonTextField;

   @FXML
   private ComboBox partySizeDropdown;

    @FXML
   private ToggleGroup paymentToggleGroup;

   // calculates and displays the tip and total amounts
   @FXML
   private void calculateButtonPressed(ActionEvent event) {
      try {
         BigDecimal amount = new BigDecimal(amountTextField.getText());
         BigDecimal tip = amount.multiply(tipPercentage);
         BigDecimal total = amount.add(tip);
         RadioButton paymentOption = (RadioButton) paymentToggleGroup.getSelectedToggle();
         int partySize = Integer.parseInt((String) partySizeDropdown.getValue());

         if (paymentOption.getText().equals("Cash")) {
             BigDecimal cashDiscount = amount.multiply(new BigDecimal((0.3)));
             total = amount.subtract(cashDiscount);
         }

         BigDecimal perPersonTotal = total.divide(new BigDecimal(partySize), RoundingMode.HALF_UP);

         totalPerPersonTextField.setText(currency.format(perPersonTotal));
         tipTextField.setText(currency.format(tip));
         totalTextField.setText(currency.format(total));
      } catch (NumberFormatException ex) {
         amountTextField.setText("Enter amount");
         amountTextField.selectAll();
         amountTextField.requestFocus();
      }
   }

   // called by FXMLLoader to initialize the controller
   public void initialize() {
      // Set values for partySizeDropdown
      partySizeDropdown.setItems(FXCollections.observableArrayList(
              "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"));

      // 0-4 rounds down, 5-9 rounds up
      currency.setRoundingMode(RoundingMode.HALF_UP);

      // listener for changes to tipPercentageSlider's value
      tipPercentageSlider.valueProperty().addListener(
          (ov, oldValue, newValue) -> {
             tipPercentage = BigDecimal.valueOf(newValue.intValue() / 100.0);
             tipPercentageLabel.setText(percent.format(tipPercentage));
          }
      );
   }
}