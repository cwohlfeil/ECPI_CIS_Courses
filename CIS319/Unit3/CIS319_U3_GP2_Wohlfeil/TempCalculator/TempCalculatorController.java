/*
 * Name: Cameron Wohlfeil
 * Date: 03/15/2019
 * Description: Temperature Calculator Controller for JavaFX GUI
 */

package TempCalculator;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;

import javax.swing.JOptionPane;

public class TempCalculatorController {
   // GUI controls defined in FXML and used by the controller's code
   @FXML
   private BorderPane borderPane;

   @FXML
   private FlowPane flowPane;

   @FXML
   private TextField fahrenField;

   @FXML
   private Label fahrenLabel;

   @FXML
   private Button calculateButton;

   @FXML
   private Label resultLabel;

   // calculates and displays the tip and total amounts
   @FXML
   private void calculateButtonPressed(ActionEvent event) {
      double fahrenValue, celciusValue;
      String fahrenString;

      fahrenString = fahrenField.getText();

      try {
         fahrenValue = Double.parseDouble(fahrenString);
         celciusValue = (fahrenValue - 32) * 5 / 9;

         String answer = String.format("%s%.2f%n", "Your temperature in celcius is ", celciusValue);
         resultLabel.setText(answer);
      } catch (NumberFormatException exception) {
         JOptionPane.showMessageDialog(null, "Please enter a valid temperature",
                 "Error", JOptionPane.ERROR_MESSAGE);
      }
   }
}