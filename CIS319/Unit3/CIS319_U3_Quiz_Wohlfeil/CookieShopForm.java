
/*
 * Name: Cameron Wohlfeil
 * Date: 3/22/2019
 * Description: Unit 3 Quiz - Cookie shop form
 *
 * Create a form for Cookie shop  (CookieShopForm.java)
 * Your form should have the following
 * 1) Dropdown list with 3 sizes of cookies (and a please select a value label)
 * <Please Select a value>
 * 1/2 dozen ($3.00)
 * 1 dozen ($5.50)
 * 2 dozen ($11.00)
 * 2) A textbox that says how many of a particular size they want
 * 3) Two checkboxes that say delivery ($5)  and gift wrapped? ($3).
 * 4) Have two Buttons on the Screen (Calculate) and (Clear)
 * Calculate Button should validate that a size was chosen and
 * a valid amount was entered. Show an error in an JOptionPane if they weren't.
 * If all entries were valid, calculate the subtotal, the tax (5%) and total cost
 * and put that into 3 different Read-only textboxes
 * The clear button should clear all textfields, dropdowns, and checkboxes.
 * Must be done in JavaFX for full credit.
 */

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class CookieShopForm extends Application {

	@Override
	public void start(Stage stage) throws Exception {
		Parent root = FXMLLoader.load(getClass().getResource("CookieShopForm.fxml"));

		Scene scene = new Scene(root);
		stage.setTitle("Cookie Shop Form");
		stage.setScene(scene);
		stage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
