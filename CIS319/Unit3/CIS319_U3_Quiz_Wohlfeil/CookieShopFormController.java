
/*
 * Name: Cameron Wohlfeil
 * Date: 3/22/2019
 * Description: Unit 3 Quiz - Cookie shop form controller
 */

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import javax.swing.*;

public class CookieShopFormController {
	@FXML
	private TextField quantityTextField, totalTextField, subTotalTextField, taxTextField;
	@FXML
	private ComboBox<String> sizeComboBox;
	@FXML
	private CheckBox deliveryCheckBox, giftCheckBox;

	@FXML
	public void calculateTotal(ActionEvent event) {
		double total, subTotal, tax;

		try {
			int quantity = Integer.parseInt(quantityTextField.getText());
			String size = sizeComboBox.getValue();

			// Validate quantity
			if (quantity < 1) {
				throw new NumberFormatException();
			}

			// Validate cookie size
			if (size.equals("1/2 dozen ($3.00)")) {
				subTotal = quantity * 3.0;
			} else if (size.equals("1 dozen ($5.50)")) {
				subTotal = quantity * 5.5;
			} else if (size.equals("2 dozen ($11.00)")) {
				subTotal = quantity * 11.0;
			} else {
				throw new IllegalArgumentException();
			}

			if (deliveryCheckBox.isSelected()) {
				subTotal += 5.0;
			}

			if (giftCheckBox.isSelected()) {
				subTotal += 3.0;
			}

			tax = subTotal * 0.05;
			total = subTotal + tax;

			// Set calculated text fields
			subTotalTextField.setText(String.format("$%,.2f", subTotal));
			taxTextField.setText(String.format("$%,.2f", tax));
			totalTextField.setText(String.format("$%,.2f", total));
		} catch (NumberFormatException e) {
			final JPanel panel = new JPanel();
			JOptionPane.showMessageDialog(panel,
					"Invalid quantity, please try again.", "Error", JOptionPane.ERROR_MESSAGE);
		} catch (IllegalArgumentException e) {
			final JPanel panel = new JPanel();
			JOptionPane.showMessageDialog(panel,
					"You must choose a cookie size, please try again.", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	public void clearForm() {
		totalTextField.clear();
		taxTextField.clear();
		subTotalTextField.clear();
		quantityTextField.clear();
		deliveryCheckBox.setSelected(false);
		giftCheckBox.setSelected(false);
	}

	public void initialize() {
		// Set up the combo box with options
		sizeComboBox.setItems(FXCollections.observableArrayList(
				"<Please Select a value>", "1/2 dozen ($3.00)", "1 dozen ($5.50)", "2 dozen ($11.00)"));
	}
}
