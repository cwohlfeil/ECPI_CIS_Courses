/*
 * Name: Cameron Wohlfeil
 * Date: 3/22/2019
 * Description: BMI Calculator
 */

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class BMICalculator extends Application {
	@Override
	public void start(Stage stage) throws Exception {
		Parent root = FXMLLoader.load(getClass().getResource("BMICalculator.fxml"));
		Scene scene = new Scene(root);
		stage.setTitle("BMI Calculator");
		stage.setScene(scene);
		stage.show();
	}
	public static void main(String[] args) {
		launch(args);
	}
}
