/*
 * Name: Cameron Wohlfeil
 * Date: 3/22/2019
 * Description: BMI Calculator
 */

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import javax.swing.*;

public class BMICalculatorController {
	// JPanel for errors
	private final JPanel panel = new JPanel();

	@FXML
	private TextField weightTextField, heightTextField, bmiTextField, bmiCategoryTextField;

	@FXML
	private ComboBox unitComboBox;

	@FXML
	public void calculate(ActionEvent event) {
		try {
			double height = Double.parseDouble(heightTextField.getText());
			double weight = Double.parseDouble(weightTextField.getText());
			String units = (String) unitComboBox.getValue();
			double bmi;

			// Validate input
			if (height < 1 || weight < 1) {
				throw new NumberFormatException();
			}

			if (units.equals("Imperial")) {
				bmi = weight * 703 / Math.pow(height, 2);
			} else if (units.equals("Metric")) {
				bmi = weight / Math.pow(height, 2);
			} else {
				throw new IllegalArgumentException();
			}

			// Set output
			bmiTextField.setText(String.format("%.1f", bmi));

			if (bmi < 18.5) {
				bmiCategoryTextField.setText("Underweight");
			} else if (bmi >= 18.5 && bmi < 24.9) {
				bmiCategoryTextField.setText("Normal");
			} else if (bmi >= 25 && bmi < 29.9) {
				bmiCategoryTextField.setText("Overweight");
			} else if (bmi >= 30) { // Obese
				bmiCategoryTextField.setText("Obese");
			}
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(panel, "You must enter a valid measurement, please try again.",
					"Error", JOptionPane.ERROR_MESSAGE);
		} catch (IllegalArgumentException e) {
			JOptionPane.showMessageDialog(panel, "You must select a unit of measurement, please try again.",
					"Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	public void clearForm() {
		heightTextField.clear();
		weightTextField.clear();
		bmiTextField.clear();
		bmiCategoryTextField.clear();
	}

	public void initialize() {
		// Set up the combo box with options
		unitComboBox.setItems(FXCollections.observableArrayList("Imperial", "Metric"));
	}
}
