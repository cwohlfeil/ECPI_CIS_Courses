/*
 * Name: Cameron Wohlfeil
 * Date: 03/15/2019
 * Description: Currency Converter Controller for JavaFX GUI
 */

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;

import javax.swing.JOptionPane;

public class CurrencyConverterController {
   // GUI controls defined in FXML and used by the controller's code
   @FXML
   private BorderPane borderPane;

   @FXML
   private FlowPane flowPane;

   @FXML
   private TextField dollarField;

   @FXML
   private Label dollarLabel;

   @FXML
   private TextField conversionRateField;

   @FXML
   private Label conversionRateLabel;

   @FXML
   private Label dracsLabel;

   @FXML
   private Button calculateButton;

   @FXML
   private Label outputLabel;

   // calculates and displays the tip and total amounts
   @FXML
   private void calculateButtonPressed(ActionEvent event) {
      double dollarValue, conversionRateValue, dracValue;
      String dollarString, conversionRateString, dracString;

      dollarString = dollarField.getText();
      conversionRateString = conversionRateField.getText();

      try {
         dollarValue = Double.parseDouble(dollarString);
         conversionRateValue = Double.parseDouble(conversionRateString);
         dracValue = dollarValue * conversionRateValue;
         dracString = String.format("%,.2f", dracValue);
         outputLabel.setText(dracString);
      } catch (NumberFormatException exception) {
         JOptionPane.showMessageDialog(null,
                 "Please enter a valid dollar amount and conversion rate",
                 "Error", JOptionPane.ERROR_MESSAGE);
      }
   }
}