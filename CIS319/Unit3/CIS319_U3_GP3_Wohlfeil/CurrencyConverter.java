/*
 * Name: Cameron Wohlfeil
 * Date: 03/15/2019
 * Description: Currency Converter with JavaFX GUI
 */

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class CurrencyConverter extends Application {
   @Override
   public void start(Stage stage) throws Exception {
      Parent root = 
         FXMLLoader.load(getClass().getResource("CurrencyConverter.fxml"));

      Scene scene = new Scene(root); // attach scene graph to scene
      stage.setTitle("Currency Converter"); // displayed in window's title bar
      stage.setScene(scene); // attach scene to stage
      stage.show(); // display the stage
   }

   public static void main(String[] args) {
      // create a CurrencyConverter object and call its start method
      launch(args); 
   }
}
