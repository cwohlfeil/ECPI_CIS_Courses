/* 
 * Name: Cameron Wohlfeil
 * Date: 3/5/2019
 * Description: Department composition test
 *
 * 1) Ask the user for information for multiple department objects. 
 * 2) User should enter department name and office number. 
 * 3) Stop when the user enters ZZZ for the department name.
 * 4) Each Department should have two CommissionEmployees and two BasePlusCommissionEmployees.  
 * 5) Ask the user for the appropriate information to create these objects.  
 * 6) Store your departments in an ArrayList and be sure to catch any exceptions.
 * 7) Once the user is done entering departments, loop through your ArrayList and call the toString method for each Department object. 
 * 8) Make sure you catch all appropriate exceptions in the main.
 */

import java.util.ArrayList;
import java.util.Scanner;

public class DepartmentTest {

    public static void main(String[] args) {
        // Local variables
        ArrayList<Department> departments = new ArrayList<>();
        String departmentName, officeNumber;

        // try-with-resources to automatically handle Scanner
        try (Scanner input = new Scanner(System.in)) {
            // Get initial department name
            System.out.print("Department Name (ZZZ to quit): ");
            departmentName = input.nextLine();

            // if trimmed lowercase department name isn't zzz, continue
            while (!departmentName.trim().toLowerCase().equals("zzz")) {
                // Department loop variables
                int basePlusCommCounter = 0, commissionCounter = 0;
                ArrayList<CommissionEmployee> employees = new ArrayList<>();

                // Get department office number
                System.out.print("Office Number: ");
                officeNumber = input.nextLine();

                System.out.println("Enter Employee Information (must be 2 of each type)");

                while (basePlusCommCounter < 2 || commissionCounter < 2) {
                    // create the next employee
                    String firstName, lastName, ssn;
                    int employeeType;
                    double grossSales, commissionRate, baseSalary;

                    // get employee type
                    System.out.print("Employee Type (1 for commission, " +
                            "2 for salary plus commission): ");
                    employeeType = input.nextInt();
                    input.nextLine(); // clear newline character
                    System.out.print("Employee First Name: ");
                    firstName = input.nextLine();
                    System.out.print("Employee Last Name: ");
                    lastName = input.nextLine();
                    System.out.print("Employee SSN: ");
                    ssn = input.nextLine();
                    System.out.print("Employee Gross Sales: ");
                    grossSales = input.nextDouble();
                    System.out.print("Employee Commission Rate: ");
                    commissionRate = input.nextDouble();

                    try {
                        switch (employeeType) {
                            case 1:
                                // try to create the CommissionEmployee, catch any exceptions (class only throws one type)
                                employees.add(new CommissionEmployee(firstName, lastName, ssn,
                                        grossSales, commissionRate));
                                commissionCounter++;
                                break;
                            case 2:
                                // Get the baseSalary
                                System.out.print("Employee Base Salary: ");
                                baseSalary = input.nextDouble();
                                // try to create the BasePlusCommissionEmployee
                                employees.add(new BasePlusCommissionEmployee(firstName, lastName, ssn, grossSales,
                                        commissionRate, baseSalary));
                                basePlusCommCounter++;

                                break;
                        } // end switch
                    } catch (IllegalArgumentException e) {
                        System.out.print(e.getMessage());
                    }
                } // end while

                // try to create the department, catch any exceptions (class only throws one type)
                try {
                    departments.add(new Department(departmentName, officeNumber, employees));
                } catch (IllegalArgumentException e) {
                    System.out.print(e.getMessage());
                }

                // clear newline character and get next iteration department name
                input.nextLine();
                System.out.print("Department Name (ZZZ to quit): ");
                departmentName = input.nextLine();
            } // end while
        }// end try-with-resources

        // print out all departments
        for (Department currentDepartment : departments) {
            System.out.println(currentDepartment);
        }
    } // end main function
}