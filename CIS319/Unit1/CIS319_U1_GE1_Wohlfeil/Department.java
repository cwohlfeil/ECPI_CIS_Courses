/* 
 * Name: Cameron Wohlfeil
 * Date: 3/5/2019
 * Description: Department composition class
 *
 * 1) Create a class Department that will contain CommissionEmployee objects and BasePlusCommissionEmployee objects. 
 * 2) Each department will have four employees – two CommissionEmployee objects and two BasePlusCommissionEmployee objects. 
 * 3) Each department will have a department name and an office number (both Strings), along with the employees. 
 * 4) The class Department will need a toString method which will display the name of the department, the office number, and all four employees in the department.
 */

import java.util.ArrayList;

public class Department {
    // Final variables so they can only be set in the constructor
    private final String departmentName;                               
    private final String officeNumber;
    
    // private ArrayList of employees
    private final ArrayList<CommissionEmployee> employees;

    // constructor, takes list of employees
    public Department(String departmentName, String officeNumber,
                      ArrayList<CommissionEmployee> employees) {
        // if departmentName is invalid throw exception
        if (departmentName == null || departmentName.trim().isEmpty()) {
            throw new IllegalArgumentException("Department name must be a valid string.");
        } else {
            this.departmentName = departmentName.trim();
        }
        
        // if officeNumber is invalid throw exception
        if (officeNumber == null || officeNumber.trim().isEmpty()) {
            throw new IllegalArgumentException("Office number must be a valid string.");
        } else {
            this.officeNumber = officeNumber.trim();
        }

        // if employees is invalid throw an exception
        if (employees == null) {
            throw new IllegalArgumentException("Employees must be an ArrayList of CommissionEmployee objects.");
        } else {
            // Otherwise set employees to the ArrayList
            this.employees = employees;
        }
    } // end constructor 
    
    public String getDepartmentName() { return departmentName; }
    
    public String getOfficeNumber() { return officeNumber; }
    
    public ArrayList<CommissionEmployee> getEmployees() { return employees; }

    // return String representation of object
    @Override 
    public String toString() {
        return String.format("%n%s: %s%n%s: %s%n%s%n%s%n%s",
                             "Department", getDepartmentName(),
                             "Office Number", getOfficeNumber(), 
                             "Employees",
                             "----------------",
                             getEmployees().toString());
    }  
}