// Fig. 10.9: PayrollSystemTest.java
// Employee hierarchy test program.

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PayrollSystemTest {
   public static void main(String[] args) {
      Scanner input = new Scanner(System.in);
      List<Employee> employees = new ArrayList<>();

      String firstName, lastName, ssn;
      int empType;
      double weeklySalary, hourlyRate, hoursWorked, commissionRate, grossSales, baseSalary;

      System.out.print("Enter employee type: 1 for salary, 2 for hourly, " +
              "3 for commission, 4 for base plus commission, -1 to quit): ");
      empType = input.nextInt();

      while (empType != -1) {
         input.nextLine(); // clear newline
         System.out.print("Enter first name: ");
         firstName = input.nextLine();
         System.out.print("Enter last name: ");
         lastName = input.nextLine();
         System.out.print("Enter SSN: ");
         ssn = input.nextLine();

         try {
            switch (empType) {
               case 1: // Salary
                  System.out.print("Enter weekly salary: ");
                  weeklySalary = input.nextDouble();
                  SalariedEmployee se = new SalariedEmployee(firstName, lastName, ssn, weeklySalary);
                  employees.add(se);
                  break;
               case 2: // hourly
                  System.out.print("Enter hourly rate: ");
                  hourlyRate = input.nextDouble();
                  System.out.print("Enter hours worked: ");
                  hoursWorked = input.nextDouble();
                  HourlyEmployee hourly = new HourlyEmployee(firstName, lastName, ssn, hourlyRate, hoursWorked);
                  employees.add(hourly);
                  break;
               case 3:
               case 4: // hourly
                  System.out.print("Enter commission rate: ");
                  commissionRate = input.nextDouble();
                  System.out.print("Enter gross sales: ");
                  grossSales = input.nextDouble();

                  if (empType == 3) {
                     CommissionEmployee commission = new CommissionEmployee(
                             firstName, lastName, ssn, grossSales, commissionRate);
                     employees.add(commission);
                  } else {
                     System.out.print("Enter base salary: ");
                     baseSalary = input.nextDouble();
                     BasePlusCommissionEmployee basePlusCommission = new BasePlusCommissionEmployee(
                             firstName, lastName, ssn, grossSales, commissionRate, baseSalary);
                     employees.add(basePlusCommission);
                  }
                  break;
            }
         } catch (IllegalArgumentException e) {
            System.out.print(e.getMessage());
         }

         System.out.print("Enter employee type: 1 for salary, 2 for hourly, " +
                 "3 for commission, 4 for base plus commission, -1 to quit): ");
         empType = input.nextInt();
      }

      for (Employee currentEmployee : employees) {
         System.out.println("Before raise: " + currentEmployee);

         if (currentEmployee instanceof HourlyEmployee) {
            HourlyEmployee employee = (HourlyEmployee) currentEmployee;
            employee.setWage(1.2 * employee.getWage());
         } else if (currentEmployee instanceof  SalariedEmployee) {
            SalariedEmployee employee = (SalariedEmployee) currentEmployee;
            employee.setWeeklySalary(1000 + employee.getWeeklySalary());
         } else if (currentEmployee instanceof  BasePlusCommissionEmployee) {
            BasePlusCommissionEmployee employee = (BasePlusCommissionEmployee) currentEmployee;
            employee.setCommissionRate(1.1 * employee.getCommissionRate());
            employee.setBaseSalary(1000.0 + employee.getBaseSalary());
         } else if (currentEmployee instanceof  CommissionEmployee) {
            CommissionEmployee employee = (CommissionEmployee) currentEmployee;
            employee.setCommissionRate(1.1 * employee.getCommissionRate());
         }

         System.out.println("After raise: " + currentEmployee);
         input.close();
      }
   } // end main
} // end class PayrollSystemTest

