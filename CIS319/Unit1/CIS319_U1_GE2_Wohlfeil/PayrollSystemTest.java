/*
 * Name: Cameron Wohlfeil
 * Date: 3/6/2019
 * Description: Payroll System test
 *
 * 1) Create an ArrayList that holds at least one of each type of worker.  
 * 2) Make sure you ask the user for all the appropriate input to create your classes.  
 * 3) Allow the user to create workers until they enter -1 for the type of employee.  
 * 4) Also ensure you catch all appropriate exceptions.
 * 5) One the user is done entering workers, loop through your ArrayList and call the toString() method of each worker.
 * 
 * Additionally, do the following for each type of worker: 
 * 1) Any salary workers will a $10,000 annual raise.
 * 2) Any PieceWorker will get an increase of 5 cents per piece
 * 3) Any hourly employee will get 2 dollars more an hour
 * 4) Any commission employee will get a 2.5% more commission
 * 5) Any base plus commission employee will get 5% more commission.
 * 6) After the increases are done for each employee, print out the arrayList again.
 */

import java.util.Arrays;
import java.util.Scanner;
import java.util.ArrayList;

public class PayrollSystemTest {
    public static void main(String[] args) {
        // Local variables
        int[] employeeTypeCounter = new int[5];
        int employeeType;
        double payRate, payModifier, salary, totalEarnings = 0;
        String firstName, lastName, socialSecurityNumber;
        ArrayList<Employee> employees = new ArrayList<>();
        Employee employee;
        boolean contains = Arrays.asList(employeeTypeCounter).contains(0);
        
        // try-with-resources to handle Scanner  
        try (Scanner input = new Scanner(System.in)) {
            System.out.println("Payroll System");

            // Get employee type for first iteration
            System.out.println("1 for hourly, 2 for salary, 3 for commission, ");
            System.out.print("4 for piece worker, 5 for base plus commission (-1 to quit): ");
            employeeType = input.nextInt();
            
            while (employeeType != -1) {
                input.nextLine();  // handle newline
                
                // Get general employee info
                System.out.print("Enter employee first name: ");
                firstName = input.nextLine();

                System.out.print("Enter employee last name: ");
                lastName = input.nextLine();

                System.out.print("Enter employee social security number: ");
                socialSecurityNumber = input.nextLine();

                // Get class specific employee info, try/catch to get exceptions 
                try {
                    switch (employeeType) {
                        case 1: // hourly
                            System.out.print("Enter wages: ");
                            payRate = input.nextDouble();
                            System.out.print("Enter hours: ");
                            payModifier = input.nextDouble();
                            employee = new HourlyEmployee(firstName, lastName, 
                                                          socialSecurityNumber, 
                                                          payRate, payModifier);
                            employees.add(employee);
                            employeeTypeCounter[0]++;
                            break;
                        case 2: // salary
                            System.out.print("Enter weekly salary: ");
                            salary = input.nextDouble();
                            employee = new SalariedEmployee(firstName, lastName, 
                                                            socialSecurityNumber, 
                                                            salary);
                            employees.add(employee);
                            employeeTypeCounter[1]++;
                            break;
                        case 3: // commission
                            System.out.print("Enter gross sales: ");
                            payModifier = input.nextDouble();
                            System.out.print("Enter commission rate: ");
                            payRate = input.nextDouble();
                            employee = new CommissionEmployee(firstName, lastName, 
                                                              socialSecurityNumber,
                                                              payModifier, payRate);
                            employees.add(employee);
                            employeeTypeCounter[2]++;
                            break;
                        case 4: // pieces worker
                            System.out.print("Enter wages per piece: ");
                            payRate = input.nextDouble();
                            System.out.print("Enter pieces produced: ");
                            payModifier = input.nextDouble();
                            employee = new PieceWorker(firstName, lastName, 
                                                       socialSecurityNumber,
                                                       payRate, payModifier);
                            employees.add(employee);
                            employeeTypeCounter[3]++;
                            break;
                        case 5: // base plus commission
                            System.out.print("Enter base salary: ");
                            salary = input.nextDouble();
                            System.out.print("Enter gross sales: ");
                            payModifier = input.nextDouble();
                            System.out.print("Enter commission rate: ");
                            payRate = input.nextDouble();
                            employee = new BasePlusCommissionEmployee(firstName, lastName, 
                                                                      socialSecurityNumber,
                                                                      payModifier, payRate, 
                                                                      salary);
                            employees.add(employee);
                            employeeTypeCounter[4]++;
                            break;
                    } // end switch
                } catch (IllegalArgumentException e) {
                    System.out.println(e.getMessage());
                } // end try/catch
                
                // Recalculate contains with new employee
                contains = Arrays.asList(employeeTypeCounter).contains(0);
                if (contains) {
                    System.out.println("WARNING: You do not have an employee of each type yet.");
                } 
                
                // Get employee type for next iteration
                System.out.println("1 for hourly, 2 for salary, 3 for commission, ");
                System.out.print("4 for piece worker, 5 for base plus commission (-1 to quit): ");
                employeeType = input.nextInt();
            } // end while
        } // end try-with-resources
        
        // Ensure there is an employee of each type to continue, if not then exit
        if (contains)  {
            System.out.println("ERROR: You do not have an employee of each type. Exiting...");
        } else {
            System.out.println();
            System.out.println("Employee Information (before raises)");
            // Loop through ArrayList and print out employee before raises, add earning to total
            for (Employee currentEmployee : employees) {
                if (currentEmployee != null) {
                    System.out.printf("%n%s", currentEmployee.toString());
                    totalEarnings += currentEmployee.earnings();
                }
            }

            // Print out total earnings and count of account types before raises
            System.out.printf("%nTotal hourly employees: %d", employeeTypeCounter[0]);
            System.out.printf("%nTotal salary employees: %d", employeeTypeCounter[1]);
            System.out.printf("%nTotal commission employees: %d", employeeTypeCounter[2]);
            System.out.printf("%nTotal piece workers: %d", employeeTypeCounter[3]);
            System.out.printf("%nTotal base plus commission employees: %d", employeeTypeCounter[4]);
            System.out.printf("%nTotal earnings before raises: $%,.2f%", totalEarnings);
            
            // Employee counts are the same, no need to print it out again
            System.out.println();
            System.out.println("Employee Information (after raises)");
            // Loop through ArrayList and print out employee after raises, add earning to total
            for (Employee currentEmployee : employees) {
                if (currentEmployee != null) {
                    // Give raises based on object type
                    if (currentEmployee instanceof HourlyEmployee) {
                        HourlyEmployee hourlyEmployee = (HourlyEmployee) currentEmployee;
                        hourlyEmployee.setWage(hourlyEmployee.getWage() + 2);
                    } else if (currentEmployee instanceof SalariedEmployee) {
                        SalariedEmployee salariedEmployee = (SalariedEmployee) currentEmployee;
                        salariedEmployee.setWeeklySalary(salariedEmployee.getWeeklySalary() + 10000);
                    } else if (currentEmployee instanceof PieceWorker) {
                        PieceWorker pieceWorker = (PieceWorker) currentEmployee;
                        pieceWorker.setWagePerPiece(pieceWorker.getWagePerPiece() + 0.05);
                    } else if (currentEmployee instanceof BasePlusCommissionEmployee) {
                        BasePlusCommissionEmployee basePlusEmployee = (BasePlusCommissionEmployee) currentEmployee;
                        basePlusEmployee.setCommissionRate(basePlusEmployee.getCommissionRate() + 0.05);
                    } else if (currentEmployee instanceof CommissionEmployee) {
                        // CommissionEmployee must come last since BasePlusCommissionEmployee inherits
                        CommissionEmployee commissionEmployee = (CommissionEmployee) currentEmployee;
                        commissionEmployee.setCommissionRate(commissionEmployee.getCommissionRate() + 0.025);
                    } 
                    
                    // Code to be run on all objects
                    System.out.printf("%n%s", currentEmployee.toString());
                    totalEarnings += currentEmployee.earnings();
                }
            }

            // Print out total earnings and count of account types after raises
            System.out.printf("%nTotal earnings after raises: $%,.2f", totalEarnings);
        } // end if 
    } // end main
} // end class
