/*
 * Name: Cameron Wohlfeil
 * Date: 3/6/2019
 * Description: Piece Worker Subclass
 *
 * 1) PieceWorker should inherit from Employee.
 * 2) Pieceworker should create private variables for wage (wage per piece) and pieces (number of pieces produced)
 * 3) The constructor of PieceWorker should take in enough information to instantiate the super class, as well as
 *    any values the PieceWorker class needs. The constructor should call the set methods
 * 4) Each private variable should have a get/set method and the sets should validate that wages and pieces are
 *    greater than 0. It should throw an exception if not.
 * 5) This class should override the earnings and toString() methods from the base class.
 */

public class PieceWorker extends Employee {
    private double wagePerPiece, piecesProduced;

    public PieceWorker(String firstName, String lastName, String socialSecurityNumber,
                       double wagePerPiece, double piecesProduced) {
        super(firstName, lastName, socialSecurityNumber);
        setWagePerPiece(wagePerPiece);
        setPiecesProduced(piecesProduced);
    }

    // Setters and getters
    public double getWagePerPiece() { return wagePerPiece; }

    public void setWagePerPiece(double wagePerPiece) { 
        if (wagePerPiece < 0) {
            throw new IllegalArgumentException("Commission rate must be > 0.0 and < 1.0");
        }
        this.wagePerPiece = wagePerPiece; 
    }

    public double getPiecesProduced() { return piecesProduced; }

    public void setPiecesProduced(double piecesProduced) { this.piecesProduced = piecesProduced; }

    // Override inherited methods
    @Override
    public double earnings () { return getWagePerPiece() * getPiecesProduced(); }

    @Override
    public String toString() {
        return String.format("%s: %s%n%s: $%,.2f; %s: %.2f",
                             "Piece Worker", super.toString(),
                             "Wage Per Piece", getWagePerPiece(),
                             "Pieces Produced", getPiecesProduced());
    }
}
