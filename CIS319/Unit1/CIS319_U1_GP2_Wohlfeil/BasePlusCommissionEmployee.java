// Name: Cameron Wohlfeil
// Date: 03/01/2019

public class BasePlusCommissionEmployee
{
   private double baseSalary; // base salary per week
   private CommissionEmployee commissionEmployee; // composition

   // six-argument constructor
   public BasePlusCommissionEmployee(String firstName, String lastName, 
      String socialSecurityNumber, double grossSales, 
      double commissionRate, double baseSalary)
   {
      // if baseSalary is invalid throw exception
      if (baseSalary < 0.0)                   
         throw new IllegalArgumentException(
            "Base salary must be >= 0.0");
      this.baseSalary = baseSalary;

      commissionEmployee = new CommissionEmployee(firstName, lastName, socialSecurityNumber,
              grossSales, commissionRate);
   }

   public String getFirstName() {
      return commissionEmployee.getFirstName();
   }

   // set base salary
   public void setBaseSalary(double baseSalary)
   {
      if (baseSalary < 0.0)                   
         throw new IllegalArgumentException(
            "Base salary must be >= 0.0");  

      this.baseSalary = baseSalary;                
   } 

   // return base salary
   public double getBaseSalary()
   {
      return baseSalary;
   } 

   // calculate earnings
   public double earnings()
   {
      return getBaseSalary() + commissionEmployee.earnings();
   }

   public String toString()
   {
      return String.format("%s %s%n%s: %.2f", "base-salaried",
         commissionEmployee.toString(), "base salary", getBaseSalary());
   } 
} // end class BasePlusCommissionEmployee
