// Name: Cameron Wohlfeil
// Date: 03/01/2019

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CompositionTest {
   public static void main(String[] args) {
      String firstName, lastName, ssn;
      double grossSales, commissionRate, baseSalary;

      Scanner input = new Scanner(System.in);
      List<BasePlusCommissionEmployee> employees = new ArrayList<>();


      System.out.print("Enter first name (ZZZ to quit): ");
      firstName = input.nextLine();
      while (!firstName.equalsIgnoreCase("ZZZ")) {
         System.out.print("Enter last name: ");
         lastName = input.nextLine();
         System.out.print("Enter SSN: ");
         ssn = input.nextLine();
         System.out.print("Enter gross sales: ");
         grossSales = input.nextDouble();
         System.out.print("Enter commission rate: ");
         commissionRate = input.nextDouble();
         System.out.print("Enter base salary: ");
         baseSalary = input.nextDouble();
         input.nextLine(); // clear newline character

         try {
            BasePlusCommissionEmployee emp = new BasePlusCommissionEmployee(
                    firstName, lastName, ssn, grossSales, commissionRate, baseSalary);
            employees.add(emp);
         } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
         }

         System.out.print("Enter first name (ZZZ to quit): ");
         firstName = input.nextLine();
      }

      for (BasePlusCommissionEmployee emp : employees) {
         System.out.println(emp);
      }

      input.close();
   } // end main
} // end class
