
/*
 * Name: Cameron Wohlfeil
 * Date: 03/01/2019
 * Description: Duplicates, using an ArrayList
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DuplicatesArrayList {

	public static void main(String[] args) {
		// Declarations
		List<Integer> numbers = new ArrayList<>();
		int number, duplicateCount = 0, outOfRangeCount = 0;

		// Prepare scanner for user input
		Scanner input = new Scanner(System.in);

		do {

			System.out.print("Enter number (-1) to quit: ");
			number = input.nextInt();

			if (number >= 10 && number <= 100) {
				// Check for duplicates
				if (!numbers.contains(number)) {
					numbers.add(number);
				} else {
					// Display error and increase duplicate count
					System.out.printf("%d has already been entered%n", number);
					duplicateCount++;
				}

			} else {
				// Display error and increase out of range count
				System.out.println("Error, number must be between 10 and 100");
				outOfRangeCount++;
			}

			// Display completed part of numbers array
			for (Integer num : numbers) {
				System.out.printf("%d ", num);
			}
			// New line for input
			System.out.println();

		} while (number != -1); // End while

		// Display final output
		System.out.printf("You entered %d duplicate numbers and %d numbers outside the range of 10-100", duplicateCount,
				outOfRangeCount);

		input.close();

	}

}
