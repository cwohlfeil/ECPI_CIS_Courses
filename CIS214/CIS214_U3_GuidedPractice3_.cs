// Fig. 6.2: ForCounter.cs
// Counter-controlled repetition with the for repetition statement
using System;

public class ForCounter {
    public static void Main(string[] args) {
        // for statement header includes initialization, loop-continuation condition and increment
        for (int counter = 1; counter <= 10; ++counter)
            Console.Write("{0} ", counter);

        Console.WriteLine(); // output a newline
    } // end main
} // end class ForCounter