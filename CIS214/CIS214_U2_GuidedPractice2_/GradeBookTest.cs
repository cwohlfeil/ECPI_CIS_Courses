using System;

public class GradeBookTest {
    public static void Main(string[] args) {
        GradeBook myGradeBook = new GradeBook();

        Console.WriteLine("Please enter the course name: ");
        string name = Console.ReadLine();
        Console.WriteLine();

        myGradeBook.DisplayMessage(name);
    }
}