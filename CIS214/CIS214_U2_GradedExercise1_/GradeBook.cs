// GradeBook class
using System;

public class GradeBook {
    // Getter and setter for CourseName
    public string CourseName {get; set;}
    // Getter and setter for InstructorName
    public string InstructorName {get; set;}

    // GradeBook constructor
    public GradeBook(string course, string instructor) {
        CourseName = course;
        InstructorName = instructor;

    }

    // Output method
    public void DisplayMessage() {
        Console.WriteLine("Welcome to the grade book for {0}", CourseName);
        Console.WriteLine("This course is presented by {0}", InstructorName);
    }
}