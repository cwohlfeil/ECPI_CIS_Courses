﻿using System;

namespace FinalPart2
{
    class CheckingAccount : Account // Inherit from base account 
    {
        private decimal TransactionFee { get; set; } // Private variable for transcation fee

        public CheckingAccount(decimal initialBalance, decimal initialTransactionFee) : base(initialBalance)
        {
            // Constructor, inherets from base class, sets initial transaction fee.
            TransactionFee = initialTransactionFee;
        }

        // Credit override method, uses base Credit method with extra math for transaction fee
        public override decimal Credit(decimal credit) => base.Credit(credit - TransactionFee);

        // Debit override method, uses base Debit method with extra math for transaction fee
        public override bool Debit(decimal debit)
        {
            if (base.Debit(debit))
            {
                base.Debit(TransactionFee);
                return true;
            }
            else if (!base.Debit(debit))
            {
                return false;
            }
            else
                throw new ArgumentOutOfRangeException("Debit amount exceeded account balance.");
        }
    }
}
