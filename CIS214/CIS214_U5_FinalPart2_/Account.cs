﻿using System;

namespace FinalPart2
{
    class Account
    {
        private decimal Balance { get; set; } // Account balance private variable

        public Account(decimal initialBalance) // Constructor for base Account class
        {
            // Check if initial value is above 0, assign if so, throw exception if not.
            if (initialBalance >= 0) 
                Balance = initialBalance;
            else
                throw new ArgumentOutOfRangeException("Balance", initialBalance, "Balance must be >= 0");
        }

        public decimal CurrentBalance => Balance; // Public method to access account balance

        public virtual decimal Credit ( decimal credit )
        {
            // Credit money to account
            Balance += credit;
            return CurrentBalance;
        }

        public virtual bool Debit ( decimal debit )
        {
            // Debit money from account, return false if no money is debited, true if money is debited, 
            // throw exception if there is not enough money in the account.
            if ( debit == 0 )
            {
                return false;
            }
            else if (Balance - debit >= 0)
            {
                Balance -= debit;
                return true;
            }
            else
                throw new ArgumentOutOfRangeException("Debit amount exceeded account balance.");
        }
    }
}
