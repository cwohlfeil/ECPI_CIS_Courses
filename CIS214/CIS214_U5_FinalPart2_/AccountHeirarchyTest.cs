﻿using System;

namespace FinalPart2
{
    class AccountHeirarchyTest
    {
        static void Main(string[] args)
        {
            // Test account class
            Account account1 = new Account(1000M);
            Console.WriteLine("Account 1: Generic Account");
            Console.WriteLine("Current balance: {0}", account1.CurrentBalance);
            Console.WriteLine("Crediting 100 dollars.");
            account1.Credit(100M); // Add 100
            Console.WriteLine("Current balance: {0}", account1.CurrentBalance);
            Console.WriteLine("Debiting 100 dollars.");
            account1.Debit(100M); // Subtract 100
            Console.WriteLine("Current balance: {0}", account1.CurrentBalance);
            Console.WriteLine("Attempting to set balance below 0.");
            try
            {
                account1.Debit(2000M); // Attempt to go out of bounds
            }
            catch
            {
                Console.WriteLine("Exception caught.");
            }
            

            // Test savings account class
            SavingsAccount account2 = new SavingsAccount(1000M, 0.01M);
            Console.WriteLine("\nAccount 2: Savings Account");
            Console.WriteLine("Current balance: {0}", account2.CurrentBalance);
            Console.WriteLine("Current interest: {0}", account2.CalculateInterest);
            Console.WriteLine("Adding interest to account balance.");
            account2.Credit(account2.CalculateInterest);
            Console.WriteLine("Current balance: {0}", account2.CurrentBalance);

            // Test checking account class
            CheckingAccount account3 = new CheckingAccount(1000M, 2.50M);
            Console.WriteLine("\nAccount 3: Checking Account");
            Console.WriteLine("Current balance: {0}", account3.CurrentBalance);
            Console.WriteLine("Crediting 100 dollars w/ transaction fee.");
            account3.Credit(100M); // Add 100 w/ transaction fee
            Console.WriteLine("Current balance: {0}", account3.CurrentBalance);
            Console.WriteLine("Debiting 100 dollars w/ transaction fee.");
            account3.Debit(100M); // Subtract 100 w/ transaction fee
            Console.WriteLine("Current balance: {0}", account3.CurrentBalance);
            Console.WriteLine("Debiting with 0 dollars, should be no fee.");
            account3.Debit(0M); // Subtract 0 w/o transaction fee
            Console.WriteLine("Current balance: {0}", account3.CurrentBalance);
        }
    }
}
