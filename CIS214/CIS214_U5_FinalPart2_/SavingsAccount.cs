﻿using System;

namespace FinalPart2
{
    class SavingsAccount : Account // Inheret from base account class
    {
        private decimal InterestRate {get; set;} // Private variable for interest

        public SavingsAccount(decimal initialBalance, decimal initialInterestRate) : base(initialBalance)
        {
            // Constrcutor, inherits from Account class, assign initial interest rate
            InterestRate = initialInterestRate;
        }

        public decimal CalculateInterest => CurrentBalance * InterestRate; // Public method to calculate interest
    }
}
