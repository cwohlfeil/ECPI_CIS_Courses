// Fig. 5.9: GradeBook.cs
// GradeBook class that solves the class-average problem using sentinel-controlled repetition.
using System;

public class GradeBook {
    // auto-implemented property CourseName
    public string CourseName {get; set;}

    // GradeBook constructor
    public GradeBook(string course) { // take default parameters
        CourseName = course; // assign parameter to property
    } // end constructor

    // Output method
    public void DisplayMessage() {
        Console.WriteLine("Welcome to the grade book for {0}!", CourseName);
    } // end method

    // Determine the average of an arbitrary number of grades
    public void DetermineClassAverage() {
        int total; // sum of grades
        int gradeCounter; // number of grades entered
        int grade; // grade value
        double average; // average in decimal

        // initialization phase
        total = 0; // initialize total
        gradeCounter = 0; // initialize loop counter

        // Processing phase
        // prompt for and read a grade from the user
        Console.Write("Enter grade or -1 to quit: ");
        grade = Convert.toInt32(Console.ReadLine()); // read grade

        // loop until sentinel value is read from the user
        while (gradeCounter != -1) { // loop 10 times
            total = total + grade; // add the grade to the total
            gradeCounter = gradeCounter + 1; // increment the counter by 1

            // prompt for and read a grade from the user
            Console.Write("Enter grade or -1 to quit: ");
            grade = Convert.toInt32(Console.ReadLine()); // read grade
        } // end while

        // termination phase
        // if the user entered at least one grade
        if (gradeCounter != 0) {
            // calculate the average of all the grades entered
            average = (double)total / gradeCounter;
            // display total and average of grades
            Console.WriteLine("\nTotal of the {0} grades is {1}", gradeCounter, total);
            Console.WriteLine("Class average is {0:F}", average);
        } else // no grades entered, output error method
            Console.WriteLine("No grades were entered.");
    } // end method
} // end class