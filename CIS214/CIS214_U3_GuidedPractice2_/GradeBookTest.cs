// Fig 5.7: GradebookTest.cs
// Create GradeBook object and invoke its DetermineClassAverage method

public class GradeBookTest {
   // Main method begins program execution
   public static void Main(string[] args) {
      // create GradeBook object and pass name to constructor
      GradeBook gradeBook1 = new GradeBook("CS101 Introduction to C# Programming");

      gradeBook1.DisplayMessage(); // display welcome message
      gradeBook1.DetermineClassAverage(); // Prompt for input and display class average
   } // end Main
} // end class GradeBookTest