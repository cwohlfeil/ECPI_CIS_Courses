// Invoice class file

using System;

public class Invoice {
    // Getters and setters for class properties
    public string M_PartNumber {get; set;}
    public string M_PartDescription {get; set;}
    // Specify setters so the properties are only changed if the value is non-negative
    private int _M_Quantity;
    private decimal _M_PricePerItem;
    public int M_Quantity {
        get => _M_Quantity;
        set {
            if (value >= 0) {
                _M_Quantity = value;
            }
        }
    }
    public decimal M_PricePerItem
    {
        get => _M_PricePerItem;
        set {
            if (value >= 0M) {
                _M_PricePerItem = value;
            } 
        }
    }

    // Invoice constructor
    public Invoice(string PartNumber, string PartDescription, int Quantity, decimal PricePerItem) {
        // Initialize all four values
        M_PartNumber = PartNumber;
        M_PartDescription = PartDescription;
        M_Quantity = Quantity;
        M_PricePerItem = PricePerItem;
    }

    // Class method to return invoice total
    public decimal GetInvoiceAmount() => M_Quantity * M_PricePerItem;
}