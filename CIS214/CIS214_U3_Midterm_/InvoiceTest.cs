// Program to test the Invoice class
using System;

public class InvoiceTest {
   // Main method begins program execution
   public static void Main(string[] args) {
        // Create Invoice object and set default values
        Invoice invoice1 = new Invoice("1", "Screws", 500, 0.10M);

        // Test constructor, getters, and invoice total
        Console.WriteLine("{0} should be 1", invoice1.M_PartNumber);
        Console.WriteLine("{0} should be Screws", invoice1.M_PartDescription);
        Console.WriteLine("{0} should be 500", invoice1.M_Quantity);
        Console.WriteLine("{0} should be 0.10", invoice1.M_PricePerItem);
        Console.WriteLine("{0} should be 50", invoice1.GetInvoiceAmount());


        // Test all setters with known good input
        invoice1.M_PartNumber = "2";
        invoice1.M_PartDescription = "Nails";
        invoice1.M_Quantity = 1000;
        invoice1.M_PricePerItem = 0.05M;
        Console.WriteLine("{0} should be 2", invoice1.M_PartNumber);
        Console.WriteLine("{0} should be Nails", invoice1.M_PartDescription);
        Console.WriteLine("{0} should be 1000", invoice1.M_Quantity);
        Console.WriteLine("{0} should be 0.05", invoice1.M_PricePerItem);
        Console.WriteLine("{0} should be 50", invoice1.GetInvoiceAmount());

        // Test number setters with negatives
        invoice1.M_Quantity = -500;
        invoice1.M_PricePerItem = -0.10M;
        Console.WriteLine("{0} should still be 1000.", invoice1.M_Quantity);
        Console.WriteLine("{0} should still be 0.05.", invoice1.M_PricePerItem);
   } // end Main
} // end class

