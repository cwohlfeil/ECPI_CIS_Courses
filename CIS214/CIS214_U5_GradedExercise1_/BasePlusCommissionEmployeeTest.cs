// Fig. 11.5: CommissionEmployeeTest.cs
// Testing class CommissionEmployee.
using System;

public class BasePlusCommissionEmployeeTest
{
    public static void Main(string[] args)
    {
        // instantiate CommissionEmployee object
        CommissionEmployee employee = new CommissionEmployee("Sue", "Jones", "222-22-2222", 10000.00M, .06M);
        BasePlusCommissionEmployee employee1 = new BasePlusCommissionEmployee(employee, 30000M);

        // display commission employee data
        Console.WriteLine("Employee information obtained by properties and methods: \n");
        Console.WriteLine("First name is {0}", employee1.employee.FirstName);
        Console.WriteLine("Last name is {0}", employee1.employee.LastName);
        Console.WriteLine("Social security number is {0}", employee1.employee.SocialSecurityNumber);
        Console.WriteLine("Gross sales are {0:C}", employee1.employee.GrossSales);
        Console.WriteLine("Commission rate is {0:F2}", employee1.employee.CommissionRate);
        Console.WriteLine("Base salary is {0:C}", employee1.BaseSalary);
        Console.WriteLine("Earnings are {0:C}", employee1.Earnings);

        employee1.employee.GrossSales = 5000.00M; // set gross sales   
        employee1.employee.CommissionRate = .1M; // set commission rate

        Console.WriteLine("\n{0}:\n\n{1}", "Updated employee information obtained by ToString", employee1);
        Console.WriteLine("earnings: {0:C}", employee1.Earnings);
    } // end Main
} // end class CommissionEmployeeTest