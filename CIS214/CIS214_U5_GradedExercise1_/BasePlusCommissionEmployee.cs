﻿// Fig. 11.8: BasePlusCommissionEmployee.cs
// BasePlusCommissionEmployee inherits from class CommissionEmployee.
using System;

public class BasePlusCommissionEmployee : object {
    private decimal baseSalary; // base salary per week
    public CommissionEmployee employee; // create a CommissionEmployee object to use

    // six-parameter derived class constructor
    // with call to base class CommissionEmployee constructor
    public BasePlusCommissionEmployee(CommissionEmployee employee, decimal salary)
    {
        this.employee = employee;
        baseSalary = salary;
    }

    // property that gets and sets BasePlusCommissionEmployee's base salary
    public decimal BaseSalary {
        get => baseSalary;
        set
        {
            if (value >= 0)
                baseSalary = value;
            else
                throw new ArgumentOutOfRangeException("BaseSalary", value, "BaseSalary must be >= 0");
        } // end set
    } // end property BaseSalary

    // calculate earnings
    public decimal Earnings => baseSalary + (employee.CommissionRate * employee.GrossSales);

    // return string representation of BasePlusCommissionEmployee
    public new string ToString => string.Format(
         "{0}: {1} {2}\n{3}: {4}\n{5}: {6:C}\n{7}: {8:F2}\n{9}: {10:C}",
         "base-salaried commission employee", employee.FirstName, employee.LastName,
         "social security number", employee.SocialSecurityNumber,
         "gross sales", employee.GrossSales, "commission rate", employee.CommissionRate,
         "base salary", baseSalary); // end method ToString
} // end class BasePlusCommissionEmployee