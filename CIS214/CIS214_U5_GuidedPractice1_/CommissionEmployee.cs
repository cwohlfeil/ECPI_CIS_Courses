﻿// Fig. 11.4: CommissionEmployee.cs
// CommissionEmployee class represents a commission employee.
using System;

public class CommissionEmployee : object
{
    private readonly string firstName;
    private readonly string lastName;
    private readonly string socialSecurityNumber;
    private decimal grossSales; // gross weekly sales       
    private decimal commissionRate; // commission percentage

    // five-parameter constructor
    public CommissionEmployee(string first, string last, string ssn, decimal sales, decimal rate)
    {
        // implicit call to object constructor occurs here             
        firstName = first;
        lastName = last;
        socialSecurityNumber = ssn;
        grossSales = sales; // validate gross sales via property       
        CommissionRate = rate; // validate commission rate via property
    } // end five-parameter CommissionEmployee constructor            

    // read-only property that gets commission employee's first name
    public string FirstName { get => firstName; }

    // read-only property that gets commission employee's last name
    public string LastName { get => lastName; }

    // read-only property that gets commission employee's social security number
    public string SocialSecurityNumber { get => socialSecurityNumber; }

    // property that gets and sets commission employee's gross sales
    public decimal GrossSales
    {
        get => grossSales;
        set
        {
            if (value >= 0)
                grossSales = value;
            else
                throw new ArgumentOutOfRangeException("GrossSales", value, "GrossSales must be >= 0");
        } // end set
    } // end property GrossSales

    // property that gets and sets commission employee's commission rate
    public decimal CommissionRate
    {
        get => commissionRate;
        set
        {
            if (value > 0 && value < 1)
                commissionRate = value;
            else
                throw new ArgumentOutOfRangeException("CommissionRate", value, "CommissionRate must be > 0 and < 1");
        } // end set
    } // end property CommissionRate

    // calculate commission employee's pay
    public virtual decimal Earnings => commissionRate * grossSales;

    // return string representation of CommissionEmployee object         
    public override string ToString() => string.Format(
           "{0}: {1} {2}\n{3}: {4}\n{5}: {6:C}\n{7}: {8:F2}",
           "commission employee", FirstName, LastName,
           "social security number", SocialSecurityNumber,
           "gross sales", grossSales, "commission rate", CommissionRate); // end method ToString                                             
} // end class CommissionEmployee