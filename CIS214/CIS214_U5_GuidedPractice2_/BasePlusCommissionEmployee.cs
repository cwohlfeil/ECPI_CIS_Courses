﻿// Fig. 11.8: BasePlusCommissionEmployee.cs
// BasePlusCommissionEmployee inherits from class CommissionEmployee.
using System;

public class BasePlusCommissionEmployee : CommissionEmployee {
   private decimal baseSalary; // base salary per week

    // six-parameter derived class constructor
    // with call to base class CommissionEmployee constructor
    public BasePlusCommissionEmployee(string first, string last,
       string ssn, decimal sales, decimal rate, decimal salary)
       : base(first, last, ssn, sales, rate) => BaseSalary = salary; // validate base salary via property

    // property that gets and sets BasePlusCommissionEmployee's base salary
    public decimal BaseSalary {
        get => baseSalary;
        set
        {
            if (value >= 0)
                baseSalary = value;
            else
                throw new ArgumentOutOfRangeException("BaseSalary", value, "BaseSalary must be >= 0");
        } // end set
    } // end property BaseSalary

    // calculate earnings
    public override decimal Earnings => baseSalary + (CommissionRate * GrossSales);

    // return string representation of BasePlusCommissionEmployee
    public override string ToString() => string.Format(
         "{0}: {1} {2}\n{3}: {4}\n{5}: {6:C}\n{7}: {8:F2}\n{9}: {10:C}",
         "base-salaried commission employee", FirstName, LastName,
         "social security number", SocialSecurityNumber,
         "gross sales", GrossSales, "commission rate", CommissionRate,
         "base salary", baseSalary); // end method ToString
} // end class BasePlusCommissionEmployee