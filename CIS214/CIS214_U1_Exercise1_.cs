using System;

public class Printing {
    public static void Main(string[] args) {
        Console.Write("Part (a): ");
        Console.WriteLine("1 2 3 4");

        Console.Write("Part (b): ");
        Console.Write("1 ");
        Console.Write("2 ");
        Console.Write("3 ");
        Console.Write("4 \n");

        Console.Write("Part (c): ");
        Console.WriteLine("{0} {1} {2} {3}", 1, 2, 3, 4);
    }
}