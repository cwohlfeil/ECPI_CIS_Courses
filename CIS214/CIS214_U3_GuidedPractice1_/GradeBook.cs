// GradeBook class
using System;

public class GradeBook {
    // Getter and setter for CourseName
    public string CourseName {get; set;}

    // GradeBook constructor
    public GradeBook(string course) { // take default parameters
        // assign parameters to class variables
        CourseName = course;
    } // end constructor

    // Output method
    public void DisplayMessage() {
        Console.WriteLine("Welcome to the grade book for {0}", CourseName);
    } // end method

    // Class average method
    public void DetermineClassAverage() {
        // initialization phase
        total = 0;
        gradeCounter = 1;

        // Processing phase
        while (gradeCounter <= 10) { // loop 10 times
            Console.Write("Enter grade: "); // prompt the user
            grade = Convert.toInt32(Console.ReadLine()); // read grade
            total = total + grade; // add the grade to the total
            gradeCounter = gradeCounter + 1; // increment the counter by 1
        } // end while

        // termination phase
        average = total / 10; // int division yields int result

        // display total and average of grades
        Console.WriteLine("\nTotal of all 10 grades is {0}", total);
        Console.WriteLine("Class average is {0}", average);
    } // end method
} // end class