// Date class
using System;

public class Date {
    // Getters and setters for date values
    public string month {get; set;}
    public string day {get; set;}
    public string year {get; set;}

    // Date constructor
    public Date(int m, int d, int y) {
        month = m;
        day = d;
        year = y;
    }

    // Output method
    public void DisplayDate() {
        Console.WriteLine("{0}/{1}/{2}", month, day, year);
    }
}