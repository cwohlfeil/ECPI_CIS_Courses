using System;

public class Calculate {
    public static void Main(string[] args) {
        int number1, number2, sum, product, difference, quotient;

        Console.Write("Enter first integer: ");
        number1 = Convert.ToInt32(Console.ReadLine());

        Console.Write("Enter second integer: ");
        number2 = Convert.ToInt32(Console.ReadLine());

        sum = number1 + number2;
        Console.WriteLine("\nSum is {0}", sum);

        product = number1 * number2;
        Console.WriteLine("Product is {0}", product);

        difference = number1 - number2;
        Console.WriteLine("Difference is {0}", difference);

        quotient = number1 / number2;
        Console.WriteLine("Quotient is {0}", quotient);
    }
}