using System;

public class Calculate {
    public static void Main(string[] args) {
        int number1, number2;

        Console.Write("Enter first integer: ");
        number1 = Convert.ToInt32(Console.ReadLine());

        Console.Write("Enter second integer: ");
        number2 = Convert.ToInt32(Console.ReadLine());

        if  (number1 > number2)
            Console.WriteLine("\n{0} is greater than {1}", number1, number2);
        else if  (number1 < number2)
            Console.WriteLine("\n{0} is greater than {1}", number2, number1);
        else
            Console.WriteLine("\n{0} is equal to {1}", number1, number2);
    }
}